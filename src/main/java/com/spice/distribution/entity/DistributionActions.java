package com.spice.distribution.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author ankit
 *
 */
@Entity
public class DistributionActions {

	@Id
	private int id;
	private String distribution_action;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDistribution_action() {
		return distribution_action;
	}

	public void setDistribution_action(String distribution_action) {
		this.distribution_action = distribution_action;
	}

	@Override
	public String toString() {
		return "DistributionActions [id=" + id + ", distribution_action=" + distribution_action + "]";
	}

}
