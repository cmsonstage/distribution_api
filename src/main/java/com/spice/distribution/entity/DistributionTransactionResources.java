package com.spice.distribution.entity;

import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author ankit
 *
 */
@Entity
public class DistributionTransactionResources {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Integer distribution_transaction_id;
	private BigInteger reference_id;
	@Digits(integer = 10, fraction = 0, message = "Invalid Resource Code. Should be Digits[0-9]")
	@Size(max = 10, min = 10, message = "Invalid Resource Code Size. Should be 10")
	@NotBlank(message = "Blank Resource Code Found")
	@NotEmpty(message = " Empty Resource Code Found")
	@NotNull(message = " Null Resource Code Found")
	private String resource_code;
	private String remarks;
	private Integer distribution_transaction_resource_status_id = 1;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "distribution_transaction_resource_status_id", referencedColumnName = "id", insertable = false, updatable = false)
	private DistributionTransactionResourceStatuses distributionTransactionResourceStatuses;

	public Integer getDistribution_transaction_id() {
		return distribution_transaction_id;
	}

	public void setDistribution_transaction_id(Integer distribution_transaction_id) {
		this.distribution_transaction_id = distribution_transaction_id;
	}
	
	public BigInteger getReference_id() {
		return reference_id;
	}

	public void setReference_id(BigInteger reference_id) {
		this.reference_id = reference_id;
	}

	/**
	 * @param id
	 * @param resource_code
	 */

	public int getId() {
		return id;
	}

	public DistributionTransactionResources(String resource_code) {
		super();
		this.resource_code = resource_code;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getResource_code() {
		return resource_code;
	}

	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}

	public DistributionTransactionResources() {
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getDistribution_transaction_resource_status_id() {
		return distribution_transaction_resource_status_id;
	}

	public void setDistribution_transaction_resource_status_id(Integer distribution_transaction_resource_status_id) {
		this.distribution_transaction_resource_status_id = distribution_transaction_resource_status_id;
	}

	public DistributionTransactionResourceStatuses getDistributionTransactionResourceStatuses() {
		return distributionTransactionResourceStatuses;
	}

	public void setDistributionTransactionResourceStatuses(DistributionTransactionResourceStatuses distributionTransactionResourceStatuses) {
		this.distributionTransactionResourceStatuses = distributionTransactionResourceStatuses;
	}

	@Override
	public String toString() {
		return "DistributionTransactionResources [id=" + id + ", distribution_transaction_id=" + distribution_transaction_id + ", resource_code=" + resource_code + "]";
	}

}
