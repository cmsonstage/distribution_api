package com.spice.distribution.entity;

import java.math.BigDecimal;

/**
 * @author ankit
 *
 */
public class Status {
	private BigDecimal total;
	private BigDecimal pending;
	private BigDecimal successfull;
	private BigDecimal failed;

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getPending() {
		return pending;
	}

	public void setPending(BigDecimal pending) {
		this.pending = pending;
	}

	public BigDecimal getSuccessfull() {
		return successfull;
	}

	public void setSuccessfull(BigDecimal successfull) {
		this.successfull = successfull;
	}

	public BigDecimal getFailed() {
		return failed;
	}

	public void setFailed(BigDecimal failed) {
		this.failed = failed;
	}

}
