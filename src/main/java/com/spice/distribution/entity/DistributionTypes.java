package com.spice.distribution.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author rahul
 * @email <rahul.gupta@approutes.com>
 * @date 16-Aug-2017
 */
@Entity
public class DistributionTypes {

	@Id
	private int id;
	private String distribution_type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDistribution_type() {
		return distribution_type;
	}

	public void setDistribution_type(String distribution_type) {
		this.distribution_type = distribution_type;
	}

	@Override
	public String toString() {
		return "DistributionTypes [id=" + id + ", distribution_type=" + distribution_type + "]";
	}

}
