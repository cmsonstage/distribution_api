package com.spice.distribution.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "DistributionTransactions")
public class Distributions1 {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Integer distribution_type_id;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "distribution_transaction_id")
	private Set<DistributionTransactionResources> distributionTransactionResources;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "distribution_transaction_id")
	private Set<DistributionTransactionReleases> distributionTransactionReleases;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<DistributionTransactionResources> getDistributionTransactionResources() {
		return distributionTransactionResources;
	}

	public void setDistributionTransactionResources(Set<DistributionTransactionResources> distributionTransactionResources) {
		this.distributionTransactionResources = distributionTransactionResources;
	}

	public Set<DistributionTransactionReleases> getDistributionTransactionReleases() {
		return distributionTransactionReleases;
	}

	public void setDistributionTransactionReleases(Set<DistributionTransactionReleases> distributionTransactionReleases) {
		this.distributionTransactionReleases = distributionTransactionReleases;
	}

	public Integer getDistribution_type_id() {
		return distribution_type_id;
	}

	public void setDistribution_type_id(Integer distribution_type_id) {
		this.distribution_type_id = distribution_type_id;
	}

}
