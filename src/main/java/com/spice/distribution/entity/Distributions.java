package com.spice.distribution.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "DistributionTransactions")
public class Distributions {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Integer user_id;
	private Integer service_id;
	private Integer distribution_action_id;
	private Integer distribution_type_id;
	private Integer distribution_source_id;
	private Integer distribution_transaction_status_id;
	private String description;
	private Date created_at;
	private Date updated_at;
	private String service;
	@Transient
	private Status resourceStatus = new Status();
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "distribution_transaction_status_id", referencedColumnName = "id", insertable = false, updatable = false)
	private DistributionTransactionStatusess distributionTransactionStatusess;
	//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	//	@JoinColumn(name = "distribution_transaction_id")
	//	private Set<DistributionTransactionResources> distributionTransactionResources;
	//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	//	@JoinColumn(name = "distribution_transaction_id")
	//	private Set<DistributionTransactionReleases> distributionTransactionReleases;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "distribution_type_id", referencedColumnName = "id", insertable = false, updatable = false)
	private DistributionTypes distributionTypes;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "distribution_action_id", referencedColumnName = "id", insertable = false, updatable = false)
	private DistributionActions distributionActions;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "distribution_source_id", referencedColumnName = "id", insertable = false, updatable = false)
	private DistributionSources distributionSources;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getService_id() {
		return service_id;
	}

	public void setService_id(Integer service_id) {
		this.service_id = service_id;
	}

	public Integer getDistribution_action_id() {
		return distribution_action_id;
	}

	public void setDistribution_action_id(Integer distribution_action_id) {
		this.distribution_action_id = distribution_action_id;
	}

	public Integer getDistribution_type_id() {
		return distribution_type_id;
	}

	public void setDistribution_type_id(Integer distribution_type_id) {
		this.distribution_type_id = distribution_type_id;
	}

	public Integer getDistribution_source_id() {
		return distribution_source_id;
	}

	public void setDistribution_source_id(Integer distribution_source_id) {
		this.distribution_source_id = distribution_source_id;
	}

	public Integer getDistribution_transaction_status_id() {
		return distribution_transaction_status_id;
	}

	public void setDistribution_transaction_status_id(Integer distribution_transaction_status_id) {
		this.distribution_transaction_status_id = distribution_transaction_status_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DistributionTypes getDistributionTypes() {
		return distributionTypes;
	}

	public void setDistributionTypes(DistributionTypes distributionTypes) {
		this.distributionTypes = distributionTypes;
	}

	public DistributionActions getDistributionActions() {
		return distributionActions;
	}

	public void setDistributionActions(DistributionActions distributionActions) {
		this.distributionActions = distributionActions;
	}

	public DistributionSources getDistributionSources() {
		return distributionSources;
	}

	public void setDistributionSources(DistributionSources distributionSources) {
		this.distributionSources = distributionSources;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public DistributionTransactionStatusess getDistributionTransactionStatusess() {
		return distributionTransactionStatusess;
	}

	public void setDistributionTransactionStatusess(DistributionTransactionStatusess distributionTransactionStatusess) {
		this.distributionTransactionStatusess = distributionTransactionStatusess;
	}

	public Status getResourceStatus() {
		return resourceStatus;
	}

	public void setResourceStatus(Status resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

}
