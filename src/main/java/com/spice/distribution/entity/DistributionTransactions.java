package com.spice.distribution.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author ankit
 *
 */
@Entity
public class DistributionTransactions {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Integer user_id;
	private Integer service_id;
	@NotNull(message = "Action Id cannot be blank")
	private Integer distribution_action_id;
	@NotNull(message = "Distribution Type Id cannot be blank")
	private Integer distribution_type_id;
	@NotNull(message = "Distribution Source Id cannot be blank")
	private Integer distribution_source_id;
	private Integer distribution_transaction_status_id = 1;
	private String description;
	private String service;
	@Transient
	@Valid
	private Set<DistributionTransactionResources> distributionTransactionResources;
	@Transient
	@Valid
	private Set<DistributionTransactionReleases> distributionTransactionReleases;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getService_id() {
		return service_id;
	}

	public void setService_id(Integer service_id) {
		this.service_id = service_id;
	}

	public Integer getDistribution_action_id() {
		return distribution_action_id;
	}

	public void setDistribution_action_id(Integer distribution_action_id) {
		this.distribution_action_id = distribution_action_id;
	}

	public Integer getDistribution_type_id() {
		return distribution_type_id;
	}

	public void setDistribution_type_id(Integer distribution_type_id) {
		this.distribution_type_id = distribution_type_id;
	}

	public Integer getDistribution_source_id() {
		return distribution_source_id;
	}

	public void setDistribution_source_id(Integer distribution_source_id) {
		this.distribution_source_id = distribution_source_id;
	}

	public Integer getDistribution_transaction_status_id() {
		return distribution_transaction_status_id;
	}

	public void setDistribution_transaction_status_id(Integer distribution_transaction_status_id) {
		this.distribution_transaction_status_id = distribution_transaction_status_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<DistributionTransactionResources> getDistributionTransactionResources() {
		return distributionTransactionResources;
	}

	public void setDistributionTransactionResources(Set<DistributionTransactionResources> distributionTransactionResources) {
		this.distributionTransactionResources = distributionTransactionResources;
	}

	public Set<DistributionTransactionReleases> getDistributionTransactionReleases() {
		return distributionTransactionReleases;
	}

	public void setDistributionTransactionReleases(Set<DistributionTransactionReleases> distributionTransactionReleases) {
		this.distributionTransactionReleases = distributionTransactionReleases;
	}

	@Override
	public String toString() {
		return "DistributionTransactions [id=" + id + ", user_id=" + user_id + ", service_id=" + service_id + ", distribution_action_id=" + distribution_action_id + ", distribution_type_id=" + distribution_type_id + ", distribution_source_id=" + distribution_source_id + ", distribution_transaction_status_id=" + distribution_transaction_status_id + ", description=" + description + ", distributionTransactionResources=" + distributionTransactionResources + ", distributionTransactionReleases="
				+ distributionTransactionReleases + "]";
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

}
