package com.spice.distribution.request.refreshment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.spice.distribution.request.Request;

/**
 * @author ankit
 *
 */
public class RefreshmentRequest implements Request {
	@JsonProperty
	private List<Refreshed> refresh;

	public List<Refreshed> getRefresh() {
		return refresh;
	}

	public void setRefresh(List<Refreshed> refresh) {
		this.refresh = refresh;
	}

	/**
	 * @param refresh
	 */
	public RefreshmentRequest(List<Refreshed> refresh) {
		super();
		this.refresh = refresh;
	}

}
