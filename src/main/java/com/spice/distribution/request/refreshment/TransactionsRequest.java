/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.request.refreshment;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.spice.distribution.request.Request;

/**
 * The Class TransactionsRequest.
 *
 * @author ankit
 */
public class TransactionsRequest implements Request {

	/** The country id. */
	@NotNull(message = "countryId not found!!")
	private Integer country_id;

	/** The country name. */
	@NotNull(message = "country_name null found")
	@NotBlank(message = "country_name blank found")
	private String country_name;

	/** The service id. */
	@NotNull(message = "service_id not found!!")
	private Integer service_id;

	/** The service name. */
	@NotNull(message = "service_name null found")
	@NotBlank(message = "service_name blank found")
	private String service_name;

	/** The container id. */
	@NotNull(message = "container_id not found!!")
	private Integer container_id;

	/** The container name. */
	@NotNull(message = "container_name null found")
	@NotBlank(message = "container_name blank found")
	private String container_name;

	public Integer getCountry_id() {
		return country_id;
	}

	public void setCountry_id(Integer country_id) {
		this.country_id = country_id;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public Integer getService_id() {
		return service_id;
	}

	public void setService_id(Integer service_id) {
		this.service_id = service_id;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public Integer getContainer_id() {
		return container_id;
	}

	public void setContainer_id(Integer container_id) {
		this.container_id = container_id;
	}

	public String getContainer_name() {
		return container_name;
	}

	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}

	@Override
	public String toString() {
		return "TransactionsRequest [ country_id=" + country_id + ", country_name=" + country_name + ", service_id=" + service_id + ", service_name=" + service_name + ", container_id=" + container_id + ", container_name=" + container_name + "]";
	}

}
