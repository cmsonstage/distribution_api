package com.spice.distribution.request.refreshment;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.spice.distribution.request.Request;

/**
 * @author ankit
 *
 */
public class Status implements Request {
	@NotNull(message = "TransactionId Not Found")
	@Min(value = 1, message = "TransactionId cannot be less than 1")
	private Integer transactionId;
	@NotNull(message = "OldStatusId Not Found")
	@Max(value = 7, message = "OldStatusId cannot be greater than 7")
	@Min(value = 1, message = "OldStatusId cannot be less than 1")
	private Integer oldStatusId;
	@NotNull(message = "NewStatusId Not Found")
	@Max(value = 7, message = "NewStatusId cannot be greater than 7")
	@Min(value = 1, message = "NewStatusId cannot be less than 1")
	private Integer newStatusId;

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public Integer getOldStatusId() {
		return oldStatusId;
	}

	public void setOldStatusId(Integer oldStatusId) {
		this.oldStatusId = oldStatusId;
	}

	public Integer getNewStatusId() {
		return newStatusId;
	}

	public void setNewStatusId(Integer newStatusId) {
		this.newStatusId = newStatusId;
	}

	@Override
	public String toString() {
		return "Status [transactionId=" + transactionId + ", oldStatusId=" + oldStatusId + ", newStatusId=" + newStatusId + "]";
	}

}
