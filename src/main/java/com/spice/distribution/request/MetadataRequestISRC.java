package com.spice.distribution.request;

import javax.validation.constraints.NotNull;

import com.spice.distribution.validation.MetadataSource;

public class MetadataRequestISRC  implements Request  {
	
//	@MetadataSource
//	private String metadataSource;
//	
//	private Integer serviceId;
	
	@NotNull(message = "ISRC Id Not Found")
	private String isrc;
	
	@NotNull(message = "Artist Id Not Found")
	private String artist_name;
	
	@NotNull(message = "Title Text Id Not Found")
	private String title_text;

//	public String getMetadataSource() {
//		return "cms";
//	}
//
//	public void setMetadataSource(String metadataSource) {
//		this.metadataSource = metadataSource;
//	}

//	public Integer getServiceId() {
//		return serviceId;
//	}
//
//	public void setServiceId(Integer serviceId) {
//		this.serviceId = serviceId;
//	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public String getArtist_name() {
		return artist_name;
	}

	public void setArtist_name(String artist_name) {
		this.artist_name = artist_name;
	}

	public String getTitle_text() {
		return title_text;
	}

	public void setTitle_text(String title_text) {
		this.title_text = title_text;
	}
	
	
	
}
