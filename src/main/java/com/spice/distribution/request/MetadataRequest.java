package com.spice.distribution.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.spice.distribution.validation.MetadataSource;

/**
 * @author ankit
 *
 */

public class MetadataRequest implements Request {
	@MetadataSource
	private String metadataSource;
	
	@NotNull(message = "Resources cannot be null")
	@NotEmpty(message = "Resources cannot be empty")
	private List<String> resources;

	private List<Integer> releases;
	
	@NotNull(message = "Service Id Not Found")
	private Integer serviceId;

	public String getMetadataSource() {
		return metadataSource;
	}

	public void setMetadataSource(String metadataSource) {
		this.metadataSource = metadataSource;
	}

	public List<String> getResources() {

		return resources;
	}

	public void setResources(List<String> resources) {
		this.resources = resources;
	}

	public List<Integer> getReleases() {
		return releases;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public void setReleases(List<Integer> releases) {
		this.releases = releases;
	}

}
