
package com.spice.distribution.response.safaricom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.spice.distribution.response.Response;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "audios", "albums", "artists", "genres", "languages" })
@XmlRootElement
public class SafariComResponse implements Response {

	@JsonProperty("audios")
	private List<Audio> audios = null;
	@JsonProperty("albums")
	private List<Album> albums = null;
	@JsonProperty("artists")
	private List<Artist> artists = null;
	@JsonProperty("genres")
	private List<Genre> genres = null;
	@JsonProperty("languages")
	private List<Language> languages = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("audios")
	public List<Audio> getAudios() {
		return audios;
	}

	@JsonProperty("audios")
	public void setAudios(List<Audio> audios) {
		this.audios = audios;
	}

	@JsonProperty("albums")
	public List<Album> getAlbums() {
		return albums;
	}

	@JsonProperty("albums")
	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	@JsonProperty("artists")
	public List<Artist> getArtists() {
		return artists;
	}

	@JsonProperty("artists")
	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	@JsonProperty("genres")
	public List<Genre> getGenres() {
		return genres;
	}

	@JsonProperty("genres")
	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	@JsonProperty("languages")
	public List<Language> getLanguages() {
		return languages;
	}

	@JsonProperty("languages")
	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SafariComResponse [audios=" + audios + ", albums=" + albums + ", artists=" + artists + ", genres="
				+ genres + ", languages=" + languages + ", additionalProperties=" + additionalProperties + "]";
	}

}
