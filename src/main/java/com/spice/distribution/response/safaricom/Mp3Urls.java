
package com.spice.distribution.response.safaricom;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "auto", "high", "medium", "low" })
public class Mp3Urls {

	@JsonProperty("auto")
	private String auto;
	@JsonProperty("high")
	private String high;
	@JsonProperty("medium")
	private String medium;
	@JsonProperty("low")
	private String low;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("auto")
	public String getAuto() {
		return auto;
	}

	@JsonProperty("auto")
	public void setAuto(String auto) {
		this.auto = auto;
	}

	@JsonProperty("high")
	public String getHigh() {
		return high;
	}

	@JsonProperty("high")
	public void setHigh(String high) {
		this.high = high;
	}

	@JsonProperty("medium")
	public String getMedium() {
		return medium;
	}

	@JsonProperty("medium")
	public void setMedium(String medium) {
		this.medium = medium;
	}

	@JsonProperty("low")
	public String getLow() {
		return low;
	}

	@JsonProperty("low")
	public void setLow(String low) {
		this.low = low;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

}
