/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.distribution.response.cnema;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class Release representing the Release in Json.
 * 
 * @author ankit
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Title", "UPC", "ReleaseType", "ReleaseImageUrls","ReleasePosterUrls" })
public class Release {

	/** The title. */
	@JsonProperty("Title")
	private String title;

	/** The u PC. */
	@JsonProperty("UPC")
	private String uPC;

	/** The release type. */
	@JsonProperty("ReleaseType")
	private String releaseType;

	/** The release image urls. */
	@JsonProperty("ReleaseImageUrls")
	private ReleaseImageUrls releaseImageUrls;
	@JsonProperty("ReleasePosterUrls")
	private ReleasePosterUrls releasePosterUrls;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	@JsonProperty("Title")
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	@JsonProperty("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the upc.
	 *
	 * @return the upc
	 */
	@JsonProperty("UPC")
	public String getUPC() {
		return uPC;
	}

	/**
	 * Sets the upc.
	 *
	 * @param uPC the new upc
	 */
	@JsonProperty("UPC")
	public void setUPC(String uPC) {
		this.uPC = uPC;
	}

	/**
	 * Gets the release type.
	 *
	 * @return the release type
	 */
	@JsonProperty("ReleaseType")
	public String getReleaseType() {
		return releaseType;
	}

	/**
	 * Sets the release type.
	 *
	 * @param releaseType the new release type
	 */
	@JsonProperty("ReleaseType")
	public void setReleaseType(String releaseType) {
		this.releaseType = releaseType;
	}

	/**
	 * Gets the release image urls.
	 *
	 * @return the release image urls
	 */
	@JsonProperty("ReleaseImageUrls")
	public ReleaseImageUrls getReleaseImageUrls() {
		return releaseImageUrls;
	}

	/**
	 * Sets the release image urls.
	 *
	 * @param releaseImageUrls the new release image urls
	 */
	@JsonProperty("ReleaseImageUrls")
	public void setReleaseImageUrls(ReleaseImageUrls releaseImageUrls) {
		this.releaseImageUrls = releaseImageUrls;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	

	/**
	 * @return the releasePosterUrls
	 */
	@JsonProperty("ReleasePosterUrls")
	public ReleasePosterUrls getReleasePosterUrls() {
		return releasePosterUrls;
	}

	/**
	 * @param releasePosterUrls the releasePosterUrls to set
	 */
	@JsonProperty("ReleasePosterUrls")
	public void setReleasePosterUrls(ReleasePosterUrls releasePosterUrls) {
		this.releasePosterUrls = releasePosterUrls;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Release [title=" + title + ", uPC=" + uPC + ", releaseType=" + releaseType + ", releaseImageUrls="
				+ releaseImageUrls + ", releasePosterUrls=" + releasePosterUrls + ", additionalProperties="
				+ additionalProperties + "]";
	}

}
