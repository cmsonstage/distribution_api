/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.distribution.response.cnema;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class DownloadUrls representing DownloadUrls in Json
 * 
 * @author ankit.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Standard", "Medium", "Low" })
public class DownloadUrls {

	/** The standard. */
	@JsonProperty("Standard")
	private String standard;

	/** The medium. */
	@JsonProperty("Medium")
	private String medium;

	/** The low. */
	@JsonProperty("Low")
	private String low;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the standard.
	 *
	 * @return the standard
	 */
	@JsonProperty("Standard")
	public String getStandard() {
		return standard;
	}

	/**
	 * Sets the standard.
	 *
	 * @param standard the new standard
	 */
	@JsonProperty("Standard")
	public void setStandard(String standard) {
		this.standard = standard;
	}

	/**
	 * Gets the medium.
	 *
	 * @return the medium
	 */
	@JsonProperty("Medium")
	public String getMedium() {
		return medium;
	}

	/**
	 * Sets the medium.
	 *
	 * @param medium the new medium
	 */
	@JsonProperty("Medium")
	public void setMedium(String medium) {
		this.medium = medium;
	}

	/**
	 * Gets the low.
	 *
	 * @return the low
	 */
	@JsonProperty("Low")
	public String getLow() {
		return low;
	}

	/**
	 * Sets the low.
	 *
	 * @param low the new low
	 */
	@JsonProperty("Low")
	public void setLow(String low) {
		this.low = low;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DownloadUrls [standard=" + standard + ", medium=" + medium + ", low=" + low + ", additionalProperties="
				+ additionalProperties + "]";
	}

}
