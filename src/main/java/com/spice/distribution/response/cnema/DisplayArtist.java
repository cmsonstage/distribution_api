/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.distribution.response.cnema;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class DisplayArtist representing DisplayArtist in Json
 * 
 * @author ankit.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Name", "Role", "ArtistImageUrls" })
public class DisplayArtist {

	/** The name. */
	@JsonProperty("Name")
	private String name;

	/** The role. */
	@JsonProperty("Role")
	private String role;

	/** The artist image urls. */
	@JsonProperty("ArtistImageUrls")
	private ArtistImageUrls artistImageUrls;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@JsonProperty("Name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	@JsonProperty("Name")
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	@JsonProperty("Role")
	public String getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	@JsonProperty("Role")
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Gets the artist image urls.
	 *
	 * @return the artist image urls
	 */
	@JsonProperty("ArtistImageUrls")
	public ArtistImageUrls getArtistImageUrls() {
		return artistImageUrls;
	}

	/**
	 * Sets the artist image urls.
	 *
	 * @param artistImageUrls the new artist image urls
	 */
	@JsonProperty("ArtistImageUrls")
	public void setArtistImageUrls(ArtistImageUrls artistImageUrls) {
		this.artistImageUrls = artistImageUrls;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DisplayArtist [name=" + name + ", role=" + role + ", artistImageUrls=" + artistImageUrls
				+ ", additionalProperties=" + additionalProperties + "]";
	}

}
