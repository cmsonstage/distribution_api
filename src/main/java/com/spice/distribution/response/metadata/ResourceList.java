
package com.spice.distribution.response.metadata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
// @JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ISRC", "ResourceCode", "ResourceType", "ResourceImageCode", "Title", "SubTitle", "ContentPartner", "CountryOfOrigin", "Duration", "TerritoryCodes", "Release", "DisplayArtists", "Genres", "TechnicalSoundRecordingFileDetails", "TechnicalImageFileDetails" })
public class ResourceList {

	@JsonProperty("status")
	private boolean status;
	@JsonProperty("message")
	private String message;
	@JsonProperty("ISRC")
	private String iSRC;
	@JsonProperty("ResourceCode")
	private String resourceCode;
	@JsonProperty("ResourceType")
	private String resourceType;
	@JsonProperty("ResourceImageCode")
	private String resourceImageCode;
	@JsonProperty("Title")
	private String title;
	@JsonProperty("SubTitle")
	private String subTitle;
	@JsonProperty("ContentPartner")
	private String contentPartner;
	@JsonProperty("ResourceTags")
	private List<String> resourceTags;
	@JsonProperty("Duration")
	private Integer duration;
	@JsonProperty("CountryOfOrigin")
	private List<String> countryOfOrigin = null;
	@JsonProperty("TerritoryCodes")
	private Set<String> territoryCodes = null;
	@JsonProperty("Release")
	private Release release;
	@JsonProperty("DisplayArtists")
	private List<DisplayArtist> displayArtists = null;
	@JsonProperty("Genres")
	private List<Genre> genres = null;
	@JsonProperty("TechnicalSoundRecordingFileDetails")
	private List<TechnicalSoundRecordingFileDetail> technicalSoundRecordingFileDetails = null;
	@JsonProperty("TechnicalImageFileDetails")
	private List<TechnicalImageFileDetail> technicalImageFileDetails = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("ISRC")
	public String getISRC() {
		return iSRC;
	}

	@JsonProperty("ISRC")
	public void setISRC(String iSRC) {
		this.iSRC = iSRC;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@JsonProperty("ResourceCode")
	public String getResourceCode() {
		return resourceCode;
	}

	@JsonProperty("ResourceCode")
	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	@JsonProperty("ResourceImageCode")
	public String getResourceImageCode() {
		return resourceImageCode;
	}

	@JsonProperty("ResourceImageCode")
	public void setResourceImageCode(String resourceImageCode) {
		this.resourceImageCode = resourceImageCode;
	}

	@JsonProperty("Title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("SubTitle")
	public String getSubTitle() {
		return subTitle;
	}

	@JsonProperty("SubTitle")
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	@JsonProperty("ContentPartner")
	public String getContentPartner() {
		return contentPartner;
	}

	@JsonProperty("ContentPartner")
	public void setContentPartner(String contentPartner) {
		this.contentPartner = contentPartner;
	}

	@JsonProperty("CountryOfOrigin")
	public List<String> getCountryOfOrigin() {
		return countryOfOrigin;
	}

	@JsonProperty("CountryOfOrigin")
	public void setCountryOfOrigin(List<String> countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	@JsonProperty("TerritoryCodes")
	public Set<String> getTerritoryCodes() {
		return territoryCodes;
	}

	@JsonProperty("TerritoryCodes")
	public void setTerritoryCodes(Set<String> territoryCodes) {
		this.territoryCodes = territoryCodes;
	}

	@JsonProperty("Release")
	public Release getRelease() {
		return release;
	}

	@JsonProperty("Release")
	public void setRelease(Release release) {
		this.release = release;
	}

	@JsonProperty("DisplayArtists")
	public List<DisplayArtist> getDisplayArtists() {
		return displayArtists;
	}

	@JsonProperty("DisplayArtists")
	public void setDisplayArtists(List<DisplayArtist> displayArtists) {
		this.displayArtists = displayArtists;
	}

	@JsonProperty("Genres")
	public List<Genre> getGenres() {
		return genres;
	}

	@JsonProperty("Genres")
	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	@JsonProperty("TechnicalSoundRecordingFileDetails")
	public List<TechnicalSoundRecordingFileDetail> getTechnicalSoundRecordingFileDetails() {
		return technicalSoundRecordingFileDetails;
	}

	@JsonProperty("TechnicalSoundRecordingFileDetails")
	public void setTechnicalSoundRecordingFileDetails(List<TechnicalSoundRecordingFileDetail> technicalSoundRecordingFileDetails) {
		this.technicalSoundRecordingFileDetails = technicalSoundRecordingFileDetails;
	}

	@JsonProperty("TechnicalImageFileDetails")
	public List<TechnicalImageFileDetail> getTechnicalImageFileDetails() {
		return technicalImageFileDetails;
	}

	@JsonProperty("status")
	public boolean isStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(boolean status) {
		this.status = status;
	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

	public String getiSRC() {
		return iSRC;
	}

	public void setiSRC(String iSRC) {
		this.iSRC = iSRC;
	}

	@JsonProperty("TechnicalImageFileDetails")
	public void setTechnicalImageFileDetails(List<TechnicalImageFileDetail> technicalImageFileDetails) {
		this.technicalImageFileDetails = technicalImageFileDetails;
	}

	@JsonProperty("ResourceType")
	public String getResourceType() {
		return resourceType;
	}

	@JsonProperty("ResourceType")
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/**
	 * @return the resourceTags
	 */
	public List<String> getResourceTags() {
		return resourceTags;
	}

	/**
	 * @param resourceTags the resourceTags to set
	 */
	public void setResourceTags(List<String> resourceTags) {
		this.resourceTags = resourceTags;
	}

	/**
	 * @param status
	 * @param message
	 */
	public ResourceList(boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	/**
	 * 
	 */
	public ResourceList() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResourceList [status=" + status + ", message=" + message + ", iSRC=" + iSRC + ", resourceCode="
				+ resourceCode + ", resourceType=" + resourceType + ", resourceImageCode=" + resourceImageCode
				+ ", title=" + title + ", subTitle=" + subTitle + ", contentPartner=" + contentPartner
				+ ", resourceTags=" + resourceTags + ", duration=" + duration + ", countryOfOrigin=" + countryOfOrigin
				+ ", territoryCodes=" + territoryCodes + ", release=" + release + ", displayArtists=" + displayArtists
				+ ", genres=" + genres + ", technicalSoundRecordingFileDetails=" + technicalSoundRecordingFileDetails
				+ ", technicalImageFileDetails=" + technicalImageFileDetails + ", additionalProperties="
				+ additionalProperties + "]";
	}
	
	
}
