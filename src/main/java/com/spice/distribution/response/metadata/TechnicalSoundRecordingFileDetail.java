
package com.spice.distribution.response.metadata;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Codec", "SamplingRate", "BitRate", "NumberOfChannels", "Duration", "FileSize", "FilePath", "FileName" })
public class TechnicalSoundRecordingFileDetail {

	@JsonProperty("Codec")
	private String codec;
	@JsonProperty("SamplingRate")
	private Integer samplingRate;
	@JsonProperty("BitRate")
	private Integer bitRate;
	@JsonProperty("NumberOfChannels")
	private String numberOfChannels;
	@JsonProperty("Duration")
	private Integer duration;
	@JsonProperty("FileSize")
	private Integer fileSize;
	@JsonProperty("FilePath")
	private String filePath;
	@JsonProperty("FileName")
	private String fileName;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Codec")
	public String getCodec() {
		return codec;
	}

	@JsonProperty("Codec")
	public void setCodec(String codec) {
		this.codec = codec;
	}

	@JsonProperty("SamplingRate")
	public Integer getSamplingRate() {
		return samplingRate;
	}

	@JsonProperty("SamplingRate")
	public void setSamplingRate(Integer samplingRate) {
		this.samplingRate = samplingRate;
	}

	@JsonProperty("BitRate")
	public Integer getBitRate() {
		return bitRate;
	}

	@JsonProperty("BitRate")
	public void setBitRate(Integer bitRate) {
		this.bitRate = bitRate;
	}

	@JsonProperty("NumberOfChannels")
	public String getNumberOfChannels() {
		return numberOfChannels;
	}

	@JsonProperty("NumberOfChannels")
	public void setNumberOfChannels(String numberOfChannels) {
		this.numberOfChannels = numberOfChannels;
	}

	@JsonProperty("Duration")
	public Integer getDuration() {
		return duration;
	}

	@JsonProperty("Duration")
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@JsonProperty("FileSize")
	public Integer getFileSize() {
		return fileSize;
	}

	@JsonProperty("FileSize")
	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}

	@JsonProperty("FilePath")
	public String getFilePath() {
		return filePath;
	}

	@JsonProperty("FilePath")
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@JsonProperty("FileName")
	public String getFileName() {
		return fileName;
	}

	@JsonProperty("FileName")
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TechnicalSoundRecordingFileDetail [codec=" + codec + ", samplingRate=" + samplingRate + ", bitRate="
				+ bitRate + ", numberOfChannels=" + numberOfChannels + ", duration=" + duration + ", fileSize="
				+ fileSize + ", filePath=" + filePath + ", fileName=" + fileName + ", additionalProperties="
				+ additionalProperties + "]";
	}

}
