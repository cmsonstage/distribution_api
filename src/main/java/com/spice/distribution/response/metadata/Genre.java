
package com.spice.distribution.response.metadata;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Genre", "GenreImageCode" })
public class Genre {

	@JsonProperty("Genre")
	private String genre;
	@JsonProperty("GenreImageCode")
	private String genreImageCode;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Genre")
	public String getGenre() {
		return genre;
	}

	@JsonProperty("Genre")
	public void setGenre(String genre) {
		this.genre = genre;
	}

	@JsonProperty("GenreImageCode")
	public String getGenreImageCode() {
		return genreImageCode;
	}

	@JsonProperty("GenreImageCode")
	public void setGenreImageCode(String genreImageCode) {
		this.genreImageCode = genreImageCode;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Genre [genre=" + genre + ", genreImageCode=" + genreImageCode + ", additionalProperties="
				+ additionalProperties + "]";
	}

}
