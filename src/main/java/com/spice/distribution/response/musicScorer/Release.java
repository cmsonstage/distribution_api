package com.spice.distribution.response.musicScorer;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Title", "UPC", "ReleaseType", "ReleaseImageUrls" })
public class Release {

	/** The title. */
	@JsonProperty("Title")
	private String title;

	/** The u PC. */
	@JsonProperty("UPC")
	private String uPC;

	/** The release type. */
	@JsonProperty("ReleaseType")
	private String releaseType;

	/** The release image urls. */
	@JsonProperty("ReleaseImageUrls")
	private ReleaseImageUrls releaseImageUrls;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	@JsonProperty("Title")
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	@JsonProperty("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the upc.
	 *
	 * @return the upc
	 */
	@JsonProperty("UPC")
	public String getUPC() {
		return uPC;
	}

	/**
	 * Sets the upc.
	 *
	 * @param uPC the new upc
	 */
	@JsonProperty("UPC")
	public void setUPC(String uPC) {
		this.uPC = uPC;
	}

	/**
	 * Gets the release type.
	 *
	 * @return the release type
	 */
	@JsonProperty("ReleaseType")
	public String getReleaseType() {
		return releaseType;
	}

	/**
	 * Sets the release type.
	 *
	 * @param releaseType the new release type
	 */
	@JsonProperty("ReleaseType")
	public void setReleaseType(String releaseType) {
		this.releaseType = releaseType;
	}

	/**
	 * Gets the release image urls.
	 *
	 * @return the release image urls
	 */
	@JsonProperty("ReleaseImageUrls")
	public ReleaseImageUrls getReleaseImageUrls() {
		return releaseImageUrls;
	}

	/**
	 * Sets the release image urls.
	 *
	 * @param releaseImageUrls the new release image urls
	 */
	@JsonProperty("ReleaseImageUrls")
	public void setReleaseImageUrls(ReleaseImageUrls releaseImageUrls) {
		this.releaseImageUrls = releaseImageUrls;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Release [title=" + title + ", uPC=" + uPC + ", releaseType=" + releaseType + ", releaseImageUrls="
				+ releaseImageUrls + ", additionalProperties=" + additionalProperties + "]";
	}


}