package com.spice.distribution.response.musicScorer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Artwork", "OriginalTrack", "Karaoke", "Lyrics" })
public class ResourceFiles {
	@JsonProperty("Artwork")
	private String artwork;
	@JsonProperty("OriginalTrack")
	private String originalTrack;
	@JsonProperty("Karaoke")
	private String karaoke;
	@JsonProperty("Lyrics")
	private String lyrics;
	/**
	 * @return the artwork
	 */
	@JsonProperty("Artwork")
	public String getArtwork() {
		return artwork;
	}
	/**
	 * @param artwork the artwork to set
	 */
	@JsonProperty("Artwork")
	public void setArtwork(String artwork) {
		this.artwork = artwork;
	}
	/**
	 * @return the originalTrack
	 */
	@JsonProperty("OriginalTrack")
	public String getOriginalTrack() {
		return originalTrack;
	}
	/**
	 * @param originalTrack the originalTrack to set
	 */
	@JsonProperty("OriginalTrack")
	public void setOriginalTrack(String originalTrack) {
		this.originalTrack = originalTrack;
	}
	/**
	 * @return the karaoke
	 */
	@JsonProperty("Karaoke")
	public String getKaraoke() {
		return karaoke;
	}
	/**
	 * @param karaoke the karaoke to set
	 */
	@JsonProperty("Karaoke")
	public void setKaraoke(String karaoke) {
		this.karaoke = karaoke;
	}
	/**
	 * @return the lyrics
	 */
	@JsonProperty("Lyrics")
	public String getLyrics() {
		return lyrics;
	}
	/**
	 * @param lyrics the lyrics to set
	 */
	@JsonProperty("Lyrics")
	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResourceFiles [artwork=" + artwork + ", originalTrack=" + originalTrack + ", karaoke=" + karaoke
				+ ", lyrics=" + lyrics + "]";
	}
	
}
