package com.spice.distribution.response.musicScorer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.spice.distribution.response.Response;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ResourceList" })
@XmlRootElement
public class MusicScorerResponse implements Response {
	

		/** The resource list. */
		@JsonProperty("ResourceList")
		private List<ResourceList> resourceList = null;

		/** The additional properties. */
		@JsonIgnore
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		/**
		 * Gets the resource list.
		 *
		 * @return the resource list
		 */
		@JsonProperty("ResourceList")
		public List<ResourceList> getResourceList() {
			return resourceList;
		}

		/**
		 * Sets the resource list.
		 *
		 * @param resourceList the new resource list
		 */
		@JsonProperty("ResourceList")
		public void setResourceList(List<ResourceList> resourceList) {
			this.resourceList = resourceList;
		}

		/**
		 * Gets the additional properties.
		 *
		 * @return the additional properties
		 */
		@JsonAnyGetter
		public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
		}

		/**
		 * Sets the additional property.
		 *
		 * @param name the name
		 * @param value the value
		 */
		@JsonAnySetter
		public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		
	
		@Override
		public String toString() {
			return "MusicScorerResponse [resourceList=" + resourceList + ", additionalProperties="
					+ additionalProperties + "]";
		}

	}
