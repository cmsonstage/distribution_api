package com.spice.distribution.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ankit
 *
 */
@XmlRootElement
public class Respond implements Response {
	private boolean status;
	private String message;

	/**
	 * @param status
	 * @param message
	 */
	public Respond(boolean status) {
		this.status = status;
	}

	/**
	 * 
	 */
	public Respond() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param status
	 * @param message
	 */
	public Respond(boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
