/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spice.distribution.response.urls;

/**
 *
 * @author jitender.kumar
 */
public class Mp3Urls {

	String auto;
	String high;
	String medium;
	String low;

	public Mp3Urls() {
	}

	public Mp3Urls(String resourceCode) {
		String path = "", streamUrl = "http://skcdn.scontentzone.com/";
		// String imageUrl =
		// "http://cdn.mziiki.com/052/0000/001/07/052000000107005_0.mp3";

		try {
			path = resourceCode.substring(0, 3) + "/" + resourceCode.substring(3, 7) + "/" + resourceCode.substring(7, 10) + "/07/";
			// imageUrl = "http://cdn.mziiki.com/" + imageCode.substring(0, 3) + "/" +
			// imageCode.substring(3, 7) + "/" + imageCode.substring(7, 10) + "/08/";
			auto = streamUrl + path + resourceCode + "07057_0.mp3";
			high = streamUrl + path + resourceCode + "07057_0.mp3";
			medium = streamUrl + path + resourceCode + "07056_0.mp3";
			low = streamUrl + path + resourceCode + "07078_0.mp3";
		} catch (Exception e) {

		}
		// imageUrl = imageUrl.substring(0, imageUrl.length() - 9);

	}

	public void setMp3Urls(String resourceCode) {
		String path = "", streamUrl = "http://skcdn.scontentzone.com/";
		// String imageUrl =
		// "http://cdn.mziiki.com/052/0000/001/07/052000000107005_0.mp3";

		try {
			path = resourceCode.substring(0, 3) + "/" + resourceCode.substring(3, 7) + "/" + resourceCode.substring(7, 10) + "/07/";
			// imageUrl = "http://cdn.mziiki.com/" + imageCode.substring(0, 3) + "/" +
			// imageCode.substring(3, 7) + "/" + imageCode.substring(7, 10) + "/08/";
			auto = streamUrl + path + resourceCode + "07057_0.mp3";
			high = streamUrl + path + resourceCode + "07057_0.mp3";
			medium = streamUrl + path + resourceCode + "07056_0.mp3";
			low = streamUrl + path + resourceCode + "07078_0.mp3";
		} catch (Exception e) {

		}
		// imageUrl = imageUrl.substring(0, imageUrl.length() - 9);

	}

	public String getAuto() {
		return auto;
	}

	public void setAuto(String auto) {
		this.auto = auto;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getLow() {
		return low;
	}

	public void setLow(String low) {
		this.low = low;
	}

}
