package com.spice.distribution.response.urls;

import java.util.List;
import java.util.Objects;

import com.spice.distribution.service.Url;
import com.spice.distribution.util.ObjectsUtil;

/**
 * @author ankit
 * @param <T>
 *
 */
public final class AbstractImageUrlsCnema2 implements Url<UrlsGetter> {

	//private String baseUrl = "http://cdn.mziiki.com/";
	private String baseUrl = "http://cdn.cnema.com/";
	
	private String imageCode;
	private String defaultSeparator = "/";
	private String xsBase = "08125_0.jpg";
	private String xsBaseCnema ="_04_1506_cvrart.jpg";
	private String sBase = "08126_0.jpg";
	private String sBaseCnema ="_04_1507_cvrart.jpg";
	private String mBase = "08127_0.jpg";
	private String mBaseCnema ="_04_1508_cvrart.jpg";
	private String lBase = "08128_0.jpg";
	private String lBaseCnema ="_04_1509_cvrart.jpg";
	private List<String> resourceCodes;

	/**
	 * 
	 */
	public AbstractImageUrlsCnema2(String code) {
		this(null, code);
	}

	public AbstractImageUrlsCnema2(String baseUrl, String code) {
		if (Objects.nonNull(baseUrl))
			this.baseUrl = baseUrl;
		ObjectsUtil.requireNonNull(code, "Image Code is null or empty");
		if (!ObjectsUtil.isResourceCodeValid(code))
			throw new RuntimeException("Invalid image Code : " + code);
		this.imageCode = code;
		this.resourceCodes = ObjectsUtil.breakResourceCode(imageCode, "Invalid Image Code : " + code);
	}

	@Override
	public UrlsGetter create(int serviceId) {
		if (imageCode.equalsIgnoreCase("7070872448"))
			imageCode = "7070926185";
		String imageUrl = new String();
		UrlsGetter getter = new UrlsGetter();
		if(serviceId==221) {
			getter=createCnema();
		}else {
		imageUrl = baseUrl + resourceCodes.get(0) + defaultSeparator + resourceCodes.get(1) + defaultSeparator + resourceCodes.get(2) + defaultSeparator + "08" + defaultSeparator;
		
		getter.setXs(imageUrl + imageCode + xsBase);
		getter.setS(imageUrl + imageCode + sBase);
		getter.setM(imageUrl + imageCode + mBase);
		getter.setL(imageUrl + imageCode + lBase);
		}
		return getter;
	}

	
	
	

	public UrlsGetter createCnema() {
		String imageUrl = new String();
		imageUrl = baseUrl + resourceCodes.get(0) + defaultSeparator + resourceCodes.get(1) + defaultSeparator + resourceCodes.get(2) + defaultSeparator + "08" + defaultSeparator;
		UrlsGetter getter = new UrlsGetter();
		getter.setCxs(imageUrl + imageCode + xsBaseCnema);
		getter.setCs(imageUrl + imageCode + sBaseCnema);
		getter.setCm(imageUrl + imageCode + mBaseCnema);
		getter.setCl(imageUrl + imageCode + lBaseCnema);
		return getter;
	}

	public String getSeparator() {
		return defaultSeparator;
	}

	public void setSeparator(String dEFAULT_SEPARATOR) {
		defaultSeparator = dEFAULT_SEPARATOR;
	}

	public String getXsBase() {
		return xsBase;
	}

	public void setXsBase(String xsBase) {
		this.xsBase = xsBase;
	}

	public String getsBase() {
		return sBase;
	}

	public void setsBase(String sBase) {
		this.sBase = sBase;
	}

	public String getmBase() {
		return mBase;
	}

	public void setmBase(String mBase) {
		this.mBase = mBase;
	}

	public String getlBase() {
		return lBase;
	}

	public void setlBase(String lBase) {
		this.lBase = lBase;
	}

}
