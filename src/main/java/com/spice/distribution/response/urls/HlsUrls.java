/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spice.distribution.response.urls;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author jitender.kumar
 */
public class HlsUrls {
	String auto;
	String high;
	String medium;
	String low;

	public HlsUrls() {
	}

	public void setHlsUrls(String resourceCode) {
		String path = "", streamUrl = "http://skabr.scontentzone.com/aod/_definst_/mp4:";
		try {
			path = resourceCode.substring(0, 3) + "/" + resourceCode.substring(3, 7) + "/" + resourceCode.substring(7, 10) + "/07/";

			byte[] message = ("amazons3/" + path + resourceCode + "07064_0.m4a").getBytes("UTF-8");
			String encoded = DatatypeConverter.printBase64Binary(message);

			auto = streamUrl + encoded + "/playlist.m3u8";

			message = ("amazons3/" + path + resourceCode + "07063_0.m4a").getBytes("UTF-8");
			encoded = DatatypeConverter.printBase64Binary(message);

			high = streamUrl + encoded + "/playlist.m3u8";

			message = ("amazons3/" + path + resourceCode + "07064_0.m4a").getBytes("UTF-8");
			encoded = DatatypeConverter.printBase64Binary(message);

			medium = streamUrl + encoded + "/playlist.m3u8";

			message = ("amazons3/" + path + resourceCode + "07112_0.m4a").getBytes("UTF-8");
			encoded = DatatypeConverter.printBase64Binary(message);

			low = streamUrl + encoded + "/playlist.m3u8";

			//            this.high = streamUrl + Base64.getEncoder().encodeToString(("amazons3/" + path + resourceCode + "07063_0.m4a").getBytes("utf-8")) + "/playlist.m3u8";
			//            this.medium = streamUrl + Base64.getEncoder().encodeToString(("amazons3/" + path + resourceCode + "07064_0.m4a").getBytes("utf-8")) + "/playlist.m3u8";
			//            this.low = streamUrl + Base64.getEncoder().encodeToString(("amazons3/" + path + resourceCode + "07112_0.m4a").getBytes("utf-8")) + "/playlist.m3u8";
		} catch (UnsupportedEncodingException ex) {
			Logger.getLogger(AbrUrls.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public String getAuto() {
		return auto;
	}

	public void setAuto(String auto) {
		this.auto = auto;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getLow() {
		return low;
	}

	public void setLow(String low) {
		this.low = low;
	}

}
