package com.spice.distribution.response.urls;

/**
 * @author ankit
 *
 */
public final class UrlsGetter {
	protected String xs;
	protected String s;
	protected String m;
	protected String l;
	protected String fullHighDefinition;
	protected String highDefinition;
	protected String medium;
	protected String low;
	protected String adaptiveBitrate;
	protected String standard;
	protected String downloadMedium;
	protected String downloadLow;
	protected String veryLow;
	protected String standards;
	protected String cxs;
	protected String cs;
	protected String cm;
	protected String cl;
	/**
	 * @return the veryLow
	 */
	public String getVeryLow() {
		return veryLow;
	}

	/**
	 * @param veryLow the veryLow to set
	 */
	public void setVeryLow(String veryLow) {
		this.veryLow = veryLow;
	}

	protected void setXs(String xs) {
		this.xs = xs;
	}

	protected void setS(String s) {
		this.s = s;
	}

	protected void setM(String m) {
		this.m = m;
	}

	protected void setL(String l) {
		this.l = l;
	}

	public String getXs() {
		return xs;
	}

	public String getS() {
		return s;
	}

	public String getM() {
		return m;
	}

	public String getL() {
		return l;
	}

	protected void setFullHighDefinition(String fullHighDefinition) {
		this.fullHighDefinition = fullHighDefinition;
	}

	protected void setHighDefinition(String highDefinition) {
		this.highDefinition = highDefinition;
	}

	protected void setMedium(String medium) {
		this.medium = medium;
	}

	protected void setLow(String low) {
		this.low = low;
	}

	protected void setAdaptiveBitrate(String adaptiveBitrate) {
		this.adaptiveBitrate = adaptiveBitrate;
	}

	public String getFullHighDefinition() {
		return fullHighDefinition;
	}

	public String getHighDefinition() {
		return highDefinition;
	}

	public String getMedium() {
		return medium;
	}

	public String getLow() {
		return low;
	}

	public String getAdaptiveBitrate() {
		return adaptiveBitrate;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getDownloadMedium() {
		return downloadMedium;
	}

	public void setDownloadMedium(String downloadMedium) {
		this.downloadMedium = downloadMedium;
	}

	public String getDownloadLow() {
		return downloadLow;
	}

	public void setDownloadLow(String downloadLow) {
		this.downloadLow = downloadLow;
	}

	/**
	 * @return the standards
	 */
	public String getStandards() {
		return standards;
	}

	/**
	 * @param standards the standards to set
	 */
	public void setStandards(String standards) {
		this.standards = standards;
	}

	/**
	 * @return the cxs
	 */
	public String getCxs() {
		return cxs;
	}

	/**
	 * @param cxs the cxs to set
	 */
	public void setCxs(String cxs) {
		this.cxs = cxs;
	}

	/**
	 * @return the cs
	 */
	public String getCs() {
		return cs;
	}

	/**
	 * @param cs the cs to set
	 */
	public void setCs(String cs) {
		this.cs = cs;
	}

	/**
	 * @return the cm
	 */
	public String getCm() {
		return cm;
	}

	/**
	 * @param cm the cm to set
	 */
	public void setCm(String cm) {
		this.cm = cm;
	}

	/**
	 * @return the cl
	 */
	public String getCl() {
		return cl;
	}

	/**
	 * @param cl the cl to set
	 */
	public void setCl(String cl) {
		this.cl = cl;
	}

}
