package com.spice.distribution.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.common.collect.Lists;
import com.spice.distribution.cms.entity.ReleaseResourceDisplayArtists;
import com.spice.distribution.cms.entity.ReleaseResourceGenres;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.entity.Releases;
import com.spice.distribution.cms.entity.SoundRecordings;
import com.spice.distribution.cms.entity.TechnicalSoundRecordingFileDetails;
import com.spice.distribution.entity.DistributionTransactionReleases;
import com.spice.distribution.entity.DistributionTransactionResources;
import com.spice.distribution.entity.Distributions;

/**
 * @author ankit
 *
 */
public interface Filter {
	Integer MAINARTISTID = 102;

	public static List<Integer> filterReleaseId(Collection<Releases> releasesList) {
		return releasesList.stream().map(Releases::getId).collect(Collectors.toList());
	}

	public static <T> List<T> convertResource(Iterator<T> iterator) {
		return Lists.newArrayList(iterator);
	}

	public static void setTransactionId(Set<DistributionTransactionResources> resources, int id) {
		for (DistributionTransactionResources resource : resources)
			resource.setDistribution_transaction_id(id);
	}

	public static void setTransactionIdForRelease(Set<DistributionTransactionReleases> resources, int id) {
		for (DistributionTransactionReleases resource : resources)
			resource.setDistribution_transaction_id(id);
	}

	public static List<Integer> filterTransactionId(Iterable<Distributions> ids) {
		Stream<Distributions> stream = StreamSupport.stream(ids.spliterator(), false);
		return stream.map(Distributions::getId).collect(Collectors.toList());
	}

	public static List<Integer> filterReleaseResourceId(List<ReleaseResources> releaseResourcesList) {
		return releaseResourcesList.stream().map(ReleaseResources::getId).collect(Collectors.toList());
	}

	public static Iterable<Integer> filterReleaseId(List<ReleaseResources> releaseResourcesList) {
		return releaseResourcesList.stream().map(ReleaseResources::getReleaseId).collect(Collectors.toList());
	}

	public static List<ReleaseResourceGenres> filterReleaseResourceGenresList(int resourceId, List<ReleaseResourceGenres> releaseResourceGenresList) {
		return releaseResourceGenresList.stream().filter(e -> e.getReleaseResourceId().equals(resourceId)).collect(Collectors.toList());
	}

	public static List<Integer> filterGenreId(List<ReleaseResourceGenres> releaseResourceGenresList) {
		return releaseResourceGenresList.stream().map(ReleaseResourceGenres::getGenre_id).collect(Collectors.toList());
	}

	public static List<ReleaseResourceDisplayArtists> filterReleaseResourceDisplayArtists(int resourceId, List<ReleaseResourceDisplayArtists> releaseResourceDisplayArtistsList) {
		return releaseResourceDisplayArtistsList.stream().filter(e -> e.getReleaseResourceId().equals(resourceId)).collect(Collectors.toList());
	}

	public static List<Integer> filterDisplayArtistId(List<ReleaseResourceDisplayArtists> releaseResourceDisplayArtistsList) {
		return releaseResourceDisplayArtistsList.stream().map(ReleaseResourceDisplayArtists::getDisplay_artist_id).collect(Collectors.toList());
	}

	public static Optional<Releases> filterReleases(int releaseId, List<Releases> releasesList) {
		return releasesList.stream().filter(e -> e.getId() == releaseId).findFirst();
	}

	public static Optional<TechnicalSoundRecordingFileDetails> findDuration(List<TechnicalSoundRecordingFileDetails> list) {
		final Comparator<TechnicalSoundRecordingFileDetails> comp = (p1, p2) -> Long.compare(p1.getDuration(), p2.getDuration());
		return list.stream().max(comp);
	}

	public static SoundRecordings filterSoundRecordings(Set<SoundRecordings> list, int releaseResourceId) {
		return list.stream().filter(e -> e.getRelease_resource_id() == releaseResourceId).findFirst().orElse(null);
	}

	public static Optional<SoundRecordings> findDurationForCMS(Set<SoundRecordings> list) {
		final Comparator<SoundRecordings> comp = (p1, p2) -> Long.compare(p1.getDuration(), p2.getDuration());
		return list.stream().max(comp);
	}

	public static boolean isNullOrEmpty(String toTest) {
		return (toTest == null || toTest.length() == 0);
	}

}
