package com.spice.distribution.util;

/**
 * @author ankit
 *
 */
public enum DBType {
	CMS, DISTRIBUTION;
}
