/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 *   Utility class for I/O related work.
 *
 * @author ankit
 */
public abstract class ObjectsUtil {

	/** The Constant LENGTH_OF_CODE.  representing length of any unique code define in Horizon
	 *  example ResourceCode,ArtistImage Code,  GenreImageCode etc.
	 */
	private final static int LENGTH_OF_CODE = 10;

	/**
	 * Checks if is null or empty.
	 *
	 * @param object a reference to be checked against {@code null}
	 * @return {@code true} if the provided reference is {@code null} otherwise
	 *         returns {@code false}.
	 */
	public static boolean isNullOrEmpty(Object object) {
		if (object instanceof String)
			return isNullOrEmpty((String) object);

		return Objects.isNull(object);
	}

	/**
	 * Checks if is null or empty.
	 *
	 * @param <T> the generic type
	 * @param collection the collection
	 * @return true, if is null or empty
	 */
	public static <T> boolean isNullOrEmpty(Collection<T> collection) {
		return collection == null || collection.isEmpty();
	}

	/**
	 * String is null or empty.
	 *
	 * @param string a reference to be checked against {@code null} or {@code empty}
	 * @return {@code true} if the provided reference is {@code null} or
	 *         {@code empty} otherwise returns {@code false}.
	 */
	public static boolean isNullOrEmpty(String string) {
		return string == null || string.isEmpty();
	}

	/**
	 * Checks if is not null or empty.
	 *
	 * @param string the string
	 * @return true, if is not null or empty
	 */
	public static boolean isNotNullOrEmpty(String string) {
		return !isNullOrEmpty(string);
	}

	/**
	 * Checks if is numeric.
	 *
	 * @param cs instance to check for numeric valid
	 * @return true is and only if cs is numeric otherwise false
	 */
	public static boolean isNumeric(final CharSequence cs) {
		if (isEmpty(cs))
			return false;

		final int sz = cs.length();
		for (int i = 0; i < sz; i++)
			if (!Character.isDigit(cs.charAt(i)))
				return false;
		return true;
	}

	/**
	 * Checks if is resource code valid.
	 *
	 * @param cs the  String representing code.
	 * @return true, if is resource code valid otherwise false.
	 */
	public static boolean isResourceCodeValid(final CharSequence cs) {
		if (isEmpty(cs))
			return false;
		if (!isNumeric(cs))
			return false;
		if (cs.length() != LENGTH_OF_CODE)
			return false;
		return true;
	}

	/**
	 * Checks if is empty.
	 *
	 * @param cs instance is used to check for null/empty value.
	 * @return true, if is empty
	 */
	private static boolean isEmpty(final CharSequence cs) {
		return cs == null || cs.length() == 0;
	}

	/**
	 * Checks that the specified Object reference is not {@code null} throws a
	 * customized {@link NullPointerException} if it is.
	 * 
	 * @param obj the object reference to check for nullity
	 * @param message detail message to be used in the event that a {@code
	 *                NullPointerException} is thrown
	 */
	public static void requireNonNull(Object object, String message) {
		Objects.requireNonNull(object, message);
	}

	/**
	 * Checks that the specified String reference is not {@code null} and empty and
	 * throws a customized {@link IllegalArgumentException} if it is.
	 * 
	 * @param obj the object reference to check for nullity
	 * @param message detail message to be used in the event that a {@code
	 *                IllegalArgumentException} is thrown
	 */
	public static void requireNonNull(String obj, String message) {
		if (isNullOrEmpty(obj))
			throw new IllegalArgumentException(message);
	}

	/**
	 * Break resource code. into 3-4-3
	 *
	 * @param resourceCode the resource code to e broken.
	 * @return the list containing broken resourceCode.
	 */
	public static List<String> breakResourceCode(String resourceCode, String message) {
		if (!isResourceCodeValid(resourceCode))
			throw new IllegalArgumentException(message);
		List<String> list = new ArrayList<>();
		list.add(resourceCode.substring(0, 3));
		list.add(resourceCode.substring(3, 7));
		list.add(resourceCode.substring(7));
		return list;
	}

}