package com.spice.distribution.util;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.stereotype.Component;

/**
 * @author ankit
 *
 */
@Component
public class EntityManagerUtil {

	@Autowired
	@Qualifier("distributionEntityManager")
	private EntityManager distributionDatabase;

	private EntityManager getDistribution() {
		return distributionDatabase;
	}

	public JpaRepositoryFactory getJpaFactory() {
		return new JpaRepositoryFactory(this.getDistribution());
	}

	public <T> T construct(Class<T> clazz) {
		return getJpaFactory().getRepository(clazz);
	}

}
