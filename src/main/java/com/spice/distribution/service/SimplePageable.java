package com.spice.distribution.service;

import org.springframework.data.domain.Pageable;

/**
 * @author Manish
 *
 */
public interface SimplePageable extends Pageable {
	
	int getFirstResult();

	public int getTotalElements();

	public void setTotalElements(int totalElements);

}
