package com.spice.distribution.service;

import java.math.BigInteger;

import com.spice.distribution.cms.entity.SafariComServicesResponse;
import com.spice.distribution.request.CnemaNotificationRequest;
import com.spice.distribution.response.Response;

public interface NotificationService {
	
	public Response updateRemarkByResources(SafariComServicesResponse request);
	
	public Response updateRemarkByResourcesCnema(CnemaNotificationRequest request);
	
	
}
