/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service;

import com.spice.distribution.response.Response;

/**
 * Base interface for generating Json Response for all the API.
 * 
 * @author ankit
 *
 */
public interface Jsonify<T, R extends Response> {

	/**
	 * Generate the Json.
	 *
	 * @param t the t must be implementing Response
	 * @return the response representing Json.
	 */
	R generate(T t,int serviceId);
	
	R generateKaraoke(T t);
}
