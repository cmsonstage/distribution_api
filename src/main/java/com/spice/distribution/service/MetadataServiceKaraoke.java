package com.spice.distribution.service;



import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.Response;


public interface MetadataServiceKaraoke<T, R> extends Request, Response{
	
	R processMetadataByKaraoke(T t);
		
	R getKaraokeMetadata(String sort,int size,int page);	
	
	R getMetadataForKeeng(int service_id);

}
