package com.spice.distribution.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spice.distribution.cms.entity.Services;
import com.spice.distribution.cms.repo.ServiceRepository;
import com.spice.distribution.entity.DistributionTypes;
import com.spice.distribution.util.RepositoryFactory;

/**
 * @author ankit
 *
 */
@Service("type")
public class DType extends DistributorAdapter {

	@Override
	public Iterable<DistributionTypes> processType() {
		return repositoryFactory.getDistributionType().findAll();
	}

	@Override
	public List<Services> processServices(Integer typeId, Integer categoryId) {
		return serviceRepository.findByServiceTypeIdAndServiceCategoryId(typeId, categoryId);
	}

	@Autowired
	ServiceRepository serviceRepository;
	@Autowired
	RepositoryFactory repositoryFactory;
}
