package com.spice.distribution.service.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.entity.Audio;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.entity.ResponseStatus;
import com.spice.distribution.cms.entity.SafariComServicesResponse;
import com.spice.distribution.cms.entity.ServicePerformanceSummaries;
import com.spice.distribution.cms.repo.ReleaseResourcesRepository;
import com.spice.distribution.cms.repo.ReleasesRepository;
import com.spice.distribution.entity.DistributionTransactionResources;
import com.spice.distribution.request.CnemaNotificationRequest;
import com.spice.distribution.request.ServiceSummariesRequest;
import com.spice.distribution.response.CnemaNotificationResponse;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.NotificationService;
import com.spice.distribution.util.RepositoryFactory;

@Service
public class NotificationServiceImpl implements NotificationService {

	public static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);

	@Autowired
	@Qualifier("distributionEntityManager")
	@PersistenceContext
	private EntityManager em;
	
	

	@Override
	@Transactional
	public Response updateRemarkByResources(SafariComServicesResponse request) {
		// TODO Auto-generated method stub
		List<Audio> listAudio = request.getData().getAudios();
		String resource_code,remarks;
		BigInteger reference_id  = request.getTrackingId();
		LOGGER.debug(" Notification Transaction Id : " + reference_id );
		try {
		for(Audio obj : listAudio) {			
			resource_code = obj.getSafariId();
			remarks = obj.getResponse().getMessage();
			String sql = "update DistributionTransactionResources set remarks = ' "+remarks +" ' where reference_id = " + reference_id + " AND resource_code = " +resource_code  ;
			LOGGER.debug("Sql : " + sql);
			Query q = em.createNativeQuery("update DISTRIBUTION.DistributionTransactionResources set remarks = ' "+remarks+" ' where reference_id = " + reference_id + " AND resource_code = " +resource_code );
			//em.persist(q);
		    int i=q.executeUpdate();
			//repoFactory.getDistributionTransactionResource().updateRemark(reference_id,resource_code);
			//repoFactory.getContentRefreshingTransactionRepository().updateRemark(reference_id,resource_code);
		}
		return request.getResponseStatus();
		}catch (Exception e) {
			ResponseStatus rs = new ResponseStatus();
			rs.setStatusCode("200");
			rs.setStatusMessage("Error");
			LOGGER.debug("Error : "+ e); 
			return rs;
		}
	}

	@Override
	@Transactional
	public Response updateRemarkByResourcesCnema(CnemaNotificationRequest request) {
		
		LOGGER.debug(" Notification Transaction Id : " + request.getRefrenceId() );
		try {
			BigInteger reference_id = request.getRefrenceId();		
			String resource_code = request.getResourceCode();
			String remarks = request.getMessage();
			
				String sql = "update DISTRIBUTION.DistributionTransactionResources set remarks = ' "+remarks +" ' where reference_id = " + reference_id + " AND resource_code = '" +resource_code+"'"  ;
				LOGGER.debug("Sql : " + sql);
				Query q = em.createNativeQuery(sql );
				//em.persist(q);
			    int i=q.executeUpdate();
			    if(i>0)
				return new CnemaNotificationResponse("true","Remarks Updated Successfully .");
			    
			    return new CnemaNotificationResponse("false","ReferenceId && ResouceCode is not exist  .");
		
		}catch (Exception e) {
			
			//System.out.println("Error : " + e.getMessage());
			ResponseStatus rs = new ResponseStatus();
			rs.setStatusCode("200");
			rs.setStatusMessage("Error");
			LOGGER.debug("Error : "+ e); 
			return rs;
			//return new CnemaNotificationResponse("false","Error In Query Execution.");
		}
		
	}
}
