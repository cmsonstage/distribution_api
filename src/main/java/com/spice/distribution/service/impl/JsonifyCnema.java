package com.spice.distribution.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.naming.OperationNotSupportedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import com.google.common.base.Joiner;
import com.spice.distribution.cms.constant.ResourceType;
import com.spice.distribution.cms.entity.ContentPartners;
import com.spice.distribution.cms.entity.DealTermsUsageUseTypes;
import com.spice.distribution.cms.entity.Deals;
import com.spice.distribution.cms.entity.Images;
import com.spice.distribution.cms.entity.ReleaseResourceTags;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.entity.Releases;
import com.spice.distribution.cms.entity.SoundRecordings;
import com.spice.distribution.cms.entity.TechnicalVideoFileDetails;
import com.spice.distribution.cms.entity.UseTypes;
import com.spice.distribution.cms.entity.Videos;
import com.spice.distribution.cms.repo.ArtistRolesRepository;
import com.spice.distribution.cms.repo.ContentPartnersRepository;
import com.spice.distribution.cms.repo.DealRepository;
import com.spice.distribution.cms.repo.DealTermsUsageUseTypesRepository;
import com.spice.distribution.cms.repo.DisplayArtistRepository;
import com.spice.distribution.cms.repo.GenresRepository;
import com.spice.distribution.cms.repo.ReleaseResourcesRepository;
import com.spice.distribution.cms.repo.ReleasesRepository;
import com.spice.distribution.cms.repo.UseTypesRepository;
import com.spice.distribution.exceptions.DistributionException;
import com.spice.distribution.response.cnema.Cnema;
import com.spice.distribution.response.cnema.DisplayArtist;
import com.spice.distribution.response.cnema.DownloadUrls;
import com.spice.distribution.response.cnema.Genre;
import com.spice.distribution.response.cnema.Release;
import com.spice.distribution.response.cnema.ResourceList;
import com.spice.distribution.response.cnema.StreamingUrls;
import com.spice.distribution.response.urls.AbstractImageUrlsCnema2;
import com.spice.distribution.response.urls.AbstractSoundAndVideoUrlsCnema2;
import com.spice.distribution.service.Jsonify;
import com.spice.distribution.service.internal.CommonJson;
import com.spice.distribution.service.internal.Urls;
import com.spice.distribution.util.Filter;
import com.spice.distribution.util.MetadataUtil;
import com.spice.distribution.util.ObjectsUtil;
import com.spice.distribution.util.RepositoryFactory;

/**
 * @author ankit
 *
 */
@Component("cnemaJson")
@Scope(proxyMode = ScopedProxyMode.INTERFACES, value = "prototype")
public class JsonifyCnema extends CommonJson implements Jsonify<Collection<ReleaseResources>, Cnema> {

	private static final String SEPARATOR = ",";
	private int serviceId;

	@Override
	public Cnema generate(Collection<ReleaseResources> t, int serviceId) {
		Objects.requireNonNull(t, "ReleaseResources Not Found");
		Cnema cnema = new Cnema();
		this.serviceId = serviceId;
		cnema.setResourceList(generateResourceList((List<ReleaseResources>) t));
		return cnema;
	}
	
	@Override
	public Cnema generateKaraoke(Collection<ReleaseResources> t) {
		Objects.requireNonNull(t, "ReleaseResources Not Found");
		Cnema cnema = new Cnema();
		//this.serviceId = serviceId;
		cnema.setResourceList(generateResourceList((List<ReleaseResources>) t));
		return cnema;
	}

	private List<ResourceList> generateResourceList(List<ReleaseResources> list) {
		List<ResourceList> resourceList = new ArrayList<>();
		for (ReleaseResources resources : list)
			resourceList.add(this.generateResource(resources));
		return resourceList;
	}

	private ResourceList generateResource(ReleaseResources rr) {
		ResourceList rl = new ResourceList();
		rl.setResourceCode(rr.getResourceCode());
		this.generateSubResource(rr, rl);
		if (Objects.nonNull(rr.getLanguage()))
			rl.setLanguageCodes(Arrays.asList(rr.getLanguage().getIso639_1()));
		rl.setResourceTags(createTag(rr));
		rl.setIsPremium(rr.getIs_premium());
		rl.setPublicationDate(new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a Z").format(new Date()));
		ContentPartners contentPartners = repo.findOne(rr.getContent_partner_id());
		rl.setContentPartner(contentPartners.getContent_partner());
		rl.setUseTypes(this.findUsageRights(rr));
		rl.setCountryOfOrigin(this.generateCountryOfOrigin(contentPartners.getCountries()));
		Releases releases = releasesRepository.findOne(rr.getReleaseId());
		Release release = this.generateReleases(rr, releases);
		this.generateTDG(rr, rl, releases.getId());
		rl.setRelease(release);
		createLog(rl);
		return rl;
	}

	private List<String> findUsageRights(ReleaseResources rr) {
		UseTypesRepository typesRepository=repoFactory.getUseTypesRepository();
		List<String> useTypes=typesRepository.useType(rr.getId());
		if(!ObjectsUtil.isNullOrEmpty(useTypes)) {
			return useTypes;
		}
		/*List<UseTypes> useTypes=
		List<Deals> deals = dealRepository.findByReleaseId(rr.getReleaseId());
		if (!ObjectsUtil.isNullOrEmpty(deals)) {
			DealTermsUsageUseTypesRepository dealTerm = repoFactory.getDealTermsUsageUseTypesRepository();
			List<DealTermsUsageUseTypes> dealTermsUsageUseTypes = dealTerm.findByDealId(deals.get(0).getId());
			if (!ObjectsUtil.isNullOrEmpty(dealTermsUsageUseTypes)) {
				UseTypesRepository useTypesRepositories = repoFactory.getUseTypesRepository();
				UseTypes useTypes = useTypesRepositories.findUseTypeById(dealTermsUsageUseTypes.get(0).getUseTypeId());
				if (!ObjectsUtil.isNullOrEmpty(dealTermsUsageUseTypes))
					return useTypes.getUseType();
			}
		}*/
		return new ArrayList<>();
	}

	private List<String> createTag(ReleaseResources rr) {
		List<ReleaseResourceTags> resourceTags = repoFactory.getReleaseResourceTags()
				.findByReleaseResourceId(rr.getId());
		if (!ObjectsUtil.isNullOrEmpty(resourceTags)) {
			List<String> tagList = new ArrayList<>();
			for (ReleaseResourceTags tags : resourceTags) {
				if (Objects.nonNull(tags.getTags()))
					tagList.add(tags.getTags().getTag().trim());
			}
			return tagList;
		}
		message.add("Resource Tag not Found");
		return new ArrayList<>();
	}

	private void createLog(ResourceList rl) {
		if (message.isEmpty()) {
			rl.setStatus(true);
			rl.setMessage("Successfull");
		} else {
			rl.setMessage(Joiner.on(SEPARATOR).join(message));
			message.clear();
		}
	}

	public StreamingUrls generateStreamingUrls(ReleaseResources rr) {
		Urls urls = new Urls(new AbstractSoundAndVideoUrlsCnema2(rr.getResourceCode()), serviceId);
		Set<Videos> videos = rr.getVideos();
		Optional<Videos> video = videos.stream().findFirst();
		List<TechnicalVideoFileDetails> technicalVideoFileDetails = repoFactory.getTechnicalVideoFileRepository()
				.findByVideoIdOrderByDurationDesc(video.get().getId());
		if (!validateAllTechnicaldetails(technicalVideoFileDetails))
			message.add("StreamingUrls Format missing");
		return urls.generateStreamingUrls();
	}

	private boolean validateAllTechnicaldetails(List<TechnicalVideoFileDetails> technicalVideoFileDetails) {
		boolean isExist = true;
		if (!ObjectsUtil.isNullOrEmpty(technicalVideoFileDetails)) {
			List<Integer> techIds = new ArrayList<Integer>(Arrays.asList(197, 198, 199, 200, 201, 202));
			for (Integer techId : techIds) {
				if (!technicalVideoFileDetails.stream()
						.anyMatch(tehchvideo -> tehchvideo.getTechnicalVideoDetailsId().equals(techId)))
					return false;
			}
		} else {
			isExist = false;
		}
		return isExist;
	}

	public DownloadUrls generateDownloadUrls(String resourceCode) {
		Urls urls = new Urls(new AbstractSoundAndVideoUrlsCnema2(resourceCode), serviceId);
		return urls.generateDownloadUrls();
	}

	public Release generateReleases(ReleaseResources rr, Releases releases) {
		Release release = new Release();
		release.setTitle(releases.getTitle_text());
		release.setUPC(releases.getUpc());
		release.setReleaseType(releases.getRelease_type().getRelease_type());
		//Integer count = repoFactory.getTechnicalImageFileRepository().count(rr.getId(), Arrays.asList(198));
		Integer count = repoFactory.getTechnicalImageFileRepository().count(releases.getId(), Arrays.asList(2,3,6,7));
		//System.out.println("count : " + count);
//		if (count == null || count < Arrays.asList(21, 80).size()) {
//			message.add("ReleaseImageUrls technical details is missing");
//		}
		
		if (count == null || count < Arrays.asList(2,3,6,7).size()) {
			message.add("ReleaseImageUrls technical details is missing");
		}
		
		List<ReleaseResources> releaseResourcesImageCode = releaseResourcesRepository.findByReleaseId(releases.getId());
		for (ReleaseResources imageCode : releaseResourcesImageCode) {
			if (imageCode.getResource_type_id().equals(ResourceType.Image.getId())) {
				if (ObjectsUtil.isNotNullOrEmpty(imageCode.getResourceCode())) {
					Urls urls = new Urls(new AbstractImageUrlsCnema2(imageCode.getResourceCode()), serviceId);
					release.setReleaseImageUrls(urls.generateReleaseImageUrlsCnema());
					release.setReleasePosterUrls(urls.generateReleasePosterUrlsCnema());
				}
			}
		}
		return release;
	}

	private void genrerateSound(ReleaseResources rr, ResourceList rl) {
		rl.setResourceType(ResourceType.SoundRecording.name());
		for (SoundRecordings recordings : rr.getSoundRecordings()) {
			rl.setISRC(recordings.getIsrc());
			rl.setTitle(recordings.getTitle_text());
			rl.setSubTitle(recordings.getSub_title());
			rl.setResourceSubType(recordings.getSoundRecordingTypes().getSound_recording_type());
			rl.setDuration(repoFactory.getTechnicalSoundFileRepository().maxDuration(recordings.getId()));
		}
	}

	private void generateImage(ReleaseResources rr, ResourceList rl) {
		rl.setResourceType(ResourceType.Image.name());
		for (Images image : rr.getImages()) {
			rl.setTitle(image.getTitle_text());
			rl.setSubTitle(image.getSub_title());
			rl.setResourceSubType(image.getImageTypes().getImage_type());
		}
	}

	private void generateVideo(ReleaseResources rr, ResourceList rl) {
		rl.setResourceType(ResourceType.Video.name());
		for (Videos video : rr.getVideos()) {
			rl.setISRC(video.getIsrc());
			rl.setTitle(video.getTitle_text());
			rl.setSubTitle(video.getSub_title());
			rl.setResourceSubType(video.getVideoTypes().getVideo_type());
			rl.setDuration(repoFactory.getTechnicalVideoFileRepository().maxDuration(video.getId()));
		}
	}

	private void generateText(ReleaseResources rr, ResourceList rl) {
		throw new DistributionException(new OperationNotSupportedException("Text is not Supported in this Version"));
	}

	private void generateSubResource(ReleaseResources rr, ResourceList rl) {
		if ((rr.getResource_type_id().equals(ResourceType.SoundRecording.getId()))) {
			Integer count = repoFactory.getTechnicalSoundFileRepository().count(rr.getId(),
					Arrays.asList(33, 192, 53, 196, 192, 196, 200));
			if (count == null || count < Arrays.asList(33, 192, 53, 196, 192, 196, 200).size()) {
				message.add("sound Recording technical details is missing");
			}
			this.genrerateSound(rr, rl);
		} else if ((rr.getResource_type_id().equals(ResourceType.Image.getId())))
			this.generateImage(rr, rl);
		else if ((rr.getResource_type_id().equals(ResourceType.Video.getId()))) {
			this.generateVideo(rr, rl);
			Integer count = repoFactory.getTechnicalVideoFileRepository()
					.countByreleaseResourceIdAndtechnicalVideoDetailsId(rr.getId(),
							(List<Integer>) Arrays.asList(197, 198, 199, 200, 201, 202));
			
			if (count == null || count < Arrays.asList(197, 198, 199, 200, 201, 202).size()) {
				message.add("video technical details is missing");
			}
			rl.setStreamingUrls(generateStreamingUrls(rr));
			rl.setDownloadUrls(generateDownloadUrls(rr.getResourceCode()));
		} else if ((rr.getResource_type_id().equals(ResourceType.Text.getId())))
			this.generateText(rr, rl);
	}

	public void generateTDG(ReleaseResources rr, ResourceList resourceList, int id) {
		resourceList.setTerritoryCodes(this.generateTerritoryCodes(dealRepository.findByReleaseId(id)));
		resourceList.setDisplayArtists(this.generateDisplayArtist(rr, resourceList));
		resourceList.setGenres(this.generateGenre(rr, resourceList));
	}

	public List<DisplayArtist> generateDisplayArtist(ReleaseResources rr, ResourceList rl) {
		List<DisplayArtist> displayArtistList = new ArrayList<>();
		List<Object[]> displayArtistName = displayArtistRepository.findByReleaseResourceId(rr.getId());
		List<String> artistRole = artistRolesRepository.findByReleaseResourceId(rr.getId());
		for (int i = 0; i < displayArtistName.size(); i++) {
			if (i <= artistRole.size()) {
				String artist = artistRole.get(i);
				if (!Filter.isNullOrEmpty(artist) && artist.equalsIgnoreCase("MainArtist")
						|| artist.equalsIgnoreCase("FeaturedArtist") || artist.equalsIgnoreCase("Actor") || artist.equalsIgnoreCase("Director")) {
					DisplayArtist displayArtist = new DisplayArtist();
					displayArtist.setName((String) displayArtistName.get(i)[0]);
					String imageCode = (String) displayArtistName.get(i)[1];
					if (ObjectsUtil.isNotNullOrEmpty(imageCode)) {
						Urls urls = new Urls(new AbstractImageUrlsCnema2(imageCode), 0);
						displayArtist.setArtistImageUrls(urls.generateArtistImageUrls());
					}
					displayArtist.setRole(artist);
					displayArtistList.add(displayArtist);
				}
			}
		}
		if (Objects.isNull(displayArtistList) && displayArtistList.isEmpty())
			message.add("Display Artist Not Found");
		return displayArtistList;
	}

	public List<Genre> generateGenre(ReleaseResources rr, ResourceList rl) {
		List<Genre> genreList = new ArrayList<>();
		List<Object[]> genres = genresRepository.findByReleaseResourceId(rr.getId());
		for (Object[] obj : genres) {
			Genre genre = new Genre();
			genre.setGenre((String) obj[0]);
			String imageCode = (String) obj[1];
			if (ObjectsUtil.isNotNullOrEmpty(imageCode)) {
				Urls urls = new Urls(new AbstractImageUrlsCnema2(imageCode), 0);
				genre.setGenreImageUrls(urls.generateGenreImageUrls());
			}
			genreList.add(genre);
		}
		if (Objects.isNull(genreList) && genreList.isEmpty())
			message.add("Genre Not Found");
		return genreList;
	}

	@Autowired
	private GenresRepository genresRepository;
	@Autowired
	private ArtistRolesRepository artistRolesRepository;
	@Autowired
	private DisplayArtistRepository displayArtistRepository;
	@Autowired
	private ContentPartnersRepository repo;
	@Autowired
	private ReleasesRepository releasesRepository;
	@Autowired
	private DealRepository dealRepository;
	@Autowired
	private ReleaseResourcesRepository releaseResourcesRepository;
	@Autowired
	private RepositoryFactory repoFactory;

}
