package com.spice.distribution.service.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.entity.Releases;
import com.spice.distribution.cms.repo.ReleaseResourcesRepository;
import com.spice.distribution.cms.repo.ReleasesRepository;
import com.spice.distribution.request.MetadataRequest;
import com.spice.distribution.request.MetadataRequestISRC;
import com.spice.distribution.response.Respond;
import com.spice.distribution.response.Response;
import com.spice.distribution.response.cnema.Cnema;
import com.spice.distribution.response.metadata.MetadataResponse;
import com.spice.distribution.response.metadata.ResourceList;
import com.spice.distribution.response.musicScorer.MusicScorerResponse;
import com.spice.distribution.response.safaricom.Audio;
import com.spice.distribution.response.safaricom.SafariComResponse;
import com.spice.distribution.service.Metadata;
import com.spice.distribution.service.internal.JsonMapper;
import com.spice.distribution.util.RepositoryFactory;

/**
 * @author ankit
 *
 */
@Component("cms")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "prototype")
public class CMSMetadata implements Metadata<Response, MetadataRequest> {
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Response processByResource(MetadataRequest t) {
				
		List<ReleaseResources> releaseResources = repo.findByResourceCodeIn(t.getResources());
		if (!releaseResources.isEmpty())
			return doCheck(releaseResources, t, mapper.getMapper(t.getServiceId()).generate((Collection) releaseResources,t.getServiceId()));
		return new Respond(false, "Resource Codes Not Found");

	}

	public Response doCheck(List<ReleaseResources> releaseResources, MetadataRequest t, Response response) {
		for (String r : t.getResources()) {
			boolean status = this.check(releaseResources, r.trim());
			if (!status) {
				if (response instanceof SafariComResponse)
					addError(status, (SafariComResponse) response, r);
				else if (response instanceof MetadataResponse)
					addError(status, (MetadataResponse) response, r);
				else if (response instanceof Cnema)
					addError(status, (Cnema) response, r);
				else if (response instanceof MusicScorerResponse)
					addError(status, (MusicScorerResponse) response, r);
			}else {
				if (response instanceof SafariComResponse)
					DistributionApplication.LOGGER.debug("Final Response "+((SafariComResponse)response).toString());
				else if (response instanceof MetadataResponse)
					DistributionApplication.LOGGER.debug("Final Response "+((MetadataResponse)response).toString());
				else if (response instanceof Cnema)
					DistributionApplication.LOGGER.debug("Final Response "+((Cnema)response).toString());
				else if (response instanceof MusicScorerResponse)
					DistributionApplication.LOGGER.debug("Final Response "+((MusicScorerResponse)response).toString());
			}
		}
		return response;
	}

	private void addError(boolean status, MusicScorerResponse response, String r) {
		response.getResourceList().add(new com.spice.distribution.response.musicScorer.ResourceList(status, message(r)));
	}

	public void addError(boolean status, SafariComResponse respon, String r) {
		respon.getAudios().add(new Audio(status, message(r)));
	}

	public void addError(boolean status, MetadataResponse respon, String r) {
		respon.getResourceList().add(new ResourceList(status, message(r)));
	}

	public void addError(boolean status, Cnema response, String r) {
		response.getResourceList().add(new com.spice.distribution.response.cnema.ResourceList(status, message(r)));
	}

	public boolean check(Collection<ReleaseResources> releaseResourcesList, String resourceCode) {
		return releaseResourcesList.stream().anyMatch(e -> e.getResourceCode().equalsIgnoreCase(resourceCode));
	}

	private String message(String r) {
		StringBuilder builder = new StringBuilder("Resource Code : ");
		builder.append(r);
		return builder.append(" Not Found").toString();
	}

	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Response processFeed(int t) {
		
		List<ReleaseResources> releaseResources = repo.findByResourceCodeIn(repoFactory.getDistributionTransactionResource().findByServiceId(t));
		if (!releaseResources.isEmpty())
//			return doCheck(releaseResources, t, mapper.getMapper(t).generate((Collection) releaseResources));
			return mapper.getMapper(t).generate((Collection)releaseResources,t);
		return new Respond(false, "Resource Codes Not Found");
	}

	@Override
	@SuppressWarnings("unused")
	public Response processByRelease(MetadataRequest t) {
		Iterable<Releases> releases = releaseRepository.findAll(t.getReleases());
		return null;
		// JsonifyCMS cms = (JsonifyCMS) mapper.getMapper(t.getServiceId());
		// return cms.generateForRelease(releases);
	}

	@Autowired
	private JsonMapper<Collection<ReleaseResources>, Response> mapper;

	@Autowired
	private ReleaseResourcesRepository repo;

	@Autowired
	private ReleasesRepository releaseRepository;
	@Autowired
	private RepositoryFactory repoFactory;

}
