package com.spice.distribution.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.spice.distribution.cms.entity.ServicePerformanceSummaries;
import com.spice.distribution.request.ServiceSummariesRequest;
import com.spice.distribution.response.CnemaNotificationResponse;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.MISReportDataService;

@Repository
@Transactional
@Service("misreport")
public class MISReportDataSerivceImpl implements MISReportDataService {

	@Autowired
	@Qualifier("cmsEntityManager")
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Response insertIntoServicePerformanceSummaries(ServiceSummariesRequest request, int service_id) {
		// TODO Auto-generated method stub
		try {
			ServicePerformanceSummaries servicePerformanceSummaries = new ServicePerformanceSummaries();

			// servicePerformanceSummaries.setService_id(service_id);
			servicePerformanceSummaries.setReporting_date(request.getReporting_date());
			
			servicePerformanceSummaries.setBilling_fail_hourly(request.getBilling_fail_hourly());			
			servicePerformanceSummaries.setBilling_fail_daily(request.getBilling_fail_daily());
			servicePerformanceSummaries.setBilling_fail_monthly(request.getBilling_fail_monthly());
			servicePerformanceSummaries.setBilling_fail_weekly(request.getBilling_fail_weekly());
			
			servicePerformanceSummaries.setData_hourly(request.getData_hourly());			
			servicePerformanceSummaries.setData_daily(request.getData_daily());
			servicePerformanceSummaries.setData_monthly(request.getData_monthly());
			servicePerformanceSummaries.setData_weekly(request.getData_weekly());
			
			servicePerformanceSummaries.setRegistration_app(request.getRegistration_app());
			servicePerformanceSummaries.setRegistration_sms(request.getRegistration_sms());
			
			servicePerformanceSummaries.setRenewal_hourly(request.getRenewal_hourly());
			servicePerformanceSummaries.setRenewal_daily(request.getRenewal_daily());
			servicePerformanceSummaries.setRenewal_monthly(request.getRenewal_monthly());
			servicePerformanceSummaries.setRenewal_weekly(request.getRenewal_weekly());

			servicePerformanceSummaries.setReporting_month(request.getReporting_month());
			servicePerformanceSummaries.setReporting_year(request.getReporting_year());

			servicePerformanceSummaries.setRevenue_hourly(request.getRevenue_hourly());			
			servicePerformanceSummaries.setRevenue_daily(request.getRevenue_daily());
			servicePerformanceSummaries.setRevenue_monthly(request.getRevenue_monthly());
			servicePerformanceSummaries.setRevenue_weekly(request.getRevenue_weekly());
			
			servicePerformanceSummaries.setService_id(service_id);
			
			servicePerformanceSummaries.setSubscription_app_hourly(request.getSubscription_app_hourly());			
			servicePerformanceSummaries.setSubscription_app_daily(request.getSubscription_app_daily());
			servicePerformanceSummaries.setSubscription_app_monthly(request.getSubscription_app_monthly());
			servicePerformanceSummaries.setSubscription_app_weekly(request.getSubscription_app_weekly());
			
			servicePerformanceSummaries.setSubscription_sms_hourly(request.getSubscription_sms_hourly());			
			servicePerformanceSummaries.setSubscription_sms_daily(request.getSubscription_sms_daily());
			servicePerformanceSummaries.setSubscription_sms_monthly(request.getSubscription_sms_monthly());
			servicePerformanceSummaries.setSubscription_sms_weekly(request.getSubscription_sms_weekly());
			
			servicePerformanceSummaries.setTotal_renewal(request.getTotal_renewal());
			servicePerformanceSummaries.setTotal_revenue(request.getTotal_revenue());
			servicePerformanceSummaries.setTotal_subscription_app(request.getTotal_subscription_app());
			servicePerformanceSummaries.setTotal_subscription_sms(request.getTotal_subscription_sms());
			servicePerformanceSummaries.setTotal_visit(request.getTotal_visit());
			servicePerformanceSummaries.setTotal_visit_and_browse(request.getTotal_visit_and_browse());
			servicePerformanceSummaries.setUnsubscribe_by_system(request.getUnsubscribe_by_system());
			servicePerformanceSummaries.setUnsubscribe_by_user(request.getUnsubscribe_by_user());
			entityManager.persist(servicePerformanceSummaries);
			
			return new CnemaNotificationResponse("true", "Data inserted Successfully");
		} catch (Exception e) {

			return new CnemaNotificationResponse("false", "Data is not inserted Successfully");
		}
	}

	@Override
	public List<ServicePerformanceSummaries>  isServicePresent(ServiceSummariesRequest request, int service_id) {
		try {
			
//			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
//			String strDate = dateFormat.format(request.getReporting_date());  
			
			String sql = "SELECT * FROM ServicePerformanceSummaries where  reporting_date = ? AND service_id = ? ";
		      
		      Query query = entityManager.createNativeQuery(sql, ServicePerformanceSummaries.class);
		      query.setParameter(1, request.getReporting_date());
		      query.setParameter(2,service_id);
		      List<ServicePerformanceSummaries> list = query.getResultList();
		    //  System.out.println("List Size : " + list.size());
			return list;
		} catch (Exception e) {

			return null;
		}

	}

	@Override
	public Response updateIntoServicePerformanceSummaries(ServiceSummariesRequest request, int service_id,
			ServicePerformanceSummaries obj) {
		try {
		//ojb.setReporting_date(request.getReporting_date());
			
		obj.setBilling_fail_hourly(request.getBilling_fail_hourly());	
		obj.setBilling_fail_daily(request.getBilling_fail_daily());
		obj.setBilling_fail_monthly(request.getBilling_fail_monthly());
		obj.setBilling_fail_weekly(request.getBilling_fail_weekly());
		
		obj.setData_hourly(request.getData_hourly());
		obj.setData_daily(request.getData_daily());
		obj.setData_monthly(request.getData_monthly());
		obj.setData_weekly(request.getData_weekly());
		obj.setRegistration_app(request.getRegistration_app());
		obj.setRegistration_sms(request.getRegistration_sms());

		obj.setRenewal_hourly(request.getRenewal_hourly());
		obj.setRenewal_daily(request.getRenewal_daily());
		obj.setRenewal_monthly(request.getRenewal_monthly());
		obj.setRenewal_weekly(request.getRenewal_weekly());

		obj.setReporting_month(request.getReporting_month());
		obj.setReporting_year(request.getReporting_year());

		obj.setRevenue_hourly(request.getRevenue_hourly());
		obj.setRevenue_daily(request.getRevenue_daily());
		obj.setRevenue_monthly(request.getRevenue_monthly());
		obj.setRevenue_weekly(request.getRevenue_weekly());
		
		//obj.setService_id(service_id);
		obj.setSubscription_app_hourly(request.getSubscription_app_hourly());
		obj.setSubscription_app_daily(request.getSubscription_app_daily());
		obj.setSubscription_app_monthly(request.getSubscription_app_monthly());
		obj.setSubscription_app_weekly(request.getSubscription_app_weekly());
		
		obj.setSubscription_sms_hourly(request.getSubscription_sms_hourly());
		obj.setSubscription_sms_daily(request.getSubscription_sms_daily());
		obj.setSubscription_sms_monthly(request.getSubscription_sms_monthly());
		obj.setSubscription_sms_weekly(request.getSubscription_sms_weekly());
		
		obj.setTotal_renewal(request.getTotal_renewal());
		obj.setTotal_revenue(request.getTotal_revenue());
		obj.setTotal_subscription_app(request.getTotal_subscription_app());
		obj.setTotal_subscription_sms(request.getTotal_subscription_sms());
		obj.setTotal_visit(request.getTotal_visit());
		obj.setTotal_visit_and_browse(request.getTotal_visit_and_browse());
		obj.setUnsubscribe_by_system(request.getUnsubscribe_by_system());
		obj.setUnsubscribe_by_user(request.getUnsubscribe_by_user());
		entityManager.merge(obj);
		
		return new CnemaNotificationResponse("true", "Data Updated Successfully");
	} catch (Exception e) {

		return new CnemaNotificationResponse("false", "Data is not Updated Successfully");
	}
	}

}
