package com.spice.distribution.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spice.distribution.entity.DistributionSources;
import com.spice.distribution.util.RepositoryFactory;

/**
 * @author ankit
 *
 */
@Service("source")
public class DSource extends DistributorAdapter {

	@Override
	public Iterable<DistributionSources> processSource() {
		return repositoryFactory.getDistributionSource().findAll();
	}

	@Autowired
	RepositoryFactory repositoryFactory;
}
