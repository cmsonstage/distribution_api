package com.spice.distribution.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spice.distribution.entity.DistributionActions;
import com.spice.distribution.util.RepositoryFactory;

/**
 * @author ankit
 *
 */
@Service("action")
public class DAction extends DistributorAdapter {

	@Override
	public Iterable<DistributionActions> processAction() {
		return repositoryFactory.getDistributionAction().findAll();
	}

	@Autowired
	private RepositoryFactory repositoryFactory;
}
