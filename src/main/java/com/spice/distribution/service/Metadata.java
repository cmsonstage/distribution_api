package com.spice.distribution.service;

/**
 * @author ankit
 *
 */
public interface Metadata<R, T> {
	R processByResource(T t);

	R processByRelease(T t);
	
	R processFeed(int t);

}
