package com.spice.distribution.service.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;
import com.spice.distribution.cms.constant.ResourceType;
import com.spice.distribution.cms.entity.DisplayArtists;
import com.spice.distribution.cms.entity.Genres;
import com.spice.distribution.cms.entity.Images;
import com.spice.distribution.cms.entity.ReleaseResourceDisplayArtists;
import com.spice.distribution.cms.entity.ReleaseResourceGenres;
import com.spice.distribution.cms.entity.ReleaseResourceTags;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.entity.Releases;
import com.spice.distribution.cms.entity.SoundRecordings;
import com.spice.distribution.cms.entity.TechnicalSoundRecordingFileDetails;
import com.spice.distribution.cms.repo.ReleaseResourceDisplayArtistRepository;
import com.spice.distribution.cms.repo.ReleaseResourceGenresRepository;
import com.spice.distribution.cms.repo.ReleaseResourcesRepository;
import com.spice.distribution.cms.repo.ReleasesRepository;
import com.spice.distribution.response.safaricom.Artist;
import com.spice.distribution.response.safaricom.Audio;
import com.spice.distribution.response.safaricom.Genre;
import com.spice.distribution.response.safaricom.SafariComResponse;
import com.spice.distribution.service.Jsonify;
import com.spice.distribution.service.impl.JsonifySafariCom;
import com.spice.distribution.util.Filter;
import com.spice.distribution.util.ObjectsUtil;
import com.spice.distribution.util.RepositoryFactory;

/**
 * @author ankit
 *
 */
@Component("safaricom")
@Scope(proxyMode = ScopedProxyMode.INTERFACES, value = "prototype")
public class JsonifySafariComCms extends JsonifySafariCom
		implements Jsonify<Collection<ReleaseResources>, SafariComResponse> {

	private List<ReleaseResourceGenres> genresListCms;
	private List<ReleaseResourceDisplayArtists> displayArtistCms;
	private List<Releases> releasesList;
	private List<String> message = new ArrayList<>();
	private List<com.spice.distribution.response.safaricom.Language> languageList = new ArrayList<>();
	private List<Genre> genreList = new ArrayList<>();
	private List<Artist> artistList = new ArrayList<>();
	private List<com.spice.distribution.response.safaricom.Album> albumList = new ArrayList<>();

	@Override
	public SafariComResponse generate(Collection<ReleaseResources> t,int serviceId) {
		Objects.requireNonNull(t, "ReleaseResources Not Found");
		SafariComResponse response = new SafariComResponse();
		response.setAudios(this.generateAudioList((List<ReleaseResources>) t));
		response.setAlbums(albumList);
		response.setArtists(artistList);
		response.setGenres(genreList);
		response.setLanguages(languageList);
		return response;
	}
	
	@Override
	public SafariComResponse generateKaraoke(Collection<ReleaseResources> t) {
		Objects.requireNonNull(t, "ReleaseResources Not Found");
		SafariComResponse response = new SafariComResponse();
		response.setAudios(this.generateAudioList((List<ReleaseResources>) t));
		response.setAlbums(albumList);
		response.setArtists(artistList);
		response.setGenres(genreList);
		response.setLanguages(languageList);
		return response;
	}

	// public SafariComResponse generateSafariCom(List<ReleaseResources>
	// releaseResourcesList) {
	// Objects.requireNonNull(releaseResourcesList, "ReleaseResources Not Found");
	// SafariComResponse response = new SafariComResponse();
	// response.setAudios(this.generateAudioList(releaseResourcesList));
	// response.setAlbums(albumList);
	// response.setArtists(artistList);
	// response.setGenres(genreList);
	// response.setLanguages(languageList);
	// return response;
	// }

	public List<Audio> generateAudioList(List<ReleaseResources> releaseResourcesList) {
		List<Audio> audioList = new ArrayList<>();
		List<Integer> releaseResourceIdList = Filter.filterReleaseResourceId(releaseResourcesList);
		genresListCms = genresRepository.findByReleaseResourceIdIn(releaseResourceIdList);
		displayArtistCms = displayArtistRepository.findByReleaseResourceIdIn(releaseResourceIdList);
		releasesList = releasesRepository.findAll(Filter.filterReleaseId(releaseResourcesList));
		for (ReleaseResources releaseResources : releaseResourcesList) {
			if (releaseResources.getResource_type_id().equals(ResourceType.SoundRecording.getId()))
				audioList.add(this.generateAudio(releaseResources));
		}
		return audioList;
	}

	public Audio generateAudio(ReleaseResources releaseResources) {
		Audio audio = new Audio();
		audio.setSafariId(Long.parseLong(releaseResources.getResourceCode()));
		audio.setResourceCode(releaseResources.getResourceCode());
		Optional<SoundRecordings> optional = this.filterSoundByResourceId(releaseResources.getId(),
				releaseResources.getSoundRecordings());
		if (optional.isPresent()) {
			audio.setTitle(optional.get().getTitle_text());
			audio.setSubTitle(optional.get().getSub_title());
			audio.setDescription(new String());
			audio.setSkizaId(SKIZAID);
		}
		audio.setLanguageId(releaseResources.getLanguage_id());
		audio.setResourceTags(generateResourceTag(releaseResources));
		this.generateLanguage(releaseResources);
		SoundRecordings soundRecordings = Filter.filterSoundRecordings(releaseResources.getSoundRecordings(),
				releaseResources.getId());
		if (Objects.nonNull(soundRecordings)) {
				Optional<TechnicalSoundRecordingFileDetails> details = Filter
						.findDuration(soundRecordings.getTechnicalSoundRecordingFileDetails());
//				audio.setDuration(details.getDuration().intValue());
				details.ifPresent(e->audio.setDuration(e.getDuration().intValue()));
		}
		List<ReleaseResourceGenres> releaseResourceGenresList = Filter
				.filterReleaseResourceGenresList(releaseResources.getId(), genresListCms);
		audio.setGenreIds(Filter.filterGenreId(releaseResourceGenresList));
		this.generateGenre(releaseResourceGenresList);

		List<ReleaseResourceDisplayArtists> releaseResourceDisplayArtists = Filter
				.filterReleaseResourceDisplayArtists(releaseResources.getId(), displayArtistCms);
		audio.setArtistIds(Filter.filterDisplayArtistId(releaseResourceDisplayArtists));
		this.generateArtist(releaseResourceDisplayArtists);
		audio.setIsPublished(false);
		Optional<Releases> optionalReleases = Filter.filterReleases(releaseResources.getReleaseId(), releasesList);
		if (optionalReleases.isPresent()) {
			Releases releases = optionalReleases.get();
			audio.setAlbumIds(Arrays.asList(releases.getId()));
			this.generateAlbum(releases, audio);
			audio.setAbrUrls(this.generateAbrUrls(releaseResources.getResourceCode()));
			audio.setHlsUrls(this.generateHls(releaseResources.getResourceCode()));
			audio.setMp3Urls(this.generateMp3(releaseResources.getResourceCode()));
			audio.setRtspUrls(this.generateRtsp(releaseResources.getResourceCode()));
		}
		if (message.isEmpty()) {
			audio.setStatus(true);
			audio.setMessage("Successfull");
		} else {
			audio.setMessage(Joiner.on(",").join(message));
		}
		return audio;
	}
	

	private List<String> generateResourceTag(ReleaseResources rr) {
		List<ReleaseResourceTags> resourceTags = repoFactory.getReleaseResourceTags().findByReleaseResourceId(rr.getId());
		if (!ObjectsUtil.isNullOrEmpty(resourceTags)) {
			List<String> tagList = new ArrayList<>();
			for (ReleaseResourceTags tags : resourceTags) {
				if (Objects.nonNull(tags.getTags()))
					tagList.add(tags.getTags().getTag());
			}
			return tagList;
	}
		return new ArrayList<>();
	}

	public void generateAlbum(Releases releases, Audio audio) {
		com.spice.distribution.response.safaricom.Album al = new com.spice.distribution.response.safaricom.Album();
		al.setSafariId(releases.getId());
		al.setTitle(releases.getTitle_text());
		List<ReleaseResources> releaseResourcesList = releaseResourcesRepository.findByReleaseId(releases.getId());
		boolean status = true;
		for (ReleaseResources releaseResources : releaseResourcesList) {
			if (releaseResources.getResource_type_id().equals(ResourceType.Image.getId())) {
				status = false;
				for (Images image : releaseResources.getImages()) {
					if (image.getImage_type_id() == 5) {
						boolean status1 = true;
						if (Objects.nonNull(releaseResources.getResourceCode())) {
							status1 = false;
							al.setCarouselImages(this.generateCarouselImages(releaseResources.getResourceCode()));
							al.setArtworkImages(this.generateArtworkImages(releaseResources.getResourceCode()));
							audio.setCarouselImages(this.generateCarouselImages(releaseResources.getResourceCode()));
							audio.setArtworkImages(this.generateArtworkImages(releaseResources.getResourceCode()));
						}
						if (status1)
							message.add("Album and Audio Image Code   Not Found");
					}
				}
			}
		}
		if (status)
			message.add("Album Artwork Image Not Found");
		albumList.add(al);
	}

	public void generateArtist(List<ReleaseResourceDisplayArtists> releaseResourceDisplayArtists) {
		boolean status1 = true;
		for (ReleaseResourceDisplayArtists resourceDisplayArtist : releaseResourceDisplayArtists) {
			DisplayArtists displayArtist = resourceDisplayArtist.getDisplayArtists();
			if (displayArtist != null) {
				// for (DisplayArtists displayArtist :
				// resourceDisplayArtist.getDisplayArtists()) {
				boolean status = true;
				status1 = false;
				Artist artist = new Artist();
				artist.setSafariId(displayArtist.getId());
				artist.setTitle(displayArtist.getFull_name());
				if (Objects.nonNull(displayArtist.getArtist_image_resource_code())) {
					status = false;
					artist.setCarouselImages(
							this.generateCarouselImages(displayArtist.getArtist_image_resource_code()));
					artist.setArtworkImages(this.generateArtworkImages(displayArtist.getArtist_image_resource_code()));
				}
				if (status)
					message.add("Artist Image Code of id: " + displayArtist.getId() + " Not Found");
				artistList.add(artist);
				// }
			}
			if (status1)
				message.add("Artist Not Found");
		}
	}

	public void generateGenre(List<ReleaseResourceGenres> releaseResourceGenresList) {
		boolean status1 = true;
		for (ReleaseResourceGenres resourceGenres : releaseResourceGenresList) {
			Genres genres = resourceGenres.getGenres();
			if (genres != null) {
				// for (Genres genres : resourceGenres.getGenres()) {
				boolean status = true;
				status1 = false;
				Genre genre = new Genre();
				genre.setSafariId(genres.getId());
				genre.setTitle(genres.getGenre());
				if (Objects.nonNull(genres.getGenre_image_resource_code())) {
					status = false;
					genre.setCarouselImages(this.generateCarouselImages(genres.getGenre_image_resource_code()));
					genre.setArtworkImages(this.generateArtworkImages(genres.getGenre_image_resource_code()));
				}
				if (status)
					message.add("Genre Image Code of id : " + genres.getId() + " Not Found");
				genreList.add(genre);
				// }
			}
			if (status1)
				message.add("Genre Not Found");
		}

	}

	public void generateLanguage(ReleaseResources resources) {
		if (Objects.nonNull(resources.getLanguage())) {
			com.spice.distribution.response.safaricom.Language lan =  new com.spice.distribution.response.safaricom.Language();
			lan.setSafariId(resources.getLanguage().getId());
			lan.setCode(resources.getLanguage().getIso639_1());
			lan.setFamily(resources.getLanguage().getLanguage_family());
			lan.setTitle(resources.getLanguage().getLanguage_name());
			languageList.add(lan);
		}
	}

	private Optional<SoundRecordings> filterSoundByResourceId(int releaseResourceId,
			Set<SoundRecordings> soundRecordingSet) {
		return soundRecordingSet.stream().filter(e -> e.getRelease_resource_id() == releaseResourceId).findFirst();
	}

	@Autowired
	private ReleaseResourceGenresRepository genresRepository;
	@Autowired
	private ReleaseResourceDisplayArtistRepository displayArtistRepository;
	@Autowired
	private ReleasesRepository releasesRepository;
	@Autowired
	private ReleaseResourcesRepository releaseResourcesRepository;
	@Autowired
	private RepositoryFactory repoFactory;
}
