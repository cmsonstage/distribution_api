package com.spice.distribution.service.internal;
import com.spice.distribution.response.musicScorer.ArtistImageUrls;
import com.spice.distribution.response.musicScorer.DownloadUrls;
import com.spice.distribution.response.musicScorer.GenreImageUrls;
import com.spice.distribution.response.musicScorer.ReleaseImageUrls;
import com.spice.distribution.response.musicScorer.StreamingUrls;
import com.spice.distribution.response.urls.UrlsGetter;
import com.spice.distribution.service.Url;
import java.util.Objects;
public class MusicScorerUrl {

/**
 * @author ankit
 *
 */


	private UrlsGetter getter;

	/**
	 * @param code
	 */
	public MusicScorerUrl(Url<UrlsGetter> url,int serviceId) {
		Objects.requireNonNull(url, "Url Not Specified");
		this.getter = url.create(serviceId);
	}

	public ReleaseImageUrls generateReleaseImageUrls() {
		ReleaseImageUrls urls = new ReleaseImageUrls();
		urls.setL(getter.getL());
		urls.setM(getter.getM());
		urls.setXs(getter.getXs());
		urls.setS(getter.getS());
		return urls;
	}

	public ArtistImageUrls generateArtistImageUrls() {
		ArtistImageUrls urls = new ArtistImageUrls();
		urls.setL(getter.getL());
		urls.setM(getter.getM());
		urls.setXs(getter.getXs());
		urls.setS(getter.getS());
		return urls;
	}

	public GenreImageUrls generateGenreImageUrls() {
		GenreImageUrls urls = new GenreImageUrls();
		urls.setL(getter.getL());
		urls.setM(getter.getM());
		urls.setXs(getter.getXs());
		urls.setS(getter.getS());
		return urls;
	}

	public StreamingUrls generateStreamingUrls() {
		StreamingUrls urls = new StreamingUrls();
		urls.setFullHighDefinition(getter.getFullHighDefinition());
		urls.setHighDefinition(getter.getHighDefinition());
		urls.setStandard(getter.getStandards());
		urls.setMedium(getter.getMedium());
		urls.setLow(getter.getLow());
		urls.setVeryLow(getter.getVeryLow());
		urls.setAdaptiveBitrate(getter.getAdaptiveBitrate());
		return urls;
	}

	public DownloadUrls generateDownloadUrls() {
		DownloadUrls urls = new DownloadUrls();
		urls.setStandard(getter.getStandard());
		urls.setMedium(getter.getDownloadMedium());
		urls.setLow(getter.getDownloadLow());
		return urls;
	}
	
}

