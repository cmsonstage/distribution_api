package com.spice.distribution.service.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.spice.distribution.cms.entity.Countries;
import com.spice.distribution.cms.entity.DealTermsTerritoryCodes;
import com.spice.distribution.cms.entity.Deals;

/**
 * @author ankit
 *
 */
@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "prototype")
public class CommonJson {
	protected List<String> message = new ArrayList<>();

	public List<String> generateCountryOfOrigin(Countries countries) {
		if (Objects.isNull(countries)) {
			message.add("Country Of Origin Not Found");
		}
		return Arrays.asList(countries.getAlpha2_code());
	}

	public Set<String> generateTerritoryCodes(List<Deals> dealsList) {
		Set<String> territoryCodes = new HashSet<>();
		for (Deals deals : dealsList) {
			for (DealTermsTerritoryCodes codes : deals.getDealTermsTerritoryCodes())
				territoryCodes.add(codes.getTerritoryCodes().getDdex_territory_code());
		}
		if (Objects.isNull(territoryCodes) && territoryCodes.isEmpty())
			message.add("Territory Code Not Found");
		return territoryCodes;
	}

}
