package com.spice.distribution.model;

import javax.persistence.Entity;

import org.hibernate.search.annotations.Indexed;

import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.Response;


public class ResponseKaraoke implements Response, Request {
	
	
	String resource_code ,title_text,full_name;

	public String getResource_code() {
		return resource_code;
	}

	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}

	public String getTitle_text() {
		return title_text;
	}

	public void setTitle_text(String title_text) {
		this.title_text = title_text;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	
	

}
