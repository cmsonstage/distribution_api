package com.spice.distribution.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.Response;

public class KeengJsonResponse implements Response, Request {
	
	@JsonProperty("data")
	private List<Data> data = null;

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}
	
	
	
	

}
