package com.spice.distribution.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "media_url_mono","media_url_pre","is_preview","doc_quyen","type","total_media","id","name","singer","item_type","is_downloadable","listen_no","image","image310","url","media_url","download_url","download_url_web","locate_path",})

public class Data {
	
	@JsonProperty("media_url_mono")
	private String media_url_mono;
	
	@JsonProperty("media_url_pre")
	private String media_url_pre;
	
	@JsonProperty("is_preview")
	private String is_preview;
	
	@JsonProperty("doc_quyen")
	private String doc_quyen;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("total_media")
	private String total_media;
	
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("singer")
	private String singer;
	
	@JsonProperty("item_type")
	private String item_type;
	
	@JsonProperty("is_downloadable")
	private String is_downloadable;
	
	@JsonProperty("listen_no")
	private String listen_no;
	
	@JsonProperty("image")
	private String image;
	
	@JsonProperty("image310")
	private String image310;
	
	@JsonProperty("url")
	private String url;
	
	@JsonProperty("media_url")
	private String media_url;
	
	@JsonProperty("download_url")
	private String download_url;
	
	@JsonProperty("download_url_web")
	private String download_url_web;
	
	@JsonProperty("locate_path")
	private String locate_path;
	/* ================= Setter & Getter  ============*/
	
	@JsonProperty("media_url_mono")
	public String getMedia_url_mono() {
		return media_url_mono;
	}
	public void setMedia_url_mono(String media_url_mono) {
		this.media_url_mono = media_url_mono;
	}
	@JsonProperty("media_url_pre")
	public String getMedia_url_pre() {
		return media_url_pre;
	}
	public void setMedia_url_pre(String media_url_pre) {
		this.media_url_pre = media_url_pre;
	}
	@JsonProperty("is_preview")
	public String getIs_preview() {
		return is_preview;
	}
	public void setIs_preview(String is_preview) {
		this.is_preview = is_preview;
	}
	@JsonProperty("doc_quyen")
	public String getDoc_quyen() {
		return doc_quyen;
	}
	
	public void setDoc_quyen(String doc_quyen) {
		this.doc_quyen = doc_quyen;
	}
	@JsonProperty("type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@JsonProperty("total_media")
	public String getTotal_media() {
		return total_media;
	}
	public void setTotal_media(String total_media) {
		this.total_media = total_media;
	}
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonProperty("singer")
	public String getSinger() {
		return singer;
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
	
	@JsonProperty("item_type")
	public String getItem_type() {
		return item_type;
	}
	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}
	
	@JsonProperty("is_downloadable")
	public String getIs_downloadable() {
		return is_downloadable;
	}
	public void setIs_downloadable(String is_downloadable) {
		this.is_downloadable = is_downloadable;
	}
	
	@JsonProperty("listen_no")
	public String getListen_no() {
		return listen_no;
	}
	public void setListen_no(String listen_no) {
		this.listen_no = listen_no;
	}	
	
	@JsonProperty("image")
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	@JsonProperty("image310")
	public String getImage310() {
		return image310;
	}
	public void setImage310(String image310) {
		this.image310 = image310;
	}
	
	@JsonProperty("url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@JsonProperty("media_url")
	public String getMedia_url() {
		return media_url;
	}
	public void setMedia_url(String media_url) {
		this.media_url = media_url;
	}
	
	@JsonProperty("download_url")
	public String getDownload_url() {
		return download_url;
	}
	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}
	
	@JsonProperty("download_url_web")
	public String getDownload_url_web() {
		return download_url_web;
	}
	public void setDownload_url_web(String download_url_web) {
		this.download_url_web = download_url_web;
	}
	
	@JsonProperty("locate_path")
	public String getLocate_path() {
		return locate_path;
	}
	public void setLocate_path(String locate_path) {
		this.locate_path = locate_path;
	}
	@Override
	public String toString() {
		return "Service334Responce [media_url_mono=" + media_url_mono + ", media_url_pre=" + media_url_pre
				+ ", is_preview=" + is_preview + ", doc_quyen=" + doc_quyen + ", type=" + type + ", total_media="
				+ total_media + ", id=" + id + ", name=" + name + ", singer=" + singer + ", item_type=" + item_type
				+ ", is_downloadable=" + is_downloadable + ", listen_no=" + listen_no + ", image=" + image
				+ ", image310=" + image310 + ", url=" + url + ", media_url=" + media_url + ", download_url="
				+ download_url + ", download_url_web=" + download_url_web + ", locate_path=" + locate_path + "]";
	}
	
	

}
