package com.spice.distribution.exceptions;

/**
 * @author ankit
 *
 */
public class InvalidMetadataSourceException extends DistributionException {

	/**
	 * system generated serialVersionId.
	 */
	private static final long serialVersionUID = 2350990125709361128L;

	public InvalidMetadataSourceException(String message) {
		this(message, (Throwable) null);
	}

	public InvalidMetadataSourceException(Throwable exception) {
		this((String) null, exception);
	}

	public InvalidMetadataSourceException(String message, Throwable exception) {
		super(message, exception);
	}

}