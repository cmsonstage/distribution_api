/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.exceptions;

import org.springframework.http.HttpStatus;

/**
 * The Class HttpException.
 *
 * @author ankit
 */
public class HttpException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The status. */
	private HttpStatus status;

	/**
	 * Instantiates a new http exception.
	 *
	 * @param status  http status
	 * @param exception actual cause of the exception.
	 */
	HttpException(HttpStatus status, Throwable exception) {
		super(exception);
		this.status = status;
	}

	/**
	 * Error code representing Http error code.
	 *
	 * @return the int
	 */
	public int errorCode() {
		return status.value();
	}

	/**
	 * Reason.
	 *
	 * @return the string
	 */
	public String reason() {
		return status.getReasonPhrase();
	}
}
