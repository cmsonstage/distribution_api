package com.spice.distribution.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.entity.ServicePerformanceSummaries;
import com.spice.distribution.request.MetadataRequestISRC;
import com.spice.distribution.request.ServiceSummariesRequest;
import com.spice.distribution.response.CnemaNotificationResponse;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.MISReportDataService;
import com.spice.distribution.service.MetadataServiceKaraoke;
import com.spice.distribution.util.Generate;

@RestController
@RequestMapping(value = "/reportdata")
@CrossOrigin
public class MISReportDataController {
	
	@Autowired
	private MISReportDataService misReportDataService;
	@Autowired
	private Generate response;
	
	
	@RequestMapping(value = "/servicePerformanceSummaries", method = { RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response servicePerformanceSummaries(@RequestParam("serviceId") int service_id,@RequestBody @Valid ServiceSummariesRequest request,BindingResult result) {
		DistributionApplication.LOGGER.debug("Request For servicePerformanceSummaries Service Id : " + service_id + " Date : " + request.getReporting_date()  );
		
		List<ServicePerformanceSummaries> obj =null;
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For servicePerformanceSummaries: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		
		//return misReportDataService.insertIntoServicePerformanceSummaries(request,service_id);
		
		obj =  misReportDataService.isServicePresent( request, service_id);
		//System.out.print("Obje  :  " + obj);
		if( obj !=null && obj.size() >0 )
		{
			return misReportDataService.updateIntoServicePerformanceSummaries(request,service_id,obj.get(0));
			
			//return new CnemaNotificationResponse("false", "Data is Already Exist ");
		}else {
			return misReportDataService.insertIntoServicePerformanceSummaries(request,service_id);
		}
		
	}

}
