package com.spice.distribution.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.response.Respond;
import com.spice.distribution.util.Generate;

/**
 * @author ankit
 *
 */
@ControllerAdvice
public class ApplicationErrorController {

	@Autowired
	private Generate generate;

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Respond> handleConflict(Exception ex, WebRequest request) {
		return new ResponseEntity<Respond>(getErrorResponse(ex), HttpStatus.CONFLICT);
	}

	private Respond getErrorResponse(Exception ex) {
		if (Objects.nonNull(ex)) {
			ex.printStackTrace();
			DistributionApplication.LOGGER.error("error :"+ex.getMessage());
			return generate.getError(ex.getMessage());
		}
		return generate.getError("Unable to Process request");
	}
}