package com.spice.distribution.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.entity.ContentRefreshingTransactionDetails;
import com.spice.distribution.cms.entity.ContentRefreshingTransactionStatuses;
import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.cms.entity.SafariComServicesResponse;
import com.spice.distribution.request.CnemaNotificationRequest;
import com.spice.distribution.request.MetadataRequest;
import com.spice.distribution.request.refreshment.Status;
import com.spice.distribution.request.refreshment.TransactionsRequest;
import com.spice.distribution.response.CnemaNotificationResponse;
import com.spice.distribution.response.Respond;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.MetadataService;
import com.spice.distribution.service.NotificationService;
import com.spice.distribution.service.Refreshment;
import com.spice.distribution.service.SimplePageable;
import com.spice.distribution.util.Generate;
import com.spice.distribution.util.PageInfo;

/**
 * @author Manish 
*/
@CrossOrigin
@RestController
@RequestMapping(value = "/notification")
public class NotificationBeatsController {

//	
//	@PostMapping(value = "/beats", produces = MediaType.APPLICATION_JSON_VALUE)
//	public Response notificationBeats(@RequestBody @Valid SafariComServicesResponse request, BindingResult result) {
//		DistributionApplication.LOGGER.debug("========  Request For Beats   ==========");
//		if (result.hasErrors()) {
//			DistributionApplication.LOGGER.error("Request error For saveOrUpdate: "+result.getFieldError().getDefaultMessage());
//			return response.getError(result.getFieldError().getDefaultMessage());
//		}else {
//			System.out.println(" Status code : " + request.getResponseStatus().getStatusCode());
//			System.out.println(" Status code : " + request.getResponseStatus().getStatusMessage());
//			System.out.println(" Status code : " + request.getTrackingId());
//		}
//		return response.notificationBeats(request);
//		
//	}
	
	
	
	@RequestMapping(value = "/beats", method = { RequestMethod.GET, RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response notificationBeats(@RequestBody @Valid SafariComServicesResponse request, BindingResult result) {
		DistributionApplication.LOGGER.debug("========   Request For Beats  =========");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For getMetadataForResources: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return notificationService.updateRemarkByResources(request);
	}
	
	
	@RequestMapping(value = "/cnema", method = { RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response notificationCnema(@RequestBody @Valid CnemaNotificationRequest request, BindingResult result) {
		DistributionApplication.LOGGER.debug("========   Request For Cnema Notification  =========");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For getMetadataForResources: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}

		return notificationService.updateRemarkByResourcesCnema(request);
		//return new CnemaNotificationResponse("false","Request Body should be a valid JSON object.");
	}

	
	@Autowired
	private Generate response;
	/** The service. */
	@Autowired
	private NotificationService notificationService;

	
}

