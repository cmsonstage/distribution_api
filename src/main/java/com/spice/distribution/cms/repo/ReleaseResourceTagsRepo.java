package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.cms.entity.ReleaseResourceTags;

/**
 * @author ankit
 *
 */
public interface ReleaseResourceTagsRepo extends CrudRepository<ReleaseResourceTags, Integer> {
	List<ReleaseResourceTags> findByReleaseResourceId(Integer releaseResourceId);

}
