package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.cms.entity.ContentRefreshingTransactionStatuses;

/**
 * @author ankit
 *
 */
public interface ContentRefreshingTransactionStatusesRepository extends CrudRepository<ContentRefreshingTransactionStatuses, Integer> {
	/* (non-Javadoc)
	 * @see org.springframework.data.repository.CrudRepository#findAll()
	 */
	@Override
	List<ContentRefreshingTransactionStatuses> findAll();

}
