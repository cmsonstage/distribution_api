/*
* Copyright 2017 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.spice.distribution.cms.repo;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.spice.distribution.cms.entity.ReleaseResources;


/**
 * The Interface for generic CRUD operations on a repository for a  ReleaseResources.
 *
 * @author ankit
 * @see com.spice.distribution.cms.entity.ReleaseResources
 */
public interface ReleaseResourcesRepository extends CrudRepository<ReleaseResources, Integer> {

	
	List<ReleaseResources> findByResourceCodeIn(List<String> resourceCode);

	
	List<ReleaseResources> findByReleaseId(Integer releaseId);

	
	ReleaseResources findByResourceCode(String resourceCode);

	
	Iterable<ReleaseResources> findByReleaseIdIn(List<Integer> releaseId);
	
	@Query(value = "SELECT rr.resource_code ,sr.title_text,da.full_name  " + 
			"	FROM SoundRecordings sr  " + 
			"	RIGHT join TechnicalSoundRecordingFileDetails ts on sr.id = ts.sound_recording_id  " + 
			"	LEFT JOIN ReleaseResources rr ON rr.id = sr.release_resource_id " + 
			"	LEFT JOIN ReleaseResourceDisplayArtists rrda ON rrda.release_resource_id = sr.release_resource_id " + 
			"	LEFT JOIN DisplayArtists da On da.id = rrda.display_artist_id " +
			"	WHERE " + 
			"	((sr.isrc = :isrc ) or (sr.title_text = :title_text AND da.full_name= :full_name)) and  (ts.technical_file_type_id = 20 ) GROUP BY resource_code", nativeQuery = true)
	List<Object[]> findKaraoke1(@Param("isrc") String isrc,@Param("full_name") String full_name,@Param("title_text") String title_text);
	
	@Query(value = "SELECT rr.resource_code ,sr.title_text,da.full_name  " + 
			"	FROM SoundRecordings sr  " + 
			"	RIGHT join TechnicalSoundRecordingFileDetails ts on sr.id = ts.sound_recording_id  " + 
			"	LEFT JOIN ReleaseResources rr ON rr.id = sr.release_resource_id " + 
			"	LEFT JOIN ReleaseResourceDisplayArtists rrda ON rrda.release_resource_id = sr.release_resource_id " + 
			"	LEFT JOIN DisplayArtists da On da.id = rrda.display_artist_id " +
			"	WHERE " + 
			"	sr.isrc = :isrc and  ts.technical_file_type_id = 20 GROUP BY resource_code", nativeQuery = true)
	List<Object[]> findKaraoke2(@Param("isrc") String isrc);
	
	@Query(value = "SELECT rr.resource_code ,sr.title_text,da.full_name  " + 
			"	FROM SoundRecordings sr  " + 
			"	RIGHT join TechnicalSoundRecordingFileDetails ts on sr.id = ts.sound_recording_id  " + 
			"	LEFT JOIN ReleaseResources rr ON rr.id = sr.release_resource_id " + 
			"	LEFT JOIN ReleaseResourceDisplayArtists rrda ON rrda.release_resource_id = sr.release_resource_id " + 
			"	LEFT JOIN DisplayArtists da On da.id = rrda.display_artist_id " +
			"	WHERE " + 
			"	sr.title_text = :title_text AND da.full_name= :full_name and  ts.technical_file_type_id = 20  GROUP BY resource_code", nativeQuery = true)
	List<Object[]> findKaraoke3(@Param("full_name") String full_name,@Param("title_text")String title_text);

	@Query(value = "SELECT " + 
			"	rr.resource_code ,sr.title_text,da.full_name " + 
			"FROM " + 
			"	SoundRecordings sr " + 
			"	RIGHT JOIN TechnicalSoundRecordingFileDetails ts ON sr.id = ts.sound_recording_id " + 
			"	LEFT JOIN ReleaseResources rr ON rr.id = sr.release_resource_id " + 
			"	LEFT JOIN ReleaseResourceDisplayArtists rrda ON rrda.release_resource_id = sr.release_resource_id " + 
			"	LEFT JOIN DisplayArtists da ON da.id = rrda.display_artist_id  " + 
			"WHERE " + 
			"	sr.isrc = :isrc  AND ts.technical_file_type_id = 20	 " + 
			"	GROUP BY resource_code", nativeQuery = true)
	List<Object[]> findKaraokeByISRC(@Param("isrc") String isrc);
	
	@Query(value = "SELECT " + 
			"		rr.*   " + 
			"FROM " + 
			"	SoundRecordings sr " + 
			"	RIGHT JOIN TechnicalSoundRecordingFileDetails ts ON sr.id = ts.sound_recording_id " + 
			"	LEFT JOIN ReleaseResources rr ON rr.id = sr.release_resource_id " + 
			"	LEFT JOIN ReleaseResourceDisplayArtists rrda ON rrda.release_resource_id = sr.release_resource_id " + 
			"	LEFT JOIN DisplayArtists da ON da.id = rrda.display_artist_id  " + 
			"WHERE " + 
			"	ts.technical_file_type_id = 20	 " + 
			"	GROUP BY rr.resource_code Order BY rr.resource_code ASC LIMIT :limit OFFSET :offset ", nativeQuery = true)
	List<ReleaseResources> findByKaraokeMetadataASC (@Param("limit") int limit,@Param("offset") int offset);
	
	@Query(value = "SELECT " + 
			"		rr.*   " + 
			"FROM " + 
			"	SoundRecordings sr " + 
			"	RIGHT JOIN TechnicalSoundRecordingFileDetails ts ON sr.id = ts.sound_recording_id " + 
			"	LEFT JOIN ReleaseResources rr ON rr.id = sr.release_resource_id " + 
			"	LEFT JOIN ReleaseResourceDisplayArtists rrda ON rrda.release_resource_id = sr.release_resource_id " + 
			"	LEFT JOIN DisplayArtists da ON da.id = rrda.display_artist_id  " + 
			"WHERE " + 
			"	ts.technical_file_type_id = 20	 " + 
			"	GROUP BY rr.resource_code Order BY rr.resource_code DESC LIMIT :limit OFFSET :offset ", nativeQuery = true)
	List<ReleaseResources> findByKaraokeMetadataDESC (@Param("limit") int limit,@Param("offset") int offset);
	

	

}
