/*
* Copyright 2017 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.spice.distribution.cms.entity.ReleaseResourceGenres;

/**
 * The Interface for generic CRUD operations on a repository for a  ReleaseResourceGenres.
 *
 * @author ankit
 * @see com.spice.distribution.cms.entity.ReleaseResourceGenres
 */
public interface ReleaseResourceGenresRepository extends CrudRepository<ReleaseResourceGenres, Integer> {
	
	/**
	 * Retrieves a list of entity by its release resource id.
	 *
	 * @param releaseResourceId the release resource id
	 * @return the list representing ReleaseResourceGenres with the given param.
	 */
	@Query("From ReleaseResourceGenres as r inner join fetch r.genres where r.releaseResourceId in (:ids)")
	List<ReleaseResourceGenres> findByReleaseResourceIdIn(@Param("ids") List<Integer> releaseResourceId);

}