package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.spice.distribution.cms.entity.TechnicalSoundRecordingFileDetails;

/**
 * @author ankit
 *@author zahid
 */
public interface TechnicalSoundFileDetailRepository
		extends CrudRepository<TechnicalSoundRecordingFileDetails, Integer> {
	@Query(value = "SELECT max(duration) FROM TechnicalSoundRecordingFileDetails WHERE sound_recording_id =:sound_recording_id", nativeQuery = true)
	int maxDuration(@Param("sound_recording_id") int sound_recording_id);

	List<TechnicalSoundRecordingFileDetails> findBySoundRecordingIdOrderByDurationDesc(
			@Param("soundRecordingId") int sound_recording_id);

	@Query(value = "SELECT count(*) FROM ReleaseResources as rr join SoundRecordings as vi on rr.id=vi.release_resource_id join TechnicalSoundRecordingFileDetails as tech on vi.id=tech.sound_recording_id where rr.id =:releaseResourceId and tech.technical_sound_recording_details_id in(:ids)", nativeQuery = true)
	Integer count(@Param("releaseResourceId") Integer releaseResourceId, @Param("ids") List<Integer> techIds);

}
