package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.spice.distribution.cms.entity.TechnicalSoundRecordingFileDetails;
import com.spice.distribution.cms.entity.TechnicalVideoFileDetails;

/**
 * @author ankit
 *
 */
public interface TechnicalVideoFileDetailRepository extends CrudRepository<TechnicalVideoFileDetails, Integer> {

	@Query(value = "SELECT max(duration) FROM TechnicalVideoFileDetails WHERE video_id =:videoId", nativeQuery = true)
	Integer maxDuration(@Param("videoId") int videoId);
	
	List<TechnicalVideoFileDetails> findByVideoIdOrderByDurationDesc(
			@Param("videoId") int video_id);
	
	@Query(value="SELECT count(*) FROM ReleaseResources as rr join Videos as vi on rr.id=vi.release_resource_id join TechnicalVideoFileDetails as tech on vi.id=tech.video_id where rr.id =:releaseResourceId and tech.technical_video_details_id in(:ids)", nativeQuery = true)
	Integer countByreleaseResourceIdAndtechnicalVideoDetailsId(@Param("releaseResourceId")Integer releaseResourceId, @Param("ids") List<Integer> techIds);
	
}
