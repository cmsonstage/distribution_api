package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.cms.entity.DealTermsUsageUseTypes;


public interface DealTermsUsageUseTypesRepository  extends CrudRepository<DealTermsUsageUseTypes, Integer>{
	List<DealTermsUsageUseTypes> findByDealId(Integer dealId);
}
