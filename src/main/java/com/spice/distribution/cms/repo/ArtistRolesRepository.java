/*
* Copyright 2017 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.spice.distribution.cms.repo;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.spice.distribution.cms.entity.ArtistRoles;

/**
 *   Interface for generic CRUD operations on a repository for a ArtistRoles.
 * 
 * @author ankit
 * @see com.spice.distribution.cms.entity.ArtistRoles
 *
 */
public interface ArtistRolesRepository extends CrudRepository<ArtistRoles, Integer> {
	
	/**
	 * Retrieves an entity by its release resource id.
	 *
	 * @param releaseResourceId the release resource id
	 * @return the entity with the given id or {@literal null} if none found
	 * @throws IllegalArgumentException if {@code id} is {@literal null}
	 */
	
	@Query(value = "SELECT t1.artist_role FROM ArtistRoles t1  join  ReleaseResourceDisplayArtists t2 on t2.artist_role_id=t1.id  where t2.release_resource_id  =:id", nativeQuery = true)
	List<String> findByReleaseResourceId(@Param("id") int releaseResourceId);
}
