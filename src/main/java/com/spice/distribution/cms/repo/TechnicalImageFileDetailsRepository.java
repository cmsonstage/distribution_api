package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.spice.distribution.cms.entity.TechnicalImageFileDetails;



public interface TechnicalImageFileDetailsRepository extends CrudRepository<TechnicalImageFileDetails, Integer>{
//	@Query(value="SELECT count(*) FROM ReleaseResources as rr join Images as im on rr.id=im.release_resource_id join TechnicalImageFileDetails as tech on im.id=tech.image_id where rr.id =:releaseResourceId and tech.technical_image_details_id in(:ids)", nativeQuery = true)
//	Integer count(@Param("releaseResourceId")Integer releaseResourceId,@Param("ids") List<Integer> techIds);
	
	@Query(value="SELECT count(*) FROM ReleaseResources as rr join Releases as r on r.id= rr.release_id	join Images as im on rr.id=im.release_resource_id join TechnicalImageFileDetails as tech on im.id=tech.image_id where r.id =:releaseResourceId  and tech.technical_image_details_id in (:ids) and resource_type_id =4", nativeQuery = true)
	Integer count(@Param("releaseResourceId")Integer releaseResourceId,@Param("ids") List<Integer> techIds);
	
	
	}
