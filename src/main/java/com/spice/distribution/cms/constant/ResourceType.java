package com.spice.distribution.cms.constant;

/**
 * @author ankit
 *
 */
public enum ResourceType {
	SoundRecording(1), MIDI(2), Video(3), Image(4), Text(5), SheetMusic(6), Software(7), UserDefinedResource(8);
	private int value;

	private ResourceType(int value) {
		this.value = value;
	}

	public int getId() {
		return value;
	}
}
