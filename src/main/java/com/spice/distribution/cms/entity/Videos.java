package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "Videos")
public class Videos implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private int content_partner_id;
	@Column
	private int release_resource_id;
	@Column
	private int video_type_id;
	@Column
	private Character is_artist_related;
	@Column
	private String isrc;

	@Column
	private String title_text;
	@Column
	private String sub_title;
	@Column
	private Long duration;
	@ManyToOne
	@JoinColumn(name = "id", insertable = false, updatable = false)
	private ReleaseResources releaseResources;
	@OneToOne
	@JoinColumn(name="video_type_id",insertable=false,updatable=false)
	private VideoTypes videoTypes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContent_partner_id() {
		return content_partner_id;
	}

	public void setContent_partner_id(int content_partner_id) {
		this.content_partner_id = content_partner_id;
	}

	public int getRelease_resource_id() {
		return release_resource_id;
	}

	public void setRelease_resource_id(int release_resource_id) {
		this.release_resource_id = release_resource_id;
	}

	public int getVideo_type_id() {
		return video_type_id;
	}

	public void setVideo_type_id(int video_type_id) {
		this.video_type_id = video_type_id;
	}

	public Character getIs_artist_related() {
		return is_artist_related;
	}

	public void setIs_artist_related(Character is_artist_related) {
		this.is_artist_related = is_artist_related;
	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public String getTitle_text() {
		return title_text;
	}

	public void setTitle_text(String title_text) {
		this.title_text = title_text;
	}

	public String getSub_title() {
		return sub_title;
	}

	public void setSub_title(String sub_title) {
		this.sub_title = sub_title;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public ReleaseResources getReleaseResources() {
		return releaseResources;
	}

	public void setReleaseResources(ReleaseResources releaseResources) {
		this.releaseResources = releaseResources;
	}

	@Override
	public Videos clone() throws CloneNotSupportedException {
		return (Videos) super.clone();
	}

	public VideoTypes getVideoTypes() {
		return videoTypes;
	}

	public void setVideoTypes(VideoTypes videoTypes) {
		this.videoTypes = videoTypes;
	}

}
