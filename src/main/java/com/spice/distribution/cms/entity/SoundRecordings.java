package com.spice.distribution.cms.entity;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "SoundRecordings")
public class SoundRecordings  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private int content_partner_id;
	@Column
	private int release_resource_id;
	@Column
	private int sound_recording_type_id;
	@Column
	private String isrc;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_artist_related;
	@Column
	private String resource_reference;
	@Column
	private String title_text;
	@Column
	private String sub_title;
	@Column
	private String reference_title_language_and_script_code;
	@Column
	private String instrumentation_description;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_medley;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_potpourri;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_instrumental;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_background;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_hidden_resource;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_bonus_resource;
	@Column
	@Type(type = "numeric_boolean")
	private boolean has_pre_order_fulfillment;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_computer_generated;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_remastered;
	@Column
	@Type(type = "numeric_boolean")
	private boolean no_silence_before;
	@Column
	@Type(type = "numeric_boolean")
	private boolean no_silence_after;
	@Column
	@Type(type = "numeric_boolean")
	private boolean performer_information_required;
	@Column
	private String language_of_performance;
	@Column
	private Long duration;
	@Column
	private BigInteger numberOfCollections;
	@Column
	private String creation_date;
	@Column
	private String mastered_date;
	@Column
	private String remastered_date;
	@Column
	private String territory_of_commissioning;
	@Column
	private String identifier_Type;
	@Column
	private BigInteger number_of_featured_artists;
	@Column
	private BigInteger number_of_non_featured_artists;
	@Column
	private BigInteger number_of_contracted_artists;
	@Column
	private BigInteger number_of_non_contracted_artists;
	@Column
	@Type(type = "numeric_boolean")
	private boolean is_updated;
	@Column
	private String language_and_script_code;
	@Transient
	private String resource_code;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id", insertable = false, updatable = false)
	private ReleaseResources releaseResources;
	@OneToMany
	@JoinColumn(name = "sound_recording_id", insertable = false, updatable = false)
	private List<TechnicalSoundRecordingFileDetails> technicalSoundRecordingFileDetails;
	@OneToOne
	@JoinColumn(name="sound_recording_type_id",insertable=false,updatable=false)
	private SoundRecordingTypes soundRecordingTypes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContent_partner_id() {
		return content_partner_id;
	}

	public void setContent_partner_id(int content_partner_id) {
		this.content_partner_id = content_partner_id;
	}

	public int getRelease_resource_id() {
		return release_resource_id;
	}

	public void setRelease_resource_id(int release_resource_id) {
		this.release_resource_id = release_resource_id;
	}

	public int getSound_recording_type_id() {
		return sound_recording_type_id;
	}

	public void setSound_recording_type_id(int sound_recording_type_id) {
		this.sound_recording_type_id = sound_recording_type_id;
	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public boolean isIs_artist_related() {
		return is_artist_related;
	}

	public void setIs_artist_related(boolean is_artist_related) {
		this.is_artist_related = is_artist_related;
	}

	public String getResource_reference() {
		return resource_reference;
	}

	public List<TechnicalSoundRecordingFileDetails> getTechnicalSoundRecordingFileDetails() {
		return technicalSoundRecordingFileDetails;
	}

	public void setTechnicalSoundRecordingFileDetails(List<TechnicalSoundRecordingFileDetails> technicalSoundRecordingFileDetails) {
		this.technicalSoundRecordingFileDetails = technicalSoundRecordingFileDetails;
	}

	public void setResource_reference(String resource_reference) {
		this.resource_reference = resource_reference;
	}

	public String getTitle_text() {
		return title_text;
	}

	public void setTitle_text(String title_text) {
		this.title_text = title_text;
	}

	public String getSub_title() {
		return sub_title;
	}

	public void setSub_title(String sub_title) {
		this.sub_title = sub_title;
	}

	public String getReference_title_language_and_script_code() {
		return reference_title_language_and_script_code;
	}

	public void setReference_title_language_and_script_code(String reference_title_language_and_script_code) {
		this.reference_title_language_and_script_code = reference_title_language_and_script_code;
	}

	public String getInstrumentation_description() {
		return instrumentation_description;
	}

	public void setInstrumentation_description(String instrumentation_description) {
		this.instrumentation_description = instrumentation_description;
	}

	public boolean isIs_medley() {
		return is_medley;
	}

	public void setIs_medley(boolean is_medley) {
		this.is_medley = is_medley;
	}

	public boolean isIs_potpourri() {
		return is_potpourri;
	}

	public void setIs_potpourri(boolean is_potpourri) {
		this.is_potpourri = is_potpourri;
	}

	public boolean isIs_instrumental() {
		return is_instrumental;
	}

	public void setIs_instrumental(boolean is_instrumental) {
		this.is_instrumental = is_instrumental;
	}

	public boolean isIs_background() {
		return is_background;
	}

	public void setIs_background(boolean is_background) {
		this.is_background = is_background;
	}

	public boolean isIs_hidden_resource() {
		return is_hidden_resource;
	}

	public void setIs_hidden_resource(boolean is_hidden_resource) {
		this.is_hidden_resource = is_hidden_resource;
	}

	public boolean isIs_bonus_resource() {
		return is_bonus_resource;
	}

	public void setIs_bonus_resource(boolean is_bonus_resource) {
		this.is_bonus_resource = is_bonus_resource;
	}

	public boolean isHas_pre_order_fulfillment() {
		return has_pre_order_fulfillment;
	}

	public void setHas_pre_order_fulfillment(boolean has_pre_order_fulfillment) {
		this.has_pre_order_fulfillment = has_pre_order_fulfillment;
	}

	public boolean isIs_computer_generated() {
		return is_computer_generated;
	}

	public void setIs_computer_generated(boolean is_computer_generated) {
		this.is_computer_generated = is_computer_generated;
	}

	public boolean isIs_remastered() {
		return is_remastered;
	}

	public void setIs_remastered(boolean is_remastered) {
		this.is_remastered = is_remastered;
	}

	public boolean isNo_silence_before() {
		return no_silence_before;
	}

	public void setNo_silence_before(boolean no_silence_before) {
		this.no_silence_before = no_silence_before;
	}

	public boolean isNo_silence_after() {
		return no_silence_after;
	}

	public void setNo_silence_after(boolean no_silence_after) {
		this.no_silence_after = no_silence_after;
	}

	public boolean isPerformer_information_required() {
		return performer_information_required;
	}

	public void setPerformer_information_required(boolean performer_information_required) {
		this.performer_information_required = performer_information_required;
	}

	public String getLanguage_of_performance() {
		return language_of_performance;
	}

	public void setLanguage_of_performance(String language_of_performance) {
		this.language_of_performance = language_of_performance;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public BigInteger getNumberOfCollections() {
		return numberOfCollections;
	}

	public void setNumberOfCollections(BigInteger numberOfCollections) {
		this.numberOfCollections = numberOfCollections;
	}

	public String getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(String creation_date) {
		this.creation_date = creation_date;
	}

	public String getMastered_date() {
		return mastered_date;
	}

	public void setMastered_date(String mastered_date) {
		this.mastered_date = mastered_date;
	}

	public String getRemastered_date() {
		return remastered_date;
	}

	public void setRemastered_date(String remastered_date) {
		this.remastered_date = remastered_date;
	}

	public String getTerritory_of_commissioning() {
		return territory_of_commissioning;
	}

	public void setTerritory_of_commissioning(String territory_of_commissioning) {
		this.territory_of_commissioning = territory_of_commissioning;
	}

	public String getIdentifier_Type() {
		return identifier_Type;
	}

	public void setIdentifier_Type(String identifier_Type) {
		this.identifier_Type = identifier_Type;
	}

	public BigInteger getNumber_of_featured_artists() {
		return number_of_featured_artists;
	}

	public void setNumber_of_featured_artists(BigInteger number_of_featured_artists) {
		this.number_of_featured_artists = number_of_featured_artists;
	}

	public BigInteger getNumber_of_non_featured_artists() {
		return number_of_non_featured_artists;
	}

	public void setNumber_of_non_featured_artists(BigInteger number_of_non_featured_artists) {
		this.number_of_non_featured_artists = number_of_non_featured_artists;
	}

	public BigInteger getNumber_of_contracted_artists() {
		return number_of_contracted_artists;
	}

	public void setNumber_of_contracted_artists(BigInteger number_of_contracted_artists) {
		this.number_of_contracted_artists = number_of_contracted_artists;
	}

	public BigInteger getNumber_of_non_contracted_artists() {
		return number_of_non_contracted_artists;
	}

	public void setNumber_of_non_contracted_artists(BigInteger number_of_non_contracted_artists) {
		this.number_of_non_contracted_artists = number_of_non_contracted_artists;
	}

	public boolean isIs_updated() {
		return is_updated;
	}

	public void setIs_updated(boolean is_updated) {
		this.is_updated = is_updated;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public String getResource_code() {
		return resource_code;
	}

	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}

	public ReleaseResources getReleaseResources() {
		return releaseResources;
	}

	public void setReleaseResources(ReleaseResources releaseResources) {
		this.releaseResources = releaseResources;
	}

	public SoundRecordingTypes getSoundRecordingTypes() {
		return soundRecordingTypes;
	}

	public void setSoundRecordingTypes(SoundRecordingTypes soundRecordingTypes) {
		this.soundRecordingTypes = soundRecordingTypes;
	}
	

}
