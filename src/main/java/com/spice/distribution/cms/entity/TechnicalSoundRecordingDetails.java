package com.spice.distribution.cms.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "TechnicalSoundRecordingDetails")
public class TechnicalSoundRecordingDetails implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String drm_platform_type_version;
	@Column
	private String drm_platform_type_namespace;
	@Column
	private String drm_platform_type_user_defined_value;
	@Column
	private String container_format;
	@Column
	private String container_format_namespace;
	@Column
	private String container_format_user_defined_value;
	@Column
	private String audio_codec_type;
	@Column
	private String audio_codec_type_version;
	@Column
	private String audio_codec_type_namespace;
	@Column
	private String audio_codec_type_user_defined_value;
	@Column
	private BigDecimal bit_rate;
	@Column
	private String unit_of_bit_rate;
	@Column
	private BigInteger number_of_channels;
	@Column
	private BigDecimal sampling_rate;
	@Column
	private String unit_of_frequency;
	@Column
	private BigInteger bits_per_sample;

	@Column
	private String sound_recording_preview_part_type;
	@Column
	private BigDecimal sound_recording_preview_start_point;
	@Column
	private BigDecimal sound_recording_preview_end_point;
	@Column
	private Integer duration;
	@Column
	private BigDecimal sound_recording_preview_top_left_corner;
	@Column
	private BigDecimal sound_recording_preview_bottom_right_corner;
	@Column
	private String expression_type;
	@Column
	private String fulfillment_date;
	@Column
	private String consumer_fulfillment_date;
	@Column
	private String language_and_script_code;
	@Column
	private String usable_resource_duration;
	@Column
	private String sound_recording_preview_duration;
	@Column
	private String drm_platform_type;
	@Column
	private String file_suffix;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDrm_platform_type_version() {
		return drm_platform_type_version;
	}

	public void setDrm_platform_type_version(String drm_platform_type_version) {
		this.drm_platform_type_version = drm_platform_type_version;
	}

	public String getDrm_platform_type_namespace() {
		return drm_platform_type_namespace;
	}

	public void setDrm_platform_type_namespace(String drm_platform_type_namespace) {
		this.drm_platform_type_namespace = drm_platform_type_namespace;
	}

	public String getDrm_platform_type_user_defined_value() {
		return drm_platform_type_user_defined_value;
	}

	public void setDrm_platform_type_user_defined_value(String drm_platform_type_user_defined_value) {
		this.drm_platform_type_user_defined_value = drm_platform_type_user_defined_value;
	}

	public String getContainer_format() {
		return container_format;
	}

	public void setContainer_format(String container_format) {
		this.container_format = container_format;
	}

	public String getContainer_format_namespace() {
		return container_format_namespace;
	}

	public void setContainer_format_namespace(String container_format_namespace) {
		this.container_format_namespace = container_format_namespace;
	}

	public String getContainer_format_user_defined_value() {
		return container_format_user_defined_value;
	}

	public void setContainer_format_user_defined_value(String container_format_user_defined_value) {
		this.container_format_user_defined_value = container_format_user_defined_value;
	}

	public String getAudio_codec_type() {
		return audio_codec_type;
	}

	public void setAudio_codec_type(String audio_codec_type) {
		this.audio_codec_type = audio_codec_type;
	}

	public String getAudio_codec_type_version() {
		return audio_codec_type_version;
	}

	public void setAudio_codec_type_version(String audio_codec_type_version) {
		this.audio_codec_type_version = audio_codec_type_version;
	}

	public String getAudio_codec_type_namespace() {
		return audio_codec_type_namespace;
	}

	public void setAudio_codec_type_namespace(String audio_codec_type_namespace) {
		this.audio_codec_type_namespace = audio_codec_type_namespace;
	}

	public String getAudio_codec_type_user_defined_value() {
		return audio_codec_type_user_defined_value;
	}

	public void setAudio_codec_type_user_defined_value(String audio_codec_type_user_defined_value) {
		this.audio_codec_type_user_defined_value = audio_codec_type_user_defined_value;
	}

	public BigDecimal getBit_rate() {
		return bit_rate;
	}

	public void setBit_rate(BigDecimal bit_rate) {
		this.bit_rate = bit_rate;
	}

	public String getUnit_of_bit_rate() {
		return unit_of_bit_rate;
	}

	public void setUnit_of_bit_rate(String unit_of_bit_rate) {
		this.unit_of_bit_rate = unit_of_bit_rate;
	}

	public BigInteger getNumber_of_channels() {
		return number_of_channels;
	}

	public void setNumber_of_channels(BigInteger number_of_channels) {
		this.number_of_channels = number_of_channels;
	}

	public BigDecimal getSampling_rate() {
		return sampling_rate;
	}

	public void setSampling_rate(BigDecimal sampling_rate) {
		this.sampling_rate = sampling_rate;
	}

	public String getUnit_of_frequency() {
		return unit_of_frequency;
	}

	public void setUnit_of_frequency(String unit_of_frequency) {
		this.unit_of_frequency = unit_of_frequency;
	}

	public BigInteger getBits_per_sample() {
		return bits_per_sample;
	}

	public void setBits_per_sample(BigInteger bits_per_sample) {
		this.bits_per_sample = bits_per_sample;
	}

	public String getSound_recording_preview_part_type() {
		return sound_recording_preview_part_type;
	}

	public void setSound_recording_preview_part_type(String sound_recording_preview_part_type) {
		this.sound_recording_preview_part_type = sound_recording_preview_part_type;
	}

	public BigDecimal getSound_recording_preview_end_point() {
		return sound_recording_preview_end_point;
	}

	public void setSound_recording_preview_end_point(BigDecimal sound_recording_preview_end_point) {
		this.sound_recording_preview_end_point = sound_recording_preview_end_point;
	}

	public BigDecimal getSound_recording_preview_top_left_corner() {
		return sound_recording_preview_top_left_corner;
	}

	public void setSound_recording_preview_top_left_corner(BigDecimal sound_recording_preview_top_left_corner) {
		this.sound_recording_preview_top_left_corner = sound_recording_preview_top_left_corner;
	}

	public BigDecimal getSound_recording_preview_bottom_right_corner() {
		return sound_recording_preview_bottom_right_corner;
	}

	public void setSound_recording_preview_bottom_right_corner(BigDecimal sound_recording_preview_bottom_right_corner) {
		this.sound_recording_preview_bottom_right_corner = sound_recording_preview_bottom_right_corner;
	}

	public String getExpression_type() {
		return expression_type;
	}

	public void setExpression_type(String expression_type) {
		this.expression_type = expression_type;
	}

	public String getFulfillment_date() {
		return fulfillment_date;
	}

	public void setFulfillment_date(String fulfillment_date) {
		this.fulfillment_date = fulfillment_date;
	}

	public String getConsumer_fulfillment_date() {
		return consumer_fulfillment_date;
	}

	public void setConsumer_fulfillment_date(String consumer_fulfillment_date) {
		this.consumer_fulfillment_date = consumer_fulfillment_date;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public String getUsable_resource_duration() {
		return usable_resource_duration;
	}

	public void setUsable_resource_duration(String usable_resource_duration) {
		this.usable_resource_duration = usable_resource_duration;
	}

	public String getSound_recording_preview_duration() {
		return sound_recording_preview_duration;
	}

	public void setSound_recording_preview_duration(String sound_recording_preview_duration) {
		this.sound_recording_preview_duration = sound_recording_preview_duration;
	}

	public String getDrm_platform_type() {
		return drm_platform_type;
	}

	public void setDrm_platform_type(String drm_platform_type) {
		this.drm_platform_type = drm_platform_type;
	}

	public BigDecimal getSound_recording_preview_start_point() {
		return sound_recording_preview_start_point;
	}

	public void setSound_recording_preview_start_point(BigDecimal sound_recording_preview_start_point) {
		this.sound_recording_preview_start_point = sound_recording_preview_start_point;
	}

	public String getFile_suffix() {
		return file_suffix;
	}

	public void setFile_suffix(String file_suffix) {
		this.file_suffix = file_suffix;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	@Override
	public TechnicalSoundRecordingDetails clone() throws CloneNotSupportedException {
		return (TechnicalSoundRecordingDetails) super.clone();
	}

}
