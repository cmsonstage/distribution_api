package com.spice.distribution.cms.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
// @Indexed
@Table(name = "DisplayArtists")
public class DisplayArtists implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Integer content_partner_id;
	//	@Field
	private String full_name;
	private String full_name_ascii_transcribed;
	private String full_name_indexed;
	private String names_before_key_name;
	private String key_name;
	private String names_after_key_name;
	private String abbreviated_name;
	private String language_and_script_code;
	private String artist_image_resource_code;

	/**
	 * @param content_partner_id
	 * @param full_name
	 */
	public DisplayArtists(Integer content_partner_id, String full_name) {
		super();
		this.content_partner_id = content_partner_id;
		this.full_name = full_name;
	}

	/**
	 * 
	 */
	public DisplayArtists() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getFull_name_ascii_transcribed() {
		return full_name_ascii_transcribed;
	}

	public void setFull_name_ascii_transcribed(String full_name_ascii_transcribed) {
		this.full_name_ascii_transcribed = full_name_ascii_transcribed;
	}

	public String getFull_name_indexed() {
		return full_name_indexed;
	}

	public void setFull_name_indexed(String full_name_indexed) {
		this.full_name_indexed = full_name_indexed;
	}

	public String getNames_before_key_name() {
		return names_before_key_name;
	}

	public void setNames_before_key_name(String names_before_key_name) {
		this.names_before_key_name = names_before_key_name;
	}

	public String getKey_name() {
		return key_name;
	}

	public void setKey_name(String key_name) {
		this.key_name = key_name;
	}

	public String getNames_after_key_name() {
		return names_after_key_name;
	}

	public void setNames_after_key_name(String names_after_key_name) {
		this.names_after_key_name = names_after_key_name;
	}

	public String getAbbreviated_name() {
		return abbreviated_name;
	}

	public void setAbbreviated_name(String abbreviated_name) {
		this.abbreviated_name = abbreviated_name;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public Integer getContent_partner_id() {
		return content_partner_id;
	}

	public void setContent_partner_id(Integer content_partner_id) {
		this.content_partner_id = content_partner_id;
	}

	@Override
	public DisplayArtists clone() throws CloneNotSupportedException {
		return (DisplayArtists) super.clone();
	}

	public String getArtist_image_resource_code() {
		return artist_image_resource_code;
	}

	public void setArtist_image_resource_code(String artist_image_resource_code) {
		this.artist_image_resource_code = artist_image_resource_code;
	}

	@Override
	public String toString() {
		return "DisplayArtists [id=" + id + ", content_partner_id=" + content_partner_id + ", full_name=" + full_name + ", full_name_ascii_transcribed=" + full_name_ascii_transcribed + ", full_name_indexed=" + full_name_indexed + ", names_before_key_name=" + names_before_key_name + ", key_name=" + key_name + ", names_after_key_name=" + names_after_key_name + ", abbreviated_name=" + abbreviated_name + ", language_and_script_code=" + language_and_script_code + ", contentPartners=]";
	}

}
