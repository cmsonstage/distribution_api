package com.spice.distribution.cms.entity;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;



//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "audios" })
public class Data {

	@JsonProperty("audios")
	private List<Audio> audios = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("audios")
	public List<Audio> getAudios() {
		return audios;
	}

	@JsonProperty("audios")
	public void setAudios(List<Audio> audios) {
		this.audios = audios;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "Data [audios=" + audios + ", additionalProperties=" + additionalProperties + "]";
	}

}