package com.spice.distribution.cms.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author ankit
 *
 */
@Entity
public class ArtistRoles {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String artist_role;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArtist_role() {
		return artist_role;
	}

	public void setArtist_role(String artist_role) {
		this.artist_role = artist_role;
	}
}
