package com.spice.distribution.cms.entity;

import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "ReleaseResourceDisplayArtists")
public class ReleaseResourceDisplayArtists implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "release_resource_id")
	private Integer releaseResourceId;
	@Column
	private Integer display_artist_id;
	@Column
	private Integer artist_role_id;
	@Column
	private BigInteger sequence_number;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "display_artist_id", referencedColumnName = "id", insertable = false, updatable = false)
	private DisplayArtists displayArtists;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getReleaseResourceId() {
		return releaseResourceId;
	}

	public void setReleaseResourceId(Integer releaseResourceId) {
		this.releaseResourceId = releaseResourceId;
	}

	public Integer getDisplay_artist_id() {
		return display_artist_id;
	}

	public void setDisplay_artist_id(Integer display_artist_id) {
		this.display_artist_id = display_artist_id;
	}

	public Integer getArtist_role_id() {
		return artist_role_id;
	}

	public void setArtist_role_id(Integer artist_role_id) {
		this.artist_role_id = artist_role_id;
	}

	public BigInteger getSequence_number() {
		return sequence_number;
	}

	public void setSequence_number(BigInteger sequence_number) {
		this.sequence_number = sequence_number;
	}

	public DisplayArtists getDisplayArtists() {
		return displayArtists;
	}

	public void setDisplayArtists(DisplayArtists displayArtists) {
		this.displayArtists = displayArtists;
	}

	@Override
	public ReleaseResourceDisplayArtists clone() throws CloneNotSupportedException {
		return (ReleaseResourceDisplayArtists) super.clone();
	}

	@Override
	public String toString() {
		return "ReleaseResourceDisplayArtists [id=" + id + ", releaseResourceId=" + releaseResourceId + ", display_artist_id=" + display_artist_id + ", artist_role_id=" + artist_role_id + ", sequence_number=" + sequence_number + ", displayArtists=" + displayArtists + "]";
	}

}
