package com.spice.distribution.cms.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * @author ankit
 *
 */
@Entity
@Table(name = "TechnicalSoundRecordingFileDetails")
public class TechnicalSoundRecordingFileDetails implements Cloneable {
	@Id
	private int id;
	//@JsonProperty(value = "sound_recording_id", access = Access.WRITE_ONLY)
	@Column(name = "sound_recording_id")
	private Integer soundRecordingId;
	@Column
	private Integer sound_recording_details_by_territory_id;
	@Column
	private Integer technical_sound_recording_details_id;
	@Column
	private String technical_resource_details_reference;
	@Column
	private String file_name;
	@Column
	private String file_path;
	@Column
	private Long file_size;
	@Column
	private Long duration;
	@Column
	private Integer old_technical_sound_recording_details_id;
	@Column
	private Integer technical_file_type_id;
	@Column
	private Timestamp updated_at;
	@Column
	private Byte is_file_updated;
	@ManyToOne
	@JoinColumn(name = "technical_sound_recording_details_id", insertable = false, updatable = false)
	private TechnicalSoundRecordingDetails technicalSoundRecordingDetails;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the soundRecordingId
	 */
	public Integer getSoundRecordingId() {
		return soundRecordingId;
	}

	/**
	 * @param soundRecordingId the soundRecordingId to set
	 */
	public void setSoundRecordingId(Integer soundRecordingId) {
		this.soundRecordingId = soundRecordingId;
	}

	public Integer getSound_recording_details_by_territory_id() {
		return sound_recording_details_by_territory_id;
	}

	public TechnicalSoundRecordingDetails getTechnicalSoundRecordingDetails() {
		return technicalSoundRecordingDetails;
	}

	public void setTechnicalSoundRecordingDetails(TechnicalSoundRecordingDetails technicalSoundRecordingDetails) {
		this.technicalSoundRecordingDetails = technicalSoundRecordingDetails;
	}

	public void setSound_recording_details_by_territory_id(Integer sound_recording_details_by_territory_id) {
		this.sound_recording_details_by_territory_id = sound_recording_details_by_territory_id;
	}

	public Integer getTechnical_sound_recording_details_id() {
		return technical_sound_recording_details_id;
	}

	public void setTechnical_sound_recording_details_id(Integer technical_sound_recording_details_id) {
		this.technical_sound_recording_details_id = technical_sound_recording_details_id;
	}

	public String getTechnical_resource_details_reference() {
		return technical_resource_details_reference;
	}

	public void setTechnical_resource_details_reference(String technical_resource_details_reference) {
		this.technical_resource_details_reference = technical_resource_details_reference;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public Long getFile_size() {
		return file_size;
	}

	public void setFile_size(Long file_size) {
		this.file_size = file_size;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Integer getOld_technical_sound_recording_details_id() {
		return old_technical_sound_recording_details_id;
	}

	public void setOld_technical_sound_recording_details_id(Integer old_technical_sound_recording_details_id) {
		this.old_technical_sound_recording_details_id = old_technical_sound_recording_details_id;
	}

	public Integer getTechnical_file_type_id() {
		return technical_file_type_id;
	}

	public void setTechnical_file_type_id(Integer technical_file_type_id) {
		this.technical_file_type_id = technical_file_type_id;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Byte getIs_file_updated() {
		return is_file_updated;
	}

	public void setIs_file_updated(Byte is_file_updated) {
		this.is_file_updated = is_file_updated;
	}

	@Override
	public TechnicalSoundRecordingFileDetails clone() throws CloneNotSupportedException {
		return (TechnicalSoundRecordingFileDetails) super.clone();
	}

	@Override
	public String toString() {
		return "TechnicalSoundRecordingFileDetails [id=" + id + ", sound_recording_id=" + soundRecordingId + ", sound_recording_details_by_territory_id=" + sound_recording_details_by_territory_id + ", technical_sound_recording_details_id=" + technical_sound_recording_details_id + ", technical_resource_details_reference=" + technical_resource_details_reference + ", file_name=" + file_name + ", file_path=" + file_path + ", file_size=" + file_size + ", duration=" + duration
				+ ", old_technical_sound_recording_details_id=" + old_technical_sound_recording_details_id + ", technical_file_type_id=" + technical_file_type_id + ", updated_at=" + updated_at + ", is_file_updated=" + is_file_updated + ", getId()=" + this.getId() + ", getSound_recording_id()=" + this.getSoundRecordingId() + ", getSound_recording_details_by_territory_id()=" + this.getSound_recording_details_by_territory_id() + ", getTechnical_sound_recording_details_id()="
				+ this.getTechnical_sound_recording_details_id() + ", getTechnical_resource_details_reference()=" + this.getTechnical_resource_details_reference() + ", getFile_name()=" + this.getFile_name() + ", getFile_path()=" + this.getFile_path() + ", getFile_size()=" + this.getFile_size() + ", getDuration()=" + this.getDuration() + ", getOld_technical_sound_recording_details_id()=" + this.getOld_technical_sound_recording_details_id() + ", getTechnical_file_type_id()="
				+ this.getTechnical_file_type_id() + ", getUpdated_at()=" + this.getUpdated_at() + ", getIs_file_updated()=" + this.getIs_file_updated() + ", getClass()=" + this.getClass() + ", hashCode()=" + this.hashCode() + ", toString()=" + super.toString() + "]";
	}

}
