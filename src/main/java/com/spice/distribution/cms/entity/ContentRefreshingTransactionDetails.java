/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.spice.distribution.response.Response;

/**
 * The Class ContentRefreshingTransactionDetails represents ContentRefreshingTransactionDetails Table in CMS .
 *
 * @author ankit
 */
@Entity
public class ContentRefreshingTransactionDetails implements Response {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = Access.WRITE_ONLY)
	private int id;

	/** The content refreshing transaction id. */
	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(name = "content_refreshing_transaction_id")
	private Integer contentRefreshingTransactionId;

	/** The old display order. */
	private Integer old_display_order;

	/** The new display order. */
	@Column(name = "new_display_order")
	@JsonProperty("new_display_order")
	private Integer newDisplayOrder;

	/** The item type. */
	private String item_type;

	/** The item type id. */
	private Integer item_type_id;

	/** The item resource id. */
	private String item_resource_id;

	/** The item resource title. */
	private String item_resource_title;

	/** The image url. */
	private String image_url;

	/** The created at. */
	@CreationTimestamp
	private Date created_at;

	/** The updated at. */
	@UpdateTimestamp
	private Date updated_at;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the content refreshing transaction id.
	 *
	 * @return the content refreshing transaction id
	 */
	public Integer getContentRefreshingTransactionId() {
		return contentRefreshingTransactionId;
	}

	/**
	 * Sets the content refreshing transaction id.
	 *
	 * @param contentRefreshingTransactionId the new content refreshing transaction id
	 */
	public void setContentRefreshingTransactionId(Integer contentRefreshingTransactionId) {
		this.contentRefreshingTransactionId = contentRefreshingTransactionId;
	}

	/**
	 * Gets the old display order.
	 *
	 * @return the old display order
	 */
	public Integer getOld_display_order() {
		return old_display_order;
	}

	/**
	 * Sets the old display order.
	 *
	 * @param old_display_order the new old display order
	 */
	public void setOld_display_order(Integer old_display_order) {
		this.old_display_order = old_display_order;
	}

	/**
	 * Gets the item type.
	 *
	 * @return the item type
	 */
	public String getItem_type() {
		return item_type;
	}

	/**
	 * Sets the item type.
	 *
	 * @param item_type the new item type
	 */
	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	/**
	 * Gets the item type id.
	 *
	 * @return the item type id
	 */
	public Integer getItem_type_id() {
		return item_type_id;
	}

	/**
	 * Sets the item type id.
	 *
	 * @param item_type_id the new item type id
	 */
	public void setItem_type_id(Integer item_type_id) {
		this.item_type_id = item_type_id;
	}

	/**
	 * Gets the item resource id.
	 *
	 * @return the item resource id
	 */
	public String getItem_resource_id() {
		return item_resource_id;
	}

	/**
	 * Sets the item resource id.
	 *
	 * @param item_resource_id the new item resource id
	 */
	public void setItem_resource_id(String item_resource_id) {
		this.item_resource_id = item_resource_id;
	}

	/**
	 * Gets the item resource title.
	 *
	 * @return the item resource title
	 */
	public String getItem_resource_title() {
		return item_resource_title;
	}

	/**
	 * Sets the item resource title.
	 *
	 * @param item_resource_title the new item resource title
	 */
	public void setItem_resource_title(String item_resource_title) {
		this.item_resource_title = item_resource_title;
	}

	/**
	 * Gets the image url.
	 *
	 * @return the image url
	 */
	public String getImage_url() {
		return image_url;
	}

	/**
	 * Sets the image url.
	 *
	 * @param image_url the new image url
	 */
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	/**
	 * Gets the created at.
	 *
	 * @return the created at
	 */
	public Date getCreated_at() {
		return created_at;
	}

	/**
	 * Sets the created at.
	 *
	 * @param created_at the new created at
	 */
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	/**
	 * Gets the updated at.
	 *
	 * @return the updated at
	 */
	public Date getUpdated_at() {
		return updated_at;

	}

	/**
	 * Sets the updated at.
	 *
	 * @param updated_at the new updated at
	 */
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	@Override
	public String toString() {
		return "ContentRefreshingTransactionDetails [id=" + id + ", contentRefreshingTransactionId=" + contentRefreshingTransactionId + ", image_url=" + image_url + "]";
	}

	public Integer getNewDisplayOrder() {
		return newDisplayOrder;
	}

	public void setNewDisplayOrder(Integer newDisplayOrder) {
		this.newDisplayOrder = newDisplayOrder;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((image_url == null) ? 0 : image_url.hashCode());
		result = prime * result + ((item_resource_id == null) ? 0 : item_resource_id.hashCode());
		result = prime * result + ((item_resource_title == null) ? 0 : item_resource_title.hashCode());
		result = prime * result + ((item_type == null) ? 0 : item_type.hashCode());
		result = prime * result + ((item_type_id == null) ? 0 : item_type_id.hashCode());
		result = prime * result + ((newDisplayOrder == null) ? 0 : newDisplayOrder.hashCode());
		result = prime * result + ((old_display_order == null) ? 0 : old_display_order.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentRefreshingTransactionDetails other = (ContentRefreshingTransactionDetails) obj;
		if (image_url == null) {
			if (other.image_url != null)
				return false;
		} else if (!image_url.equals(other.image_url))
			return false;
		if (item_resource_id == null) {
			if (other.item_resource_id != null)
				return false;
		} else if (!item_resource_id.equals(other.item_resource_id))
			return false;
		if (item_resource_title == null) {
			if (other.item_resource_title != null)
				return false;
		} else if (!item_resource_title.equals(other.item_resource_title))
			return false;
		if (item_type == null) {
			if (other.item_type != null)
				return false;
		} else if (!item_type.equals(other.item_type))
			return false;
		if (item_type_id == null) {
			if (other.item_type_id != null)
				return false;
		} else if (!item_type_id.equals(other.item_type_id))
			return false;
		if (newDisplayOrder == null) {
			if (other.newDisplayOrder != null)
				return false;
		} else if (!newDisplayOrder.equals(other.newDisplayOrder))
			return false;
		if (old_display_order == null) {
			if (other.old_display_order != null)
				return false;
		} else if (!old_display_order.equals(other.old_display_order))
			return false;
		return true;
	}

}
