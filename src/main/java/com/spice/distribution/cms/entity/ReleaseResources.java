package com.spice.distribution.cms.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "ReleaseResources")
public class ReleaseResources implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private Integer upload_transaction_id;
	@Column
	private Integer content_partner_id;
	@Column(name = "release_id")
	private Integer releaseId;
	@Column
	private Integer resource_type_id;
	@Column
	private Integer release_resource_type_id;
	@Column
	private String release_resource_reference;
	@Column(name = "resource_code")
	private String resourceCode;
	@Column
	private Integer sequence_number;
	@Column
	private Integer language_id;
	@Column
	private Integer isSmilFileAvailable;

	@OneToMany(mappedBy = "release_resource_id", cascade = CascadeType.ALL)
	private Set<Images> images;
	
	@OneToMany(mappedBy = "release_resource_id", cascade = CascadeType.ALL)
	private Set<SoundRecordings> soundRecordings;

	@OneToMany(mappedBy = "release_resource_id", cascade = CascadeType.ALL)
	private Set<Videos> videos;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "language_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Languages language;
	private Byte is_premium;

	public Set<Images> getImages() {
		return images;
	}

	public void setImages(Set<Images> images) {
		this.images = images;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUpload_transaction_id() {
		return upload_transaction_id;
	}

	public void setUpload_transaction_id(Integer upload_transaction_id) {
		this.upload_transaction_id = upload_transaction_id;
	}

	public Integer getContent_partner_id() {
		return content_partner_id;
	}

	public void setContent_partner_id(Integer content_partner_id) {
		this.content_partner_id = content_partner_id;
	}

	public Integer getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Integer releaseId) {
		this.releaseId = releaseId;
	}

	public Integer getResource_type_id() {
		return resource_type_id;
	}

	public void setResource_type_id(Integer resource_type_id) {
		this.resource_type_id = resource_type_id;
	}

	public Integer getRelease_resource_type_id() {
		return release_resource_type_id;
	}

	public void setRelease_resource_type_id(Integer release_resource_type_id) {
		this.release_resource_type_id = release_resource_type_id;
	}

	public String getRelease_resource_reference() {
		return release_resource_reference;
	}

	public void setRelease_resource_reference(String release_resource_reference) {
		this.release_resource_reference = release_resource_reference;
	}

	public String getResourceCode() {
		return resourceCode;
	}

	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	public Integer getSequence_number() {
		return sequence_number;
	}

	public void setSequence_number(Integer sequence_number) {
		this.sequence_number = sequence_number;
	}

	public Integer getLanguage_id() {
		return language_id;
	}

	public void setLanguage_id(Integer language_id) {
		this.language_id = language_id;
	}

	public Integer getIsSmilFileAvailable() {
		return isSmilFileAvailable;
	}

	public void setIsSmilFileAvailable(Integer isSmilFileAvailable) {
		this.isSmilFileAvailable = isSmilFileAvailable;
	}

	public Set<SoundRecordings> getSoundRecordings() {
		return soundRecordings;
	}

	public void setSoundRecordings(Set<SoundRecordings> soundRecordings) {
		this.soundRecordings = soundRecordings;
	}

	@Override
	public ReleaseResources clone() throws CloneNotSupportedException {
		return (ReleaseResources) super.clone();
	}

	public Languages getLanguage() {
		return language;
	}

	public void setLanguage(Languages language) {
		this.language = language;
	}

	public Set<Videos> getVideos() {
		return videos;
	}

	public Byte getIs_premium() {
		return is_premium;
	}

	public void setIs_premium(Byte is_premium) {
		this.is_premium = is_premium;
	}

	public void setVideos(Set<Videos> videos) {
		this.videos = videos;
	}

}
