package com.spice.distribution.cms.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "TechnicalImageDetails")
public class TechnicalImageDetails implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String drm_platform_type;
	@Column
	private String drm_platform_type_version;
	@Column
	private String drm_platform_type_namespace;
	@Column
	private String drm_platform_type_user_defined_value;
	@Column
	private String container_format;
	@Column
	private String container_format_namespace;
	@Column
	private String container_format_user_defined_value;
	@Column
	private String image_codec_type;
	@Column
	private String image_codec_type_version;
	@Column
	private String image_codec_type_namespace;
	@Column
	private String image_codec_type_user_defined_value;
	@Column
	private BigDecimal image_height;
	@Column
	private String image_height_unit_of_measure;
	@Column
	private BigDecimal image_width;
	@Column
	private String image_width_unit_of_measure;
	@Column
	private BigDecimal aspect_ratio;
	@Column
	private String aspect_ratio_type;
	@Column
	private BigInteger color_depth;
	@Column
	private BigInteger image_resolution;

	@Column
	private String image_preview_part_type;
	@Column
	private BigDecimal image_preview_top_left_corner;
	@Column
	private BigDecimal image_preview_bottom_right_corner;
	@Column
	private String expression_type;
	@Column
	private String fulfillment_date;
	@Column
	private String consumer_fulfillment_date;
	@Column
	private String language_and_script_code;
	@Column
	private String file_suffix;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDrm_platform_type() {
		return drm_platform_type;
	}

	public void setDrm_platform_type(String drm_platform_type) {
		this.drm_platform_type = drm_platform_type;
	}

	public String getDrm_platform_type_version() {
		return drm_platform_type_version;
	}

	public void setDrm_platform_type_version(String drm_platform_type_version) {
		this.drm_platform_type_version = drm_platform_type_version;
	}

	public String getDrm_platform_type_namespace() {
		return drm_platform_type_namespace;
	}

	public void setDrm_platform_type_namespace(String drm_platform_type_namespace) {
		this.drm_platform_type_namespace = drm_platform_type_namespace;
	}

	public String getDrm_platform_type_user_defined_value() {
		return drm_platform_type_user_defined_value;
	}

	public void setDrm_platform_type_user_defined_value(String drm_platform_type_user_defined_value) {
		this.drm_platform_type_user_defined_value = drm_platform_type_user_defined_value;
	}

	public String getContainer_format() {
		return container_format;
	}

	public void setContainer_format(String container_format) {
		this.container_format = container_format;
	}

	public String getContainer_format_namespace() {
		return container_format_namespace;
	}

	public void setContainer_format_namespace(String container_format_namespace) {
		this.container_format_namespace = container_format_namespace;
	}

	public String getContainer_format_user_defined_value() {
		return container_format_user_defined_value;
	}

	public void setContainer_format_user_defined_value(String container_format_user_defined_value) {
		this.container_format_user_defined_value = container_format_user_defined_value;
	}

	public String getImage_codec_type() {
		return image_codec_type;
	}

	public void setImage_codec_type(String image_codec_type) {
		this.image_codec_type = image_codec_type;
	}

	public String getImage_codec_type_version() {
		return image_codec_type_version;
	}

	public void setImage_codec_type_version(String image_codec_type_version) {
		this.image_codec_type_version = image_codec_type_version;
	}

	public String getImage_codec_type_namespace() {
		return image_codec_type_namespace;
	}

	public void setImage_codec_type_namespace(String image_codec_type_namespace) {
		this.image_codec_type_namespace = image_codec_type_namespace;
	}

	public String getImage_codec_type_user_defined_value() {
		return image_codec_type_user_defined_value;
	}

	public void setImage_codec_type_user_defined_value(String image_codec_type_user_defined_value) {
		this.image_codec_type_user_defined_value = image_codec_type_user_defined_value;
	}

	public BigDecimal getImage_height() {
		return image_height;
	}

	public void setImage_height(BigDecimal image_height) {
		this.image_height = image_height;
	}

	public String getImage_height_unit_of_measure() {
		return image_height_unit_of_measure;
	}

	public void setImage_height_unit_of_measure(String image_height_unit_of_measure) {
		this.image_height_unit_of_measure = image_height_unit_of_measure;
	}

	public BigDecimal getImage_width() {
		return image_width;
	}

	public void setImage_width(BigDecimal image_width) {
		this.image_width = image_width;
	}

	public String getImage_width_unit_of_measure() {
		return image_width_unit_of_measure;
	}

	public void setImage_width_unit_of_measure(String image_width_unit_of_measure) {
		this.image_width_unit_of_measure = image_width_unit_of_measure;
	}

	public BigDecimal getAspect_ratio() {
		return aspect_ratio;
	}

	public void setAspect_ratio(BigDecimal aspect_ratio) {
		this.aspect_ratio = aspect_ratio;
	}

	public String getAspect_ratio_type() {
		return aspect_ratio_type;
	}

	public void setAspect_ratio_type(String aspect_ratio_type) {
		this.aspect_ratio_type = aspect_ratio_type;
	}

	public BigInteger getColor_depth() {
		return color_depth;
	}

	public void setColor_depth(BigInteger color_depth) {
		this.color_depth = color_depth;
	}

	public BigInteger getImage_resolution() {
		return image_resolution;
	}

	public void setImage_resolution(BigInteger image_resolution) {
		this.image_resolution = image_resolution;
	}

	public String getImage_preview_part_type() {
		return image_preview_part_type;
	}

	public void setImage_preview_part_type(String image_preview_part_type) {
		this.image_preview_part_type = image_preview_part_type;
	}

	public BigDecimal getImage_preview_top_left_corner() {
		return image_preview_top_left_corner;
	}

	public void setImage_preview_top_left_corner(BigDecimal image_preview_top_left_corner) {
		this.image_preview_top_left_corner = image_preview_top_left_corner;
	}

	public BigDecimal getImage_preview_bottom_right_corner() {
		return image_preview_bottom_right_corner;
	}

	public void setImage_preview_bottom_right_corner(BigDecimal image_preview_bottom_right_corner) {
		this.image_preview_bottom_right_corner = image_preview_bottom_right_corner;
	}

	public String getExpression_type() {
		return expression_type;
	}

	public void setExpression_type(String expression_type) {
		this.expression_type = expression_type;
	}

	public String getFulfillment_date() {
		return fulfillment_date;
	}

	public void setFulfillment_date(String fulfillment_date) {
		this.fulfillment_date = fulfillment_date;
	}

	public String getConsumer_fulfillment_date() {
		return consumer_fulfillment_date;
	}

	public void setConsumer_fulfillment_date(String consumer_fulfillment_date) {
		this.consumer_fulfillment_date = consumer_fulfillment_date;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public String getFile_suffix() {
		return file_suffix;
	}

	public void setFile_suffix(String file_suffix) {
		this.file_suffix = file_suffix;
	}

	@Override
	public String toString() {
		return "TechnicalImageDetails [image_codec_type=" + image_codec_type + ", image_height=" + image_height + ", image_width=" + image_width + ", aspect_ratio=" + aspect_ratio + "]";
	}

	@Override
	public TechnicalImageDetails clone() throws CloneNotSupportedException {
		return (TechnicalImageDetails) super.clone();
	}

}
