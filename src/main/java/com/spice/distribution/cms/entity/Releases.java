package com.spice.distribution.cms.entity;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "Releases")
public class Releases implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private int upload_transaction_id;
	@Column
	private int content_partner_id;
	@Column
	private int release_type_id;
	@Column
	private String user_defined_release_type;
	@Column
	private String upc;
	@Column
	private String release_reference;
	@Column
	private String title_text;
	@Column
	private String sub_title;
	@Column
	private String reference_title_language_and_script_code;
	@Column
	private String resource_omission_reason;
	@Column
	private BigInteger number_of_collections;
	@Column
	private Integer duration;
	@Column
	private Date global_release_date;
	@Column
	private Date global_original_release_date;
	@Column
	private String language_and_script_code;
	@Column
	private Character is_main_release;
	@Column
	private int release_status_id;
	@Column
	private String related_upc;
	@Column
	private String related_isrc;
	@Column
	private String global_release_identifier;
	@Column
	private String isrc;
	@OneToOne
	@JoinColumn(name = "release_type_id", insertable = false, updatable = false)
	private ReleaseTypes release_type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRelease_type_id() {
		return release_type_id;
	}

	public void setRelease_type_id(int release_type_id) {
		this.release_type_id = release_type_id;
	}

	public String getUser_defined_release_type() {
		return user_defined_release_type;
	}

	public void setUser_defined_release_type(String user_defined_release_type) {
		this.user_defined_release_type = user_defined_release_type;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getRelease_reference() {
		return release_reference;
	}

	public void setRelease_reference(String release_reference) {
		this.release_reference = release_reference;
	}

	public String getTitle_text() {
		return title_text;
	}

	public void setTitle_text(String title_text) {
		this.title_text = title_text;
	}

	public String getSub_title() {
		return sub_title;
	}

	public void setSub_title(String sub_title) {
		this.sub_title = sub_title;
	}

	public String getReference_title_language_and_script_code() {
		return reference_title_language_and_script_code;
	}

	public void setReference_title_language_and_script_code(String reference_title_language_and_script_code) {
		this.reference_title_language_and_script_code = reference_title_language_and_script_code;
	}

	public String getResource_omission_reason() {
		return resource_omission_reason;
	}

	public void setResource_omission_reason(String resource_omission_reason) {
		this.resource_omission_reason = resource_omission_reason;
	}

	public BigInteger getNumber_of_collections() {
		return number_of_collections;
	}

	public void setNumber_of_collections(BigInteger number_of_collections) {
		this.number_of_collections = number_of_collections;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getGlobal_release_date() {
		return global_release_date;
	}

	public void setGlobal_release_date(Date global_release_date) {
		this.global_release_date = global_release_date;
	}

	public Date getGlobal_original_release_date() {
		return global_original_release_date;
	}

	public void setGlobal_original_release_date(Date global_original_release_date) {
		this.global_original_release_date = global_original_release_date;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public Character getIs_main_release() {
		return is_main_release;
	}

	public void setIs_main_release(Character is_main_release) {
		this.is_main_release = is_main_release;
	}

	public int getRelease_status_id() {
		return release_status_id;
	}

	public void setRelease_status_id(int release_status_id) {
		this.release_status_id = release_status_id;
	}

	public int getUpload_transaction_id() {
		return upload_transaction_id;
	}

	public void setUpload_transaction_id(int upload_transaction_id) {
		this.upload_transaction_id = upload_transaction_id;
	}

	public int getContent_partner_id() {
		return content_partner_id;
	}

	public ReleaseTypes getRelease_type() {
		return release_type;
	}

	public void setRelease_type(ReleaseTypes release_type) {
		this.release_type = release_type;
	}

	public void setContent_partner_id(int content_partner_id) {
		this.content_partner_id = content_partner_id;
	}

	public String getRelated_upc() {
		return related_upc;
	}

	public void setRelated_upc(String related_upc) {
		this.related_upc = related_upc;
	}

	public String getRelated_isrc() {
		return related_isrc;
	}

	public void setRelated_isrc(String related_isrc) {
		this.related_isrc = related_isrc;
	}

	public String getGlobal_release_identifier() {
		return global_release_identifier;
	}

	public void setGlobal_release_identifier(String global_release_identifier) {
		this.global_release_identifier = global_release_identifier;
	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	@Override
	public Releases clone() throws CloneNotSupportedException {
		return (Releases) super.clone();
	}

	@Override
	public String toString() {
		return "Releases [id=" + id + ", upload_transaction_id=" + upload_transaction_id + ", content_partner_id=" + content_partner_id + ", release_type_id=" + release_type_id + ", user_defined_release_type=" + user_defined_release_type + ", upc=" + upc + ", release_reference=" + release_reference + ", title_text=" + title_text + ", sub_title=" + sub_title + ", reference_title_language_and_script_code=" + reference_title_language_and_script_code + ", resource_omission_reason="
				+ resource_omission_reason + ", number_of_collections=" + number_of_collections + ", duration=" + duration + ", global_release_date=" + global_release_date + ", global_original_release_date=" + global_original_release_date + ", language_and_script_code=" + language_and_script_code + ", is_main_release=" + is_main_release + ", release_status_id=" + release_status_id + ", related_upc=" + related_upc + ", related_isrc=" + related_isrc + ", global_release_identifier="
				+ global_release_identifier + ", isrc=" + isrc + ", release_type=" + release_type + "]";
	}

}
