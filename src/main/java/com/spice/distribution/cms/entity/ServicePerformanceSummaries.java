package com.spice.distribution.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ServicePerformanceSummaries")
public class ServicePerformanceSummaries {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private Integer service_id;

	@Column
	//@Temporal(TemporalType.DATE)
	private String  reporting_date;  
	
	@Column
	private Integer  reporting_month;
	@Column
	private Integer  reporting_year; 
	@Column
	private Integer  total_visit; 
	@Column
	private Integer  total_visit_and_browse ; 
	@Column
	private Integer  registration_app;
	@Column
	private Integer  registration_sms;
	@Column
	private Integer  subscription_app_hourly;
	@Column
	private Integer  subscription_app_daily; 
	@Column
	private Integer  subscription_app_weekly; 
	@Column
	private Integer  subscription_app_monthly; 
	@Column
	private Integer  total_subscription_app; 
	@Column
	private Integer  subscription_sms_hourly;
	@Column
	private Integer  subscription_sms_daily; 
	@Column
	private Integer  subscription_sms_weekly; 
	@Column
	private Integer  subscription_sms_monthly; 
	@Column
	private Integer  total_subscription_sms; 
	@Column
	private Integer  renewal_hourly;
	@Column
	private Integer  renewal_daily; 
	@Column
	private Integer  renewal_weekly; 
	@Column
	private Integer  renewal_monthly; 
	@Column
	private Integer  total_renewal; 
	@Column
	private Integer  data_hourly;
	@Column
	private Integer  data_daily; 
	@Column
	private Integer  data_weekly; 
	@Column
	private Integer  data_monthly; 
	@Column
	private Integer  billing_fail_hourly;
	@Column
	private Integer  billing_fail_daily; 
	@Column
	private Integer  billing_fail_weekly; 
	@Column
	private Integer  billing_fail_monthly; 
	@Column
	private Integer  unsubscribe_by_user; 
	@Column
	private Integer  unsubscribe_by_system;
	@Column
	private Integer  revenue_hourly;
	@Column
	private Integer  revenue_daily; 
	@Column
	private Integer  revenue_weekly; 
	@Column
	private Integer  revenue_monthly; 
	@Column
	private Integer  total_revenue;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getService_id() {
		return service_id;
	}
	public void setService_id(Integer service_id) {
		this.service_id = service_id;
	}
	public String getReporting_date() {
		return reporting_date;
	}
	public void setReporting_date(String reporting_date) {
		this.reporting_date = reporting_date;
	}
	public Integer getReporting_month() {
		return reporting_month;
	}
	public void setReporting_month(Integer reporting_month) {
		this.reporting_month = reporting_month;
	}
	public Integer getReporting_year() {
		return reporting_year;
	}
	public void setReporting_year(Integer reporting_year) {
		this.reporting_year = reporting_year;
	}
	public Integer getTotal_visit() {
		return total_visit;
	}
	public void setTotal_visit(Integer total_visit) {
		this.total_visit = total_visit;
	}
	public Integer getTotal_visit_and_browse() {
		return total_visit_and_browse;
	}
	public void setTotal_visit_and_browse(Integer total_visit_and_browse) {
		this.total_visit_and_browse = total_visit_and_browse;
	}
	public Integer getRegistration_app() {
		return registration_app;
	}
	public void setRegistration_app(Integer registration_app) {
		this.registration_app = registration_app;
	}
	public Integer getRegistration_sms() {
		return registration_sms;
	}
	public void setRegistration_sms(Integer registration_sms) {
		this.registration_sms = registration_sms;
	}
	public Integer getSubscription_app_hourly() {
		return subscription_app_hourly;
	}
	public void setSubscription_app_hourly(Integer subscription_app_hourly) {
		this.subscription_app_hourly = subscription_app_hourly;
	}
	public Integer getSubscription_app_daily() {
		return subscription_app_daily;
	}
	public void setSubscription_app_daily(Integer subscription_app_daily) {
		this.subscription_app_daily = subscription_app_daily;
	}
	public Integer getSubscription_app_weekly() {
		return subscription_app_weekly;
	}
	public void setSubscription_app_weekly(Integer subscription_app_weekly) {
		this.subscription_app_weekly = subscription_app_weekly;
	}
	public Integer getSubscription_app_monthly() {
		return subscription_app_monthly;
	}
	public void setSubscription_app_monthly(Integer subscription_app_monthly) {
		this.subscription_app_monthly = subscription_app_monthly;
	}
	public Integer getTotal_subscription_app() {
		return total_subscription_app;
	}
	public void setTotal_subscription_app(Integer total_subscription_app) {
		this.total_subscription_app = total_subscription_app;
	}
	public Integer getSubscription_sms_hourly() {
		return subscription_sms_hourly;
	}
	public void setSubscription_sms_hourly(Integer subscription_sms_hourly) {
		this.subscription_sms_hourly = subscription_sms_hourly;
	}
	public Integer getSubscription_sms_daily() {
		return subscription_sms_daily;
	}
	public void setSubscription_sms_daily(Integer subscription_sms_daily) {
		this.subscription_sms_daily = subscription_sms_daily;
	}
	public Integer getSubscription_sms_weekly() {
		return subscription_sms_weekly;
	}
	public void setSubscription_sms_weekly(Integer subscription_sms_weekly) {
		this.subscription_sms_weekly = subscription_sms_weekly;
	}
	public Integer getSubscription_sms_monthly() {
		return subscription_sms_monthly;
	}
	public void setSubscription_sms_monthly(Integer subscription_sms_monthly) {
		this.subscription_sms_monthly = subscription_sms_monthly;
	}
	public Integer getTotal_subscription_sms() {
		return total_subscription_sms;
	}
	public void setTotal_subscription_sms(Integer total_subscription_sms) {
		this.total_subscription_sms = total_subscription_sms;
	}
	public Integer getRenewal_hourly() {
		return renewal_hourly;
	}
	public void setRenewal_hourly(Integer renewal_hourly) {
		this.renewal_hourly = renewal_hourly;
	}
	public Integer getRenewal_daily() {
		return renewal_daily;
	}
	public void setRenewal_daily(Integer renewal_daily) {
		this.renewal_daily = renewal_daily;
	}
	public Integer getRenewal_weekly() {
		return renewal_weekly;
	}
	public void setRenewal_weekly(Integer renewal_weekly) {
		this.renewal_weekly = renewal_weekly;
	}
	public Integer getRenewal_monthly() {
		return renewal_monthly;
	}
	public void setRenewal_monthly(Integer renewal_monthly) {
		this.renewal_monthly = renewal_monthly;
	}
	public Integer getTotal_renewal() {
		return total_renewal;
	}
	public void setTotal_renewal(Integer total_renewal) {
		this.total_renewal = total_renewal;
	}
	public Integer getData_hourly() {
		return data_hourly;
	}
	public void setData_hourly(Integer data_hourly) {
		this.data_hourly = data_hourly;
	}
	public Integer getData_daily() {
		return data_daily;
	}
	public void setData_daily(Integer data_daily) {
		this.data_daily = data_daily;
	}
	public Integer getData_weekly() {
		return data_weekly;
	}
	public void setData_weekly(Integer data_weekly) {
		this.data_weekly = data_weekly;
	}
	public Integer getData_monthly() {
		return data_monthly;
	}
	public void setData_monthly(Integer data_monthly) {
		this.data_monthly = data_monthly;
	}
	public Integer getBilling_fail_hourly() {
		return billing_fail_hourly;
	}
	public void setBilling_fail_hourly(Integer billing_fail_hourly) {
		this.billing_fail_hourly = billing_fail_hourly;
	}
	public Integer getBilling_fail_daily() {
		return billing_fail_daily;
	}
	public void setBilling_fail_daily(Integer billing_fail_daily) {
		this.billing_fail_daily = billing_fail_daily;
	}
	public Integer getBilling_fail_weekly() {
		return billing_fail_weekly;
	}
	public void setBilling_fail_weekly(Integer billing_fail_weekly) {
		this.billing_fail_weekly = billing_fail_weekly;
	}
	public Integer getBilling_fail_monthly() {
		return billing_fail_monthly;
	}
	public void setBilling_fail_monthly(Integer billing_fail_monthly) {
		this.billing_fail_monthly = billing_fail_monthly;
	}
	public Integer getUnsubscribe_by_user() {
		return unsubscribe_by_user;
	}
	public void setUnsubscribe_by_user(Integer unsubscribe_by_user) {
		this.unsubscribe_by_user = unsubscribe_by_user;
	}
	public Integer getUnsubscribe_by_system() {
		return unsubscribe_by_system;
	}
	public void setUnsubscribe_by_system(Integer unsubscribe_by_system) {
		this.unsubscribe_by_system = unsubscribe_by_system;
	}
	public Integer getRevenue_hourly() {
		return revenue_hourly;
	}
	public void setRevenue_hourly(Integer revenue_hourly) {
		this.revenue_hourly = revenue_hourly;
	}
	public Integer getRevenue_daily() {
		return revenue_daily;
	}
	public void setRevenue_daily(Integer revenue_daily) {
		this.revenue_daily = revenue_daily;
	}
	public Integer getRevenue_weekly() {
		return revenue_weekly;
	}
	public void setRevenue_weekly(Integer revenue_weekly) {
		this.revenue_weekly = revenue_weekly;
	}
	public Integer getRevenue_monthly() {
		return revenue_monthly;
	}
	public void setRevenue_monthly(Integer revenue_monthly) {
		this.revenue_monthly = revenue_monthly;
	}
	public Integer getTotal_revenue() {
		return total_revenue;
	}
	public void setTotal_revenue(Integer total_revenue) {
		this.total_revenue = total_revenue;
	}
	
	
	
	
}
