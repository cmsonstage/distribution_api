package com.spice.distribution.cms.entity;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;



//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "safariId", "response" })
public class Audio {

	@JsonProperty("safariId")
	private String safariId;
	@JsonProperty("response")
	private Response response;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("safariId")
	public String getSafariId() {
		return safariId;
	}

	@JsonProperty("safariId")
	public void setSafariId(String safariId) {
		this.safariId = safariId;
	}

	@JsonProperty("response")
	public Response getResponse() {
		return response;
	}

	@JsonProperty("response")
	public void setResponse(Response response) {
		this.response = response;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "Audio [safariId=" + safariId + ", response=" + response + ", additionalProperties=" + additionalProperties + "]";
	}

}