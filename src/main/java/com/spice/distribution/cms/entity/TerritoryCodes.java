package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "TerritoryCodes")
public class TerritoryCodes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String ddex_territory_code;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDdex_territory_code() {
		return ddex_territory_code;
	}

	public void setDdex_territory_code(String ddex_territory_code) {
		this.ddex_territory_code = ddex_territory_code;
	}

}
