/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.cms.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.Response;

/**
 * The Class ContentRefreshingTransactions represents ContentRefreshingTransactions Table in CMS.
 *
 * @author ankit
 */
@Entity
@Indexed
public class ContentRefreshingTransactions implements Response, Request, Comparable<ContentRefreshingTransactions> {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	/** The user id. */
	@JsonProperty(access = Access.WRITE_ONLY)
	@NotNull(message = "userId not found!!")
	private int user_id;

	/** The country id. */

	private Integer country_id;

	/** The service id. */

	private Integer service_id;

	/** The service name. */
	@Field(store = Store.NO)
	private String service_name;

	/** The container id. */
	private Integer container_id;

	/** The country name. */
	@Field(store = Store.NO)
	private String country_name;

	/** The container name. */
	@Field(store = Store.NO)
	private String container_name;

	/** The content refreshing transaction status id. */
	@JsonProperty(value = "content_refreshing_transaction_status_id", access = Access.WRITE_ONLY)
	@Column(name = "content_refreshing_transaction_status_id")
	private int contentRefreshingTransactionStatusId = 1;

	/** The comment. */
	private String comment;

	/** The description. */
	private String description;

	/** The created at. */
	@CreationTimestamp
	private Date created_at;

	/** The updated at. */
	@UpdateTimestamp
	private Date updated_at;

	/** The users. */
	@OneToOne
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private Users users;

	/** The content refreshing transaction statuses. */
	@OneToOne
	@JoinColumn(name = "content_refreshing_transaction_status_id", insertable = false, updatable = false)
	@IndexedEmbedded
	private ContentRefreshingTransactionStatuses contentRefreshingTransactionStatuses;

	/** The content refreshing transaction details. */
	@JsonProperty(access = Access.WRITE_ONLY)
	@Valid
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "content_refreshing_transaction_id", referencedColumnName = "id")
	private Set<ContentRefreshingTransactionDetails> contentRefreshingTransactionDetails;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * Sets the user id.
	 *
	 * @param user_id the new user id
	 */

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * Gets the country id.
	 *
	 * @return the country id
	 */
	public Integer getCountry_id() {
		return country_id;
	}

	/**
	 * Sets the country id.
	 *
	 * @param country_id the new country id
	 */
	public void setCountry_id(Integer country_id) {
		this.country_id = country_id;
	}

	/**
	 * Gets the service id.
	 *
	 * @return the service id
	 */
	public Integer getService_id() {
		return service_id;
	}

	/**
	 * Sets the service id.
	 *
	 * @param service_id the new service id
	 */
	public void setService_id(Integer service_id) {
		this.service_id = service_id;
	}

	/**
	 * Gets the service name.
	 *
	 * @return the service name
	 */
	public String getService_name() {
		return service_name;
	}

	/**
	 * Sets the service name.
	 *
	 * @param service_name the new service name
	 */
	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	/**
	 * Gets the container id.
	 *
	 * @return the container id
	 */
	public Integer getContainer_id() {
		return container_id;
	}

	/**
	 * Sets the container id.
	 *
	 * @param container_id the new container id
	 */
	public void setContainer_id(Integer container_id) {
		this.container_id = container_id;
	}

	/**
	 * Gets the container name.
	 *
	 * @return the container name
	 */
	public String getContainer_name() {
		return container_name;
	}

	/**
	 * Sets the container name.
	 *
	 * @param container_name the new container name
	 */
	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}

	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the created at.
	 *
	 * @return the created at
	 */
	public Date getCreated_at() {
		return created_at;
	}

	/**
	 * Sets the created at.
	 *
	 * @param created_at the new created at
	 */
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	/**
	 * Gets the updated at.
	 *
	 * @return the updated at
	 */
	public Date getUpdated_at() {
		return updated_at;
	}

	/**
	 * Sets the updated at.
	 *
	 * @param updated_at the new updated at
	 */
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Users getUsers() {
		return users;
	}

	/**
	 * Sets the users.
	 *
	 * @param users the new users
	 */
	public void setUsers(Users users) {
		this.users = users;
	}

	/**
	 * Gets the content refreshing transaction statuses.
	 *
	 * @return the content refreshing transaction statuses
	 */
	public ContentRefreshingTransactionStatuses getContentRefreshingTransactionStatuses() {
		return contentRefreshingTransactionStatuses;
	}

	/**
	 * Sets the content refreshing transaction statuses.
	 *
	 * @param contentRefreshingTransactionStatuses the new content refreshing transaction statuses
	 */
	public void setContentRefreshingTransactionStatuses(ContentRefreshingTransactionStatuses contentRefreshingTransactionStatuses) {
		this.contentRefreshingTransactionStatuses = contentRefreshingTransactionStatuses;
	}

	/**
	 * Gets the country name.
	 *
	 * @return the country name
	 */
	public String getCountry_name() {
		return country_name;
	}

	/**
	 * Sets the country name.
	 *
	 * @param country_name the new country name
	 */
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	/**
	 * Gets the content refreshing transaction details.
	 *
	 * @return the content refreshing transaction details
	 */
	public Set<ContentRefreshingTransactionDetails> getContentRefreshingTransactionDetails() {
		return contentRefreshingTransactionDetails;
	}

	/**
	 * Sets the content refreshing transaction details.
	 *
	 * @param contentRefreshingTransactionDetails the new content refreshing transaction details
	 */
	public void setContentRefreshingTransactionDetails(Set<ContentRefreshingTransactionDetails> contentRefreshingTransactionDetails) {
		this.contentRefreshingTransactionDetails = contentRefreshingTransactionDetails;
	}

	/**
	 * Gets the content refreshing transaction status id.
	 *
	 * @return the content refreshing transaction status id
	 */
	public int getContentRefreshingTransactionStatusId() {
		return contentRefreshingTransactionStatusId;
	}

	public void setContentRefreshingTransactionStatusId(int contentRefreshingTransactionStatusId) {
		this.contentRefreshingTransactionStatusId = contentRefreshingTransactionStatusId;
	}

	@Override
	public int compareTo(ContentRefreshingTransactions o) {
		return this.getId() - o.getId();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContentRefreshingTransactions [id=" + id + ", contentRefreshingTransactionDetails=" + contentRefreshingTransactionDetails + "]";
	}

}
