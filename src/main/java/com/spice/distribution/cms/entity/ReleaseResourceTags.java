package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "ReleaseResourceTags")
public class ReleaseResourceTags implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "release_resource_id")
	private Integer releaseResourceId;
	@Column
	private Integer tag_id;
	@OneToOne
	@JoinColumn(name = "tag_id", insertable = false, updatable = false)
	private Tags tags;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getReleaseResourceId() {
		return releaseResourceId;
	}

	public void setReleaseResourceId(Integer releaseResourceId) {
		this.releaseResourceId = releaseResourceId;
	}

	public Integer getTag_id() {
		return tag_id;
	}

	public void setTag_id(Integer tag_id) {
		this.tag_id = tag_id;
	}

	public Tags getTags() {
		return tags;
	}

	public void setTags(Tags tags) {
		this.tags = tags;
	}

	@Override
	public ReleaseResourceTags clone() throws CloneNotSupportedException {
		return (ReleaseResourceTags) super.clone();
	}

}
