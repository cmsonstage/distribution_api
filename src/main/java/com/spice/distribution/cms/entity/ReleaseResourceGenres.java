package com.spice.distribution.cms.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "ReleaseResourceGenres")
public class ReleaseResourceGenres implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "release_resource_id")
	private Integer releaseResourceId;
	@Column
	private Integer genre_id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "genre_id", referencedColumnName = "id", insertable = false, updatable = false)
	private Genres genres;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getReleaseResourceId() {
		return releaseResourceId;
	}

	public void setReleaseResourceId(Integer releaseResourceId) {
		this.releaseResourceId = releaseResourceId;
	}

	public Integer getGenre_id() {
		return genre_id;
	}

	public void setGenre_id(Integer genre_id) {
		this.genre_id = genre_id;
	}

	@Override
	public ReleaseResourceGenres clone() throws CloneNotSupportedException {
		return (ReleaseResourceGenres) super.clone();
	}

	public Genres getGenres() {
		return genres;
	}

	public void setGenres(Genres genres) {
		this.genres = genres;
	}

	@Override
	public String toString() {
		return "ReleaseResourceGenres [id=" + id + ", releaseResourceId=" + releaseResourceId + ", genre_id=" + genre_id + ", genres=" + genres + "]";
	}

}
