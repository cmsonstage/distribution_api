package com.spice.distribution.cms.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Store;

/**
 * @author ankit
 *
 */
@Entity
public class ContentRefreshingTransactionStatuses {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Field(store = Store.NO)
	private String content_refreshing_transaction_status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent_refreshing_transaction_status() {
		return content_refreshing_transaction_status;
	}

	public void setContent_refreshing_transaction_status(String content_refreshing_transaction_status) {
		this.content_refreshing_transaction_status = content_refreshing_transaction_status;
	}

}
