package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="UseTypes")
public class UseTypes {
	@Id
	private int id;
	@Column(name="use_type")
	private String UseType;
	@Column
	private String definition;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the useType
	 */
	public String getUseType() {
		return UseType;
	}
	/**
	 * @param useType the useType to set
	 */
	public void setUseType(String useType) {
		UseType = useType;
	}
	/**
	 * @return the definition
	 */
	public String getDefinition() {
		return definition;
	}
	/**
	 * @param definition the definition to set
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	

}
