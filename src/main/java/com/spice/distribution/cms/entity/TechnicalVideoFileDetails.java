package com.spice.distribution.cms.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "TechnicalVideoFileDetails")
public class TechnicalVideoFileDetails implements Cloneable {
	@Id
	private int id;
	@Column(name="video_id")
	private Integer videoId;
	@Column
	private Integer video_details_by_territory_id;
	@Column(name="technical_video_details_id")
	private Integer technicalVideoDetailsId;
	@Column
	private String technical_resource_details_reference;
	@Column
	private String file_name;
	@Column
	private String file_path;
	@Column
	private Integer technical_file_type_id;
	@Column
	private Long file_size;
	@Column
	private Long duration;
	@Column
	private Timestamp updated_at;
	@Column
	private Byte is_file_updated;
	@Type(type = "numeric_boolean")
	private Boolean is_original;
	@Type(type = "numeric_boolean")
	private Boolean is_transcoded;
	@Type(type = "numeric_boolean")
	private Boolean is_updated;
	@Type(type = "numeric_boolean")
	private Boolean is_replaced;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getVideo_details_by_territory_id() {
		return video_details_by_territory_id;
	}

	public void setVideo_details_by_territory_id(Integer video_details_by_territory_id) {
		this.video_details_by_territory_id = video_details_by_territory_id;
	}

	/**
	 * @return the videoId
	 */
	public Integer getVideoId() {
		return videoId;
	}

	/**
	 * @param videoId the videoId to set
	 */
	public void setVideoId(Integer videoId) {
		this.videoId = videoId;
	}

	/**
	 * @return the technicalVideoDetailsId
	 */
	public Integer getTechnicalVideoDetailsId() {
		return technicalVideoDetailsId;
	}

	/**
	 * @param technicalVideoDetailsId the technicalVideoDetailsId to set
	 */
	public void setTechnicalVideoDetailsId(Integer technicalVideoDetailsId) {
		this.technicalVideoDetailsId = technicalVideoDetailsId;
	}

	public String getTechnical_resource_details_reference() {
		return technical_resource_details_reference;
	}

	public void setTechnical_resource_details_reference(String technical_resource_details_reference) {
		this.technical_resource_details_reference = technical_resource_details_reference;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public Integer getTechnical_file_type_id() {
		return technical_file_type_id;
	}

	public void setTechnical_file_type_id(Integer technical_file_type_id) {
		this.technical_file_type_id = technical_file_type_id;
	}

	public Long getFile_size() {
		return file_size;
	}

	public void setFile_size(Long file_size) {
		this.file_size = file_size;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Byte getIs_file_updated() {
		return is_file_updated;
	}

	public void setIs_file_updated(Byte is_file_updated) {
		this.is_file_updated = is_file_updated;
	}

	public Boolean getIs_original() {
		return is_original;
	}

	public void setIs_original(Boolean is_original) {
		this.is_original = is_original;
	}

	public Boolean getIs_transcoded() {
		return is_transcoded;
	}

	public void setIs_transcoded(Boolean is_transcoded) {
		this.is_transcoded = is_transcoded;
	}

	public Boolean getIs_updated() {
		return is_updated;
	}

	public void setIs_updated(Boolean is_updated) {
		this.is_updated = is_updated;
	}

	public Boolean getIs_replaced() {
		return is_replaced;
	}

	public void setIs_replaced(Boolean is_replaced) {
		this.is_replaced = is_replaced;
	}

	@Override
	public TechnicalVideoFileDetails clone() throws CloneNotSupportedException {
		return (TechnicalVideoFileDetails) super.clone();
	}

}
