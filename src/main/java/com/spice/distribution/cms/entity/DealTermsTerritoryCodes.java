package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "DealTermsTerritoryCodes")
public class DealTermsTerritoryCodes implements Cloneable {
	@Id
	private int id;
	@Column
	private Integer deal_id;
	@Column
	private Integer territory_code_id;
	@OneToOne
	@JoinColumn(name = "territory_code_id", insertable = false, updatable = false)
	private TerritoryCodes territoryCodes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getDeal_id() {
		return deal_id;
	}

	public void setDeal_id(Integer deal_id) {
		this.deal_id = deal_id;
	}

	public Integer getTerritory_code_id() {
		return territory_code_id;
	}

	public void setTerritory_code_id(Integer territory_code_id) {
		this.territory_code_id = territory_code_id;
	}

	@Override
	public DealTermsTerritoryCodes clone() throws CloneNotSupportedException {
		return (DealTermsTerritoryCodes) super.clone();
	}

	public TerritoryCodes getTerritoryCodes() {
		return territoryCodes;
	}

	public void setTerritoryCodes(TerritoryCodes territoryCodes) {
		this.territoryCodes = territoryCodes;
	}

	@Override
	public String toString() {
		return "DealTermsTerritoryCodes [id=" + id + ", deal_id=" + deal_id + ", territory_code_id=" + territory_code_id + ", deals=]";
	}

}
