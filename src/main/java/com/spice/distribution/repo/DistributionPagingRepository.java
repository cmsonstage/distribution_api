package com.spice.distribution.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.spice.distribution.entity.Distributions;

/**
 * @author ankit
 *
 */
public interface DistributionPagingRepository extends PagingAndSortingRepository<Distributions, Integer> {
}
