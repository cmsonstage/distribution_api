package com.spice.distribution.repo;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.entity.DistributionActions;

/**
 * @author ankit
 *
 */
public interface DistributionActionsRepository extends CrudRepository<DistributionActions, Integer> {
}
