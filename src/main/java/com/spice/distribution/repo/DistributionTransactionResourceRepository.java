package com.spice.distribution.repo;

import java.math.BigInteger;
import java.util.List;



import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.entity.DistributionTransactionResources;

/**
 * @author ankit
 *
 */
@Transactional
public interface DistributionTransactionResourceRepository extends CrudRepository<DistributionTransactionResources, Integer> , PagingAndSortingRepository<DistributionTransactionResources, Integer> {
	
	@Query(value = "SELECT " + "    SUM(IF(distribution_transaction_resource_status_id <= 3, 1, 0)) AS PENDING," + "    SUM(IF(distribution_transaction_resource_status_id = 5, 1, 0)) AS SUCCESSFULL," + "    SUM(IF(distribution_transaction_resource_status_id = 4, 1, 0)) AS FAILED , t1.distribution_transaction_id " + " FROM " + "    DISTRIBUTION.DistributionTransactionResources as t1" + " WHERE"
			+ " t1.distribution_transaction_id IN(:ids) group by t1.distribution_transaction_id", nativeQuery = true)
	List<Object[]> findStatus(@Param("ids") List<Integer> ids);
	
	@Query(value="select distinct t1.resource_code from DistributionTransactionResources as t1 inner join DistributionTransactions as t2 on t1.distribution_transaction_id=t2.id where t2.service_id =:serviceId order by t2.id desc limit 3",nativeQuery=true)
	List<String> findByServiceId(@Param("serviceId") int serviceId);
	
	
	@Modifying
	@Query(value = "update DistributionTransactionResources set remarks = 'Test' where reference_id = :reference_id AND resource_code = :resource_code", nativeQuery = true)
	void updateRemark(@Param("reference_id") BigInteger reference_id,@Param("resource_code") String resource_code);
	
}