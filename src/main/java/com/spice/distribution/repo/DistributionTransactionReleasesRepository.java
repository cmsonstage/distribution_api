package com.spice.distribution.repo;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.entity.DistributionTransactionReleases;

/**
 * @author ankit
 *
 */
public interface DistributionTransactionReleasesRepository extends CrudRepository<DistributionTransactionReleases, Integer> {

}
