package com.spice.distribution.repo;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.entity.Distributions1;

/**
 * @author ankit
 *
 */
public interface Distribution1Repository extends CrudRepository<Distributions1, Integer> {

}
