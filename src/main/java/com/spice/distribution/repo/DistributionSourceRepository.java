package com.spice.distribution.repo;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.entity.DistributionSources;

/**
 * @author ankit
 *
 */
public interface DistributionSourceRepository extends CrudRepository<DistributionSources, Integer> {

}
