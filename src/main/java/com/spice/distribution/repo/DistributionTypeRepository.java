package com.spice.distribution.repo;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.entity.DistributionTypes;

/**
 * @author ankit
 *
 */
public interface DistributionTypeRepository extends CrudRepository<DistributionTypes, Integer> {

}
