package com.spice.distribution.validation;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public interface EncryptResource {
	static final String encryptionKey = "SPICEVODAPPWOWZA";   
	static  final String characterEncoding = "UTF-8";   
	static  String cipherTransformation = "AES/ECB/PKCS5PADDING";    
	 static final String aesEncryptionAlgorithem = "AES"; 
	 public static String encrypt(String plainText) {    
		    String encryptedText = "";     
		   try {   
		         Cipher cipher   = Cipher.getInstance(cipherTransformation);           
		 byte[] key      = encryptionKey.getBytes(characterEncoding);            
		SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);                     
		 cipher.init(Cipher.ENCRYPT_MODE, secretKey);            
		byte[] cipherText = cipher.doFinal(plainText.getBytes("UTF8"));           
		encryptedText=Urlencode(Base64.encodeBase64String(cipherText)); 
		 
		 } catch (Exception E) 
		{             System.err.println("Encrypt Exception : "+E.getMessage());        }      
		  return encryptedText;   
		 } 
	 
	 
	 public static  String decrypt(String encryptedText) {        
		 String decryptedText = "";        try {            
		 Cipher cipher = Cipher.getInstance(cipherTransformation);           
		  byte[] key = encryptionKey.getBytes(characterEncoding);           
		  SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);
        
		  cipher.init(Cipher.DECRYPT_MODE, secretKey);                  

		    byte[] cipherText = Base64.decodeBase64(encryptedText);             
		     decryptedText = new String(cipher.doFinal(cipherText));        }
		  catch (Exception E) {  
		           //System.err.println("decrypt Exception : "+E.getMessage());         
		    E.printStackTrace();     
		                }   
		      return decryptedText; 
	 }
	 
	 public static String Urlencode(String url)  
     {  
               try {  
                    String encodeURL=URLEncoder.encode( url, "UTF-8" );  
                    return encodeURL;  
               } catch (UnsupportedEncodingException e) {  
                    return "Issue while encoding" +e.getMessage();  
               }  
     }
}
