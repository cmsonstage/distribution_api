package com.spice.distribution.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author ankit
 *
 */
public class ResourceCodeValidator implements ConstraintValidator<ResourceCodeValidation, String> {

	private volatile String codes;

	@Override
	public void initialize(ResourceCodeValidation constraintAnnotation) {
		codes = constraintAnnotation.CODE();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		for (char code : codes.toCharArray()) {
			if (!Character.isDigit(code))
				return false;
		}
		return (codes.length() == 9);

	}

}
